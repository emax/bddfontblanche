#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QFile>
#include <QTextStream>
#include <QCloseEvent>
#include <QString>
#include <QLabel>
#include <QTableView>
#include <QStringList>
#include <QMessageBox>
#include <QMap>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QList>
#include <QHeaderView>
#include <Qt>
#include <QPair>
#include <QDate>
#include <QVariant>
#include <QComboBox>
#include <QFileDialog>
#include <QTextEdit>
#include <QLineEdit>
#include <QDateEdit>
#include <QInputDialog>
#include <dialogdate.h>
#include <QDateTime>
#include <QDir>
#include <QRegExp>
#include <QRegExpValidator>
#include <QValidator>
#include <addaxisclass.h>
#include <QFont>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QColor>
#include <QPalette>
#include <QHeaderView>
#include <QKeyEvent>
#include <QShortcut>
#include <newstatus.h>
#include <focuschangepolicy.h>
#include <QApplication>
#include <changeparameterclass.h>

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QString* line);
    ~MainWindow();

private slots:
    void consultOrExportData();
    void backTo0Slot();
    void consultData();
    void whoClickedMe();
    void backToConsultOrExport();
    void expandDataConsult(int a, int b);
    void changeDateDataConsult(int index);
    void exportDatasSlot();
    void exportAllSlot();
    void exportFinalSlot();
    void prepareCampaignSlot();
    void saveDataContext();
    void inputDataSet();
    void boardInputSet();
    void inputData();
    void expandDataInput(int a, int b);
    void addComment();
    void deleteComment(int a, int b);
    void addAxis();
    void updateCommentAndModifyMeasure(int a, int b);
    void changeComment(int a);
    void checkValue(int a);
    void checkValue(double a);
    void checkValue(QString a);
    void modifiesInputTable(int a, int b);
    void modifiesInputTable(int b);
    void modifiesInputTable();
    void modifiesStatus();
    void saveRecord();
    void modifiesDataBase();
    void modifiesDatas();
    void changeAxisCharacteristics();
    void addParameter();
    void loadPreviousDatas();
    void modifiesDataPlot();
    void deleteRow();
    void moveUp();
    void moveRight();
    void moveDown();
    void moveLeft();
    void eraseContent();
    void loadBoardInput();
    void loadDatas();
    void changeLocalisationParameter(int a);
    void changeTypeParameter(int a);
    void addParameterSlot();
    void changeCharacteristics();
    void expandModifiesData(int a, int b);
    void deleteMeasure();
    void modifiesValueData(int a, int b);
    void expandchangeCharacteristics(int a, int b);
    void newParameter();
    void deleteParameter(int a, int b);
    void updateCommentConsult(int a, int b);

private:
    QGridLayout *mainLayout;
    QString *state;
    void urgentStop();
    QVariant userData;

protected:
    void closeEvent(QCloseEvent* event);
};

#endif // MAINWINDOW_H
