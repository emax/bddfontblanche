#include <MainWindow.h>

void MainWindow::addAxis()
{
    QList<QVariant> axisProperties;
    int plotCode;
    plotCode = qobject_cast<QComboBox *>(mainLayout->itemAtPosition(2, 2)->widget())->itemData(0).toInt();
    axisProperties = addAxisClass::addAxis(NULL, "Nouvel axe", plotCode);
    QTableWidget *tab;
    tab = qobject_cast<QTableWidget*>(mainLayout->itemAtPosition(2, 0)->widget());
    tab->setSortingEnabled(false);
    if (axisProperties.size() > 0)
    {
        tab->insertRow(0);
        QTableWidgetItem *cell;
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, axisProperties);
        if (axisProperties.size() > 11) cell->setText(axisProperties.at(11).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab->setItem(0, 0, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, axisProperties);
        if (axisProperties.size() > 12) cell->setText(axisProperties.at(12).toString());
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab->setItem(0, 1, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, axisProperties);
        cell->setText("0");
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab->setItem(0, 2, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, axisProperties);
        cell->setText("0");
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab->setItem(0, 3, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, axisProperties);
        cell->setText("00/00/0000");
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab->setItem(0, 4, cell);
        cell = new QTableWidgetItem;
        cell->setData(Qt::UserRole, axisProperties);
        cell->setText("00/00/0000");
        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
        tab->setItem(0, 5, cell);
    }
    tab->setSortingEnabled(true);
}
