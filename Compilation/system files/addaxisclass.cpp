#include <addaxisclass.h>

addAxisClass::addAxisClass(int plot): QDialog()
{
    plotCode = plot;
    QLabel *treeLabel = new QLabel("Arbre :");
    QLabel *positionLabel = new QLabel("Position :");
    QLabel *expositionLabel = new QLabel("Exposition :");
    QLabel *dominanceLabel = new QLabel("Dominance :");
    QLabel *numberLabel = new QLabel("Numéro de l'axe :");
    QLabel *nameLabel = new QLabel("Nom de l'axe :");
    QGridLayout *mainLayout = new QGridLayout;

    tree = new QComboBox;
    position = new QComboBox;
    exposition = new QComboBox;
    dominance = new QComboBox;
    axisNumber = new QLineEdit;
    axisName = new QLabel;
    validate = new QPushButton("Valider");

    tree->setEditable(true);
    position->setEditable(true);
    exposition->setEditable(true);
    dominance->setEditable(true);

    // first list1
    QFile treeFile("bin/list1");
    int countLine(-1);
    QMap<QString, int> treePosition;
    QMap<int, QStringList> treeMap;
    QMap<int, QString> treeNames;
    QStringList dataRow;
    int Id;
    QString line;
    if (!treeFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list1.");
        urgentStop();
        return;
    }else{
        QTextStream treeStream(&treeFile);
        line = treeStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list1\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!treeStream.atEnd()  && line != "###")
            {
                line = treeStream.readLine();
                if (line.split("\t").size() > 0)treePosition.insert(line.split("\t").at(0), countLine);
                countLine++;
            }
            while(!treeStream.atEnd())
            {
                dataRow = treeStream.readLine().split("\t");
                if (dataRow.size() > 0) Id = dataRow.at(0).toInt();
                dataRow.removeFirst();
                if (dataRow.size() > treePosition["Id0"])
                {
                    if (dataRow.at(treePosition["Id0"]).toInt() == plot)
                    {
                        treeMap.insert(Id, dataRow);
                    }
                }
                if (dataRow.size() > treePosition["NumArb"]) treeNames.insert(Id, QString("PA%1").arg(dataRow.at(treePosition["NumArb"])));
            }
        }
        // then position
        QFile posFile("bin/dictionary/position");
        countLine = -1;
        QMap<QString, int> posPosition;
        QMap<int, QStringList> posMap;
        if (!posFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/position.");
            urgentStop();
            return;
        }else{
            QTextStream posStream(&posFile);
            line = posStream.readLine();
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/position\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!posStream.atEnd()  && line != "###")
                {
                    line = posStream.readLine();
                    if (line.split("\t").size() > 0) posPosition.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
                while(!posStream.atEnd())
                {
                    dataRow = posStream.readLine().split("\t");
                    if (dataRow.size() > 0) Id = dataRow.at(0).toInt();
                    dataRow.removeFirst();
                    posMap.insert(Id, dataRow);
                }
            }
            // then exposition
            QFile expFile("bin/dictionary/exposition");
            countLine = -1;
            QMap<QString, int> expPosition;
            QMap<int, QStringList> expMap;
            if (!expFile.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/exposition.");
                urgentStop();
                return;
            }else{
                QTextStream expStream(&expFile);
                line = expStream.readLine();
                if (line != "###")
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/exposition\nMauvaise entête (\"###\" attendu)");
                    urgentStop();
                    return;
                }else{
                    line = "";
                    while (!expStream.atEnd()  && line != "###")
                    {
                        line = expStream.readLine();
                        if (line.split("\t").size() > 0) expPosition.insert(line.split("\t").at(0), countLine);
                        countLine++;
                    }
                    while(!expStream.atEnd())
                    {
                        dataRow = expStream.readLine().split("\t");
                        if (dataRow.size() > 0) Id = dataRow.at(0).toInt();
                        dataRow.removeFirst();
                        expMap.insert(Id, dataRow);
                    }
                }
                // then dominance
                QFile domFile("bin/dictionary/dominance");
                countLine = -1;
                QMap<QString, int> domPosition;
                QMap<int, QStringList> domMap;
                if (!domFile.open(QIODevice::ReadOnly | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/dominance.");
                    urgentStop();
                    return;
                }else{
                    QTextStream domStream(&domFile);
                    line = domStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/dominance\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = "";
                        while (!domStream.atEnd()  && line != "###")
                        {
                            line = domStream.readLine();
                            if (line.split("\t").size() > 0) domPosition.insert(line.split("\t").at(0), countLine);
                            countLine++;
                        }
                        while(!domStream.atEnd())
                        {
                            dataRow = domStream.readLine().split("\t");
                            if (dataRow.size() > 0) Id = dataRow.at(0).toInt();
                            dataRow.removeFirst();
                            domMap.insert(Id, dataRow);
                        }
                    }
                    // then list2
                    QFile axisFile("bin/list2");
                    countLine = -1;
                    QMap<QString, int> axisPosition;
                    QMap<int, QStringList> axisMap;
                    if (!axisFile.open(QIODevice::ReadOnly | QIODevice::Text))
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list2.");
                        urgentStop();
                        return;
                    }else{
                        QTextStream axisStream(&axisFile);
                        line = axisStream.readLine();
                        if (line != "###")
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list2\nMauvaise entête (\"###\" attendu)");
                            urgentStop();
                            return;
                        }else{
                            line = "";
                            while (!axisStream.atEnd()  && line != "###")
                            {
                                line = axisStream.readLine();
                                if (line.split("\t").size() > 0) axisPosition.insert(line.split("\t").at(0), countLine);
                                countLine++;
                            }
                            while(!axisStream.atEnd())
                            {
                                dataRow = axisStream.readLine().split("\t");
                                if (dataRow.size() > 0) Id = dataRow.at(0).toInt();
                                dataRow.removeFirst();
                                if (dataRow.size() > axisPosition["Id1"])
                                {
                                    if (treeMap.contains(dataRow.at(axisPosition["Id1"]).toInt()))
                                    {
                                        axisMap.insert(Id, dataRow);
                                    }
                                }
                                if (dataRow.size() > axisPosition["Id2"] && dataRow.size() > axisPosition["IdPos"])
                                {
                                    if (posMap[dataRow.at(axisPosition["IdPos"]).toInt()].size() > posPosition["ShortNamePosition"] && dataRow.size() > axisPosition["IdExp"])
                                    {
                                        if (expMap[dataRow.at(axisPosition["IdExp"]).toInt()].size() > expPosition["ShortNameExposition"] && dataRow.size() > axisPosition["IdDom"])
                                        {
                                            if (domMap[dataRow.at(axisPosition["IdDom"]).toInt()].size() > domPosition["ShortNameDominance"] && dataRow.size() > axisPosition["NRam"])
                                            {
                                                axisNames.append(QString("%1 %2%3 %4%5").arg(treeNames[dataRow.at(axisPosition["Id1"]).toInt()]).arg(posMap[dataRow.at(axisPosition["IdPos"]).toInt()].at(posPosition["ShortNamePosition"])).arg(expMap[dataRow.at(axisPosition["IdExp"]).toInt()].at(expPosition["ShortNameExposition"])).arg(domMap[dataRow.at(axisPosition["IdDom"]).toInt()].at(domPosition["ShortNameDominance"])).arg(dataRow.at(axisPosition["NRam"])));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // -> now we load axis names from axischaracteristics
                        QFile paramFile("bin/axisCharacteristics");
                        QMap<QString, int> charactPosition;
                        countLine = -1;
                        if (!paramFile.open(QIODevice::ReadOnly | QIODevice::Text))
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/axisCharacteristics.");
                            urgentStop();
                            return;
                        }else{
                            QTextStream paramStream(&paramFile);
                            line = paramStream.readLine();
                            if (line != "###")
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/axisCharacteristics\nMauvaise entête (\"###\" attendu)");
                                urgentStop();
                                return;
                            }else{
                                line = "";
                                while (!paramStream.atEnd()  && line != "###")
                                {
                                    line = paramStream.readLine();
                                    if (line != "###")
                                    {
                                        if (line.split("\t").size() > 0) charactPosition.insert(line.split("\t").at(0), countLine);
                                        countLine++;
                                    }
                                }
                                while(!paramStream.atEnd())
                                {
                                    dataRow = paramStream.readLine().split("\t");
                                    dataRow.removeFirst();
                                    if (dataRow.size() > charactPosition["CurrName"]) axisNames.append(dataRow.at(charactPosition["CurrName"]));
                                }
                            }
                            paramFile.close();
                        }
                        // Now we can fill the comboboxes
                        tree->setInsertPolicy(QComboBox::InsertAtTop);
                        for (QMap<int, QStringList>::iterator it(treeMap.begin()); it != treeMap.end(); it++)
                        {
                            tree->addItem(QString("PA%1").arg(it->at(treePosition["NumArb"])), QVariant(it.key()));
                        }
                        tree->model()->sort(0);
                        tree->setCurrentIndex(-1);
                        position->setInsertPolicy(QComboBox::InsertAtTop);
                        for (QMap<int, QStringList>::iterator it(posMap.begin()); it != posMap.end(); it++)
                        {
                            position->addItem(it->at(posPosition["ShortNamePosition"]), QVariant(it.key()));
                        }
                        position->model()->sort(0);
                        position->setCurrentIndex(-1);
                        exposition->setInsertPolicy(QComboBox::InsertAtTop);
                        for (QMap<int, QStringList>::iterator it(expMap.begin()); it != expMap.end(); it++)
                        {
                            exposition->addItem(it->at(expPosition["ShortNameExposition"]), QVariant(it.key()));
                        }
                        exposition->model()->sort(0);
                        exposition->setCurrentIndex(-1);
                        dominance->setInsertPolicy(QComboBox::InsertAtTop);
                        for (QMap<int, QStringList>::iterator it(domMap.begin()); it != domMap.end(); it++)
                        {
                            dominance->addItem(it->at(domPosition["ShortNameDominance"]), QVariant(it.key()));
                        }
                        dominance->model()->sort(0);
                        dominance->setCurrentIndex(-1);
                    }
                    axisFile.close();
                }
                domFile.close();
            }
            expFile.close();
        }
        posFile.close();
    }
    treeFile.close();

    // connect Signals to slots
    QRegExp rx("^PA[0-9]+$");
    QValidator *validator = new QRegExpValidator(rx, this);
    tree->setValidator(validator);
    QObject::connect(tree, SIGNAL(editTextChanged(QString)), this, SLOT(changeName(QString)));
    QObject::connect(position, SIGNAL(editTextChanged(QString)), this, SLOT(changeName(QString)));
    QObject::connect(exposition, SIGNAL(editTextChanged(QString)), this, SLOT(changeName(QString)));
    QObject::connect(dominance, SIGNAL(editTextChanged(QString)), this, SLOT(changeName(QString)));
    QObject::connect(axisNumber, SIGNAL(textChanged(QString)), this, SLOT(changeName(QString)));
    QObject::connect(validate, SIGNAL(clicked()), this, SLOT(validation()));

    mainLayout->addWidget(treeLabel, 0, 0);
    mainLayout->addWidget(positionLabel, 1, 0);
    mainLayout->addWidget(expositionLabel, 2, 0);
    mainLayout->addWidget(dominanceLabel, 3, 0);
    mainLayout->addWidget(numberLabel, 4, 0);
    mainLayout->addWidget(nameLabel, 5, 0);
    mainLayout->addWidget(tree, 0, 1);
    mainLayout->addWidget(position, 1, 1);
    mainLayout->addWidget(exposition, 2, 1);
    mainLayout->addWidget(dominance, 3, 1);
    mainLayout->addWidget(axisNumber, 4, 1);
    mainLayout->addWidget(axisName, 5, 1);
    mainLayout->addWidget(validate, 6, 0, 1, 2);

    setLayout(mainLayout);
    setModal(true);
    show();
}

void addAxisClass::changeName(QString a)
{
    if (QObject::sender() == tree)
    {
        int pos = 0;
        if (tree->validator()->validate(a, pos) == QValidator::Acceptable)
        {
            axisName->setText(QString("%1 %2%3 %4%5").arg(tree->currentText().toUpper()).arg(position->currentText().toUpper()).arg(exposition->currentText().toUpper()).arg(dominance->currentText().toUpper()).arg(axisNumber->text()));
        }
    }else{
        axisName->setText(QString("%1 %2%3 %4%5").arg(tree->currentText().toUpper()).arg(position->currentText().toUpper()).arg(exposition->currentText().toUpper()).arg(dominance->currentText().toUpper()).arg(axisNumber->text()));
    }
}

void addAxisClass::validation()
{
    if (!axisNames.contains(axisName->text()))
    {
        if ((tree->currentText() == "") || (position->currentText() == "") || (exposition->currentText() == "") || (dominance->currentText() == "") || (axisNumber->text() == ""))
        {
            QMessageBox::critical(NULL, "Erreur", "Il faut renseigner tous les champs. Merci.");
        }else{
            treeCode = tree->currentIndex();
            bool saveTree(false);
            if (treeCode == -1)
            {
                saveTree = true;
            }else{
                if (tree->itemText(tree->currentIndex()) != tree->currentText())
                {
                    saveTree = true;
                }
            }
            int countLine(-1);
            QString cat;
            if (saveTree)
            {
                // we add the tree to the table
                QFile treeFile("bin/list1");
                QList<QString> treePosition;
                treePosition.clear();
                QString line;
                if (!treeFile.open(QIODevice::ReadWrite | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list1.");
                    urgentStop();
                    return;
                }else{
                    QTextStream treeStream(&treeFile);
                    line = treeStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list1\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = treeStream.readLine();
                        while (!treeStream.atEnd()  && line != "###")
                        {
                            if (line.split("\t").size() > 0) cat = line.split("\t").at(0);
                            treePosition.append(cat);
                            line = treeStream.readLine();
                        }
                        while(!treeStream.atEnd())
                        {
                            if (line.split("\t").size() > 0) treeCode = treeStream.readLine().split("\t").at(0).toInt();
                        }
                        treeCode++;
                        for (QList<QString>::iterator it(treePosition.begin()); it != treePosition.end(); it++)
                        {
                            //QMessageBox::information(NULL, "", *it);
                            if (*it == "Id1")
                            {
                                treeStream << treeCode;
                            }else if (*it == "Id0")
                            {
                                treeStream << plotCode;
                            }else if (*it == "NumArb")
                            {
                                treeStream << tree->currentText().remove("PA");
                            }
                            if ((*it != "") && (*it != cat))
                            {
                                treeStream << "\t";
                            }
                        }
                        treeStream << "\n";
                    }
                }
                treeFile.close();
            }else{
                treeCode = tree->itemData(treeCode).toInt();
            }
            posCode = position->currentIndex();
            bool savePos(false);
            if (posCode == -1)
            {
                savePos = true;
            }else{
                if (position->itemText(position->currentIndex()) != position->currentText())
                {
                    savePos = true;
                }
            }
            if (savePos)
            {
                // we add the position to the table
                QFile posFile("bin/dictionary/position");
                countLine = -1;
                QList<QString> posPosition;
                posPosition.clear();
                QString line;
                if (!posFile.open(QIODevice::ReadWrite | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/position.");
                    urgentStop();
                    return;
                }else{
                    QTextStream posStream(&posFile);
                    line = posStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/position\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = posStream.readLine();
                        while (!posStream.atEnd()  && line != "###")
                        {
                            if (line.split("\t").size() > 0) cat = line.split("\t").at(0);
                            posPosition.append(cat);
                            line = posStream.readLine();
                        }
                        while(!posStream.atEnd())
                        {
                            posCode = posStream.readLine().split("\t").at(0).toInt();
                        }
                        posCode++;
                        for (QList<QString>::iterator it(posPosition.begin()); it != posPosition.end(); it++)
                        {
                            if (*it == "IdPos")
                            {
                                posStream << posCode;
                            }else if (*it == "ShortNamePosition")
                            {
                                posStream << position->currentText();
                            }else if (*it == "LongNamePosition")
                            {
                                QString positionName;
                                positionName = QInputDialog::getText(NULL, "position", QString("La position \"%1\" n'existe pas dans la base de données, merci de renseigner ici à quoi correspond cette position\n(par exemple \"Bas\" pour la position \"B\")").arg(position->currentText()));
                                while (positionName.isEmpty())
                                {
                                    positionName = QInputDialog::getText(NULL, "position", QString("Ne crois pas que tu vas y échapper!\nLa position \"%1\" n'existe pas dans la base de données, merci de renseigner ici à quoi correspond cette position\n(par exemple \"Bas\" pour la position \"B\")").arg(position->currentText()));
                                }
                                posStream << positionName;
                            }
                            if ((*it != cat) && (*it != ""))
                            {
                                posStream << "\t";
                            }
                        }
                        posStream << "\n";
                    }
                }
                posFile.close();
            }else{
                posCode = position->itemData(posCode).toInt();
            }
            int expCode = exposition->currentIndex();
            bool saveExp(false);
            if (expCode == -1)
            {
                saveExp = true;
            }else{
                if (exposition->itemText(exposition->currentIndex()) != exposition->currentText())
                {
                    saveExp = true;
                }
            }
            QString line;
            if (saveExp)
            {
                // we add the exposition to the table
                QFile expFile("bin/dictionary/exposition");
                countLine = -1;
                QList<QString> expPosition;
                expPosition.clear();
                if (!expFile.open(QIODevice::ReadWrite | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/exposition.");
                    urgentStop();
                    return;
                }else{
                    QTextStream expStream(&expFile);
                    line = expStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/exposition\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = expStream.readLine();
                        while (!expStream.atEnd()  && line != "###")
                        {
                            if (line.split("\t").size() > 0) cat = line.split("\t").at(0);
                            expPosition.append(cat);
                            line = expStream.readLine();
                        }
                        while(!expStream.atEnd())
                        {
                            expCode = expStream.readLine().split("\t").at(0).toInt();
                        }
                        expCode++;
                        for (QList<QString>::iterator it(expPosition.begin()); it != expPosition.end(); it++)
                        {
                            if (*it == "IdExp")
                            {
                                expStream << expCode;
                            }else if (*it == "ShortNameExposition")
                            {
                                expStream << exposition->currentText();
                            }else if (*it == "LongNameExposition")
                            {
                                QString expositionName;
                                expositionName = QInputDialog::getText(NULL, "exposition", QString("L'exposition \"%1\" n'existe pas dans la base de données, merci de renseigner ici à quoi correspond cette exposition\n(par exemple \"Sud\" pour la position \"S\")").arg(exposition->currentText()));
                                while (expositionName.isEmpty())
                                {
                                    expositionName = QInputDialog::getText(NULL, "exposition", QString("Ne crois pas que tu vas y échapper!\nLexposition \"%1\" n'existe pas dans la base de données, merci de renseigner ici à quoi correspond cette exposition\n(par exemple \"Sud\" pour la position \"S\")").arg(exposition->currentText()));
                                }
                                expStream << expositionName;
                            }
                            if ((*it != cat) && (*it != ""))
                            {
                                expStream << "\t";
                            }
                        }
                        expStream << "\n";
                    }
                }
                expFile.close();
            }else{
                expCode = exposition->itemData(expCode).toInt();
            }
            int domCode = dominance->currentIndex();
            bool saveDom(false);
            if (domCode == -1)
            {
                saveDom = true;
            }else{
                if (dominance->itemText(dominance->currentIndex()) != dominance->currentText())
                {
                    saveDom = true;
                }
            }
            if (saveDom)
            {
                // we add the dominance to the table
                QFile domFile("bin/dictionary/dominance");
                countLine = -1;
                QList<QString> domPosition;
                domPosition.clear();
                if (!domFile.open(QIODevice::ReadWrite | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/dominance.");
                    urgentStop();
                    return;
                }else{
                    QTextStream domStream(&domFile);
                    line = domStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/dominance\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = domStream.readLine();
                        while (!domStream.atEnd()  && line != "###")
                        {
                            if (line.split("\t").size() > 0) cat = line.split("\t").at(0);
                            domPosition.append(cat);
                            line = domStream.readLine();
                        }
                        while(!domStream.atEnd())
                        {
                            domCode = domStream.readLine().split("\t").at(0).toInt();
                        }
                        domCode++;
                        for (QList<QString>::iterator it(domPosition.begin()); it != domPosition.end(); it++)
                        {
                            if (*it == "IdDom")
                            {
                                domStream << domCode;
                            }else if (*it == "ShortNameDominance")
                            {
                                domStream << dominance->currentText();
                            }else if (*it == "LongNameDominance")
                            {
                                QString dominanceName;
                                dominanceName = QInputDialog::getText(NULL, "dominance", QString("La dominance \"%1\" n'existe pas dans la base de données, merci de renseigner ici à quoi correspond cette dominance\n(par exemple \"Principal\" pour la dominance \"P\")").arg(dominance->currentText()));
                                while (dominanceName.isEmpty())
                                {
                                    dominanceName = QInputDialog::getText(NULL, "dominance", QString("Ne crois pas que tu vas y échapper!\nLa dominance \"%1\" n'existe pas dans la base de données, merci de renseigner ici à quoi correspond cette dominance\n(par exemple \"Principal\" pour la dominance \"P\")").arg(dominance->currentText()));
                                }
                                domStream << dominanceName;
                            }
                            if ((*it != cat) && (*it != ""))
                            {
                                domStream << "\t";
                            }
                        }
                        domStream << "\n";
                    }
                }
                domFile.close();
            }else{
                domCode = dominance->itemData(domCode).toInt();
            }
            accept();
            // we guess that the axis is followed
            QFile statFile("bin/dictionary/status");
            countLine = -1;
            QMap<QString, int> statPosition;
            statPosition.clear();
            if (!statFile.open(QIODevice::ReadWrite | QIODevice::Text))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/status.");
                urgentStop();
                return;
            }else{
                QTextStream statStream(&statFile);
                line = statStream.readLine();
                if (line != "###")
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/status\nMauvaise entête (\"###\" attendu)");
                    urgentStop();
                    return;
                }else{
                    line = statStream.readLine();
                    while (!statStream.atEnd()  && line != "###")
                    {
                        countLine++;
                        if (line.split("\t").size() > 0 ) statPosition.insert(line.split("\t").at(0), countLine);
                        line = statStream.readLine();
                    }
                    while(!statStream.atEnd())
                    {
                        line = statStream.readLine();
                        if (line.split("\t").size() > statPosition["Followed"])
                        {
                            if (line.split("\t").at(statPosition["Followed"]) == "T")
                            {
                                statCode = line.split("\t").at(0).toInt();
                                if (line.split("\t").size() > statPosition["NameStatus"]) statusName = line.split("\t").at(statPosition["NameStatus"]);
                            }
                        }
                    }
                }
            }
            statFile.close();
            // We can now register the new axis
            QFile axisFile("bin/list2");
            countLine = -1;
            QList<QString> axisPosition;
            axisPosition.clear();
            int IdAxis;
            if (!axisFile.open(QIODevice::ReadWrite | QIODevice::Text))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list2.");
                urgentStop();
                return;
            }else{
                QTextStream axisStream(&axisFile);
                line = axisStream.readLine();
                if (line != "###")
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list2\nMauvaise entête (\"###\" attendu)");
                    urgentStop();
                    return;
                }else{
                    line = axisStream.readLine();
                    while (!axisStream.atEnd()  && line != "###")
                    {
                        if (line.split("\t").size() > 0) cat = line.split("\t").at(0);
                        axisPosition.append(cat);
                        line = axisStream.readLine();
                    }
                    while(!axisStream.atEnd())
                    {
                        IdAxis = axisStream.readLine().split("\t").at(0).toInt();
                    }
                    IdAxis++;
                    for (QList<QString>::iterator it(axisPosition.begin()); it != axisPosition.end(); it++)
                    {
                        if (*it == "Id2")
                        {
                            axisStream << IdAxis;
                        }else if (*it == "Id1")
                        {
                            axisStream << treeCode;
                        }else if (*it == "IdPos")
                        {
                            axisStream << posCode;
                        }else if (*it == "IdExp")
                        {
                            axisStream << expCode;
                        }else if (*it == "IdDom")
                        {
                            axisStream << domCode;
                        }else if (*it == "NRam")
                        {
                            axisStream << axisNumber->text();
                            nRam = axisNumber->text();
                        }else if (*it == "IdStat")
                        {
                            axisStream << statCode;
                        }else if ((*it == "ASFirst") || (*it == "ASLast"))
                        {
                            axisStream << "0";
                        }else if ((*it == "FirstObserve") || (*it == "LastObserve"))
                        {
                            axisStream << "0000.00.00";
                        }
                        if ((*it != cat) && (*it != ""))
                        {
                            axisStream << "\t";
                        }
                    }
                    axisStream << "\n";
                }
            }
            axisFile.close();
            QMessageBox::information(NULL, "Enregistré", QString("L'axe '%1' a bien été enregistré.").arg(axisName->text()));
            axisCode = IdAxis;
        }
    }else{
        QMessageBox::critical(NULL, "Erreur", "Il existe déjà un axe avec ce nom");
    }
}

void addAxisClass::setTitle(QString title)
{
    setWindowTitle(title);
}

int addAxisClass::getAxisCode()
{
    return axisCode;
}

QString addAxisClass::getAxisName()
{
    return axisName->text();
}

QString addAxisClass::getStatusName()
{
    return statusName;
}

int addAxisClass::getTreeCode()
{
    return treeCode;
}

int addAxisClass::getPosCode()
{
    return posCode;
}

int addAxisClass::getExpCode()
{
    return expCode;
}

int addAxisClass::getDomCode()
{
    return domCode;
}

QString addAxisClass::getNRam()
{
    return nRam;
}

int addAxisClass::getStatCode()
{
    return statCode;
}

QList<QVariant> addAxisClass::addAxis(QWidget *parent, QString title, int plot)
{
    addAxisClass *window = new addAxisClass(plot);
    window->setParent(parent);
    window->setTitle(title);
    QList<QVariant> value;

    if(window->exec())
    {
        value.append(QVariant(window->getAxisCode()));
        value.append(QVariant(window->getTreeCode()));
        value.append(QVariant(window->getPosCode()));
        value.append(QVariant(window->getExpCode()));
        value.append(QVariant(window->getDomCode()));
        value.append(QVariant(window->getNRam()));
        value.append(QVariant(window->getStatCode()));
        value.append(QVariant(0));
        value.append(QVariant(0));
        QDate dateZero(0, 0, 0);
        value.append(QVariant(dateZero));
        value.append(QVariant(dateZero));
        value.append(QVariant(window->getAxisName()));
        value.append(QVariant(window->getStatusName()));
        return value;
    }
    else
    {
        return value;
    }
}
