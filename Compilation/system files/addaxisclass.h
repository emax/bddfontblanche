#ifndef ADDAXISCLASS
#define ADDAXISCLASS
#include <QDialog>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QGridLayout>
#include <QList>
#include <QVariant>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QMap>
#include <QStringList>
#include <QString>
#include <QRegExp>
#include <QValidator>
#include <QInputDialog>
#include <QDate>

class addAxisClass : public QDialog
{
    Q_OBJECT

public:
    addAxisClass(int plot);
    static QList<QVariant> addAxis(QWidget *parent, QString title, int plot);
    void setTitle(QString title);
    int getAxisCode();
    QString getAxisName();
    QString getStatusName();
    int getTreeCode();
    int getPosCode();
    int getExpCode();
    int getDomCode();
    QString getNRam();
    int getStatCode();

private:
    void urgentStop();

public slots:
    void changeName(QString a);
    void validation();

private:
    QComboBox *tree;
    QComboBox *position;
    QComboBox *exposition;
    QComboBox *dominance;
    QLineEdit *axisNumber;
    QLabel *axisName;
    QPushButton *validate;
    QList<QString> axisNames;
    int plotCode;
    int axisCode;
    QString statusName;
    int treeCode;
    int posCode;
    int expCode;
    int domCode;
    QString nRam;
    int statCode;
};

#endif // ADDAXISCLASS

