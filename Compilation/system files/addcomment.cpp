#include <MainWindow.h>

void MainWindow::addComment()
{
    QComboBox *levelCB;
    QTextEdit *commentTE;
    QLineEdit *authorLE;
    QDateEdit *dateDE;
    QList<QVariant> datas(userData.toList());

    levelCB = qobject_cast<QComboBox *>(mainLayout->itemAtPosition(2, 2)->widget());
    commentTE = qobject_cast<QTextEdit *>(mainLayout->itemAtPosition(3, 2)->widget());
    authorLE = qobject_cast<QLineEdit *>(mainLayout->itemAtPosition(4, 2)->widget());
    dateDE = qobject_cast<QDateEdit *>(mainLayout->itemAtPosition(5, 2)->widget());
    QString commentText;
    commentText = commentTE->toPlainText();

    if ((levelCB->currentIndex() != -1) && (commentText != "") && (dateDE->hasAcceptableInput()))
    {
        commentText.remove("\t");
        commentText.remove("\n");
        QFile commentFile("bin/comment");
        QString line;
        int countLine(0);
        QStringList dataRow;
        QMap<int, QString> commentPosition;
        QList<int> commentId;
        int Id(-1);
        if (!commentFile.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/comment.");
            urgentStop();
            return;
        }else{
            QTextStream commentStream(&commentFile);
            line = commentStream.readLine();
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/comment\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!commentStream.atEnd()  && line != "###")
                {
                    line = commentStream.readLine();
                    commentPosition.insert(countLine, line.split("\t").at(0));
                    countLine++;
                }
                while(!commentStream.atEnd())
                {
                    dataRow = commentStream.readLine().split("\t");
                    Id = dataRow.at(0).toInt();
                    commentId.append(Id);
                }
                qSort(commentId.begin(), commentId.end());
                if (Id != -1)
                {
                    Id = commentId.last() + 1;
                }else{
                    Id = 0;
                }
                commentStream << Id;
                for (QMap<int, QString>::iterator it(commentPosition.begin()); it != (commentPosition.end()); it++)
                {
                    if (it.value() == "Level")
                    {
                        commentStream << "\t" << levelCB->currentIndex();
                    }else if (it.value() == "IdLev")
                    {
                        commentStream << "\t" << levelCB->itemData(levelCB->currentIndex()).toString();
                    }else if (it.value() == "DateC")
                    {
                        commentStream << "\t" << dateDE->date().toString("yyyy.MM.dd");
                    }else if (it.value() == "Comment")
                    {
                        commentStream << "\t" << commentText;
                    }else if (it.value() == "AuthorC")
                    {
                        commentStream << "\t" << authorLE->text();
                    }else if ((it.value() != "IdC") && (it.value() != "###"))
                    {
                        commentStream << "\t";
                    }
                }
                commentStream << "\n";
            }
            QTableWidget * commentTable(qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(7, 1)->widget()));
            QTableWidgetItem *cell;
            int row(commentTable->rowCount());
            commentTable->insertRow(row);
            cell = new QTableWidgetItem;
            cell->setText(dateDE->date().toString("dd-MM-yyyy"));
            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
            cell->setData(Qt::UserRole, QVariant(Id));
            commentTable->setItem(row, 0, cell);
            cell = new QTableWidgetItem;
            cell->setText(authorLE->text());
            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
            cell->setData(Qt::UserRole, QVariant(Id));
            commentTable->setItem(row, 1, cell);
            cell = new QTableWidgetItem;
            cell->setText(commentText);
            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
            cell->setData(Qt::UserRole, QVariant(Id));
            commentTable->setItem(row, 2, cell);
            commentFile.close();
            commentTE->setText("");
            if (userData.toList().size() > 2)
            {
                dateDE->setDate(userData.toList().at(2).toDate());
                authorLE->setText(userData.toList().at(1).toString());
            }
        }
    }
}
