#include <MainWindow.h>

void MainWindow::addParameter()
{
    // Empty mainLayout
    while(mainLayout->count() > 0){
        QLayoutItem *item = mainLayout->takeAt(0);
        delete item->widget();
        delete item;
    }
    delete mainLayout;
    mainLayout = new QGridLayout;
    setLayout(mainLayout);

    // We save the database
    QString userName("");
    userName = QInputDialog::getText(NULL, "Nom de l'opérateur", "Dis moi quel est ton nom?");
    userName.remove("\n");
    userName.remove("\t");
    QString folderName;
    folderName = QString("%1-%2.BNV").arg(QDateTime::currentDateTime().toString("dd.MM.yyyy.hh.mm.ss.zzz")).arg(userName);
    QDir originDir("bin"), targetDir("historic");
    targetDir.mkdir(folderName);
    targetDir.cd(folderName);
    QStringList fileList;
    fileList = originDir.entryList(QStringList(), QDir::Files);
    for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
    {
        QFile::copy(QString("bin/%1").arg(*it), QString("%1/%2").arg(targetDir.path()).arg(*it));
    }
    originDir.cd("dictionary");
    targetDir.mkdir("dictionary");
    targetDir.cd("dictionary");
    fileList = originDir.entryList(QStringList(), QDir::Files);
    for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
    {
        QFile::copy(QString("bin/dictionary/%1").arg(*it), QString("%1/%2").arg(targetDir.path()).arg(*it));
    }

    // QLabels
    QLabel *overallTitle = new QLabel("Ajouter des variables");
    QLabel *GUTitle = new QLabel("Variables d'UC");
    QLabel *reproTitle = new QLabel("Variables de reproduction");
    QLabel *newParameterTitle = new QLabel("Nouvelle variable");

    // QPushButtons
    QPushButton *addParameterButton = new QPushButton("Ajouter cette variable");
    QPushButton *backButton = new QPushButton("Retour");
    QPushButton *backToMainButton = new QPushButton("Retour à la fenêtre principale");

    // QTableWidgets
    QTableWidget *GUVarTab = new QTableWidget(0, 2);
    QTableWidget *reproVarTab = new QTableWidget(0, 2);
    QTableWidget *newVarTab = new QTableWidget(1, 12);

    // Now we fill GUVarTab
    QFile varFile("bin/dictionary/variables");
    QString line;
    int countLine(0);
    QStringList dataRow;
    int typePosition(-1);
    int namePosition(-1);
    QMap<int, QStringList> varList;
    if (!varFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/variables.");
        urgentStop();
        return;
    }else{
        QTextStream varStream(&varFile);
        line = varStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/variables\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!varStream.atEnd() && line != "###")
            {
                line = varStream.readLine();
                if (line == "Name\tSTRING")
                {
                    namePosition = countLine;
                }else if (line.startsWith("TypeVar"))
                {
                    typePosition = countLine;
                }
                countLine++;
            }
            if ((namePosition == -1) || (typePosition == -1))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de l'entête du fichier : bin/dictionary/variables\nLe nom des variables ou le type des variables n'est pas trouvé dans le fichier.");
                urgentStop();
                return;
            }else{
                while (!varStream.atEnd())
                {
                    dataRow = varStream.readLine().split("\t");
                    varList.insert(dataRow.at(0).toInt(), dataRow);
                }
            }
        }
        varFile.close();
    }
    QTableWidgetItem *cell;
    int row(-1);
    for (QMap<int, QStringList>::iterator it(varList.begin()); it != varList.end(); it++)
    {
        row++;
        GUVarTab->insertRow(row);
        cell = new QTableWidgetItem;
        if (it.value().size() > namePosition)
        {
            cell->setText(it.value().at(namePosition));
        }
        cell->setData(Qt::UserRole, QVariant(it.key()));
        GUVarTab->setItem(row, 0, cell);
        cell = new QTableWidgetItem;
        if (it.value().size() > typePosition)
        {
            if (it.value().at(typePosition) == "INTEGER")
            {
                cell->setText("ENTIER");
            }else if (it.value().at(typePosition) == "DOUBLE")
            {
                cell->setText("REEL");
            }else if (it.value().at(typePosition) == "STRING")
            {
                cell->setText("TEXTE");
            }else if (it.value().at(typePosition).startsWith("eKEY:"))
            {
                cell->setText("FACTORISEE (" + it.value().at(typePosition).split("eKEY:").at(1) + ")");
            }
        }
        cell->setData(Qt::UserRole, QVariant(it.key()));
        GUVarTab->setItem(row, 1, cell);
    }

    // Then we fill reproVarTab
    QFile reproVarFile("bin/dictionary/reproductionVariables");
    countLine = 0;
    typePosition = -1;
    namePosition = -1;
    QMap<int, QStringList> reproVarList;
    if (!reproVarFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/reproductionVariables.");
        urgentStop();
        return;
    }else{
        QTextStream reproVarStream(&reproVarFile);
        line = reproVarStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/reproductionVariables\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!reproVarStream.atEnd() && line != "###")
            {
                line = reproVarStream.readLine();
                if (line == "Name\tSTRING")
                {
                    namePosition = countLine;
                }else if (line.startsWith("TypeVar"))
                {
                    typePosition = countLine;
                }
                countLine++;
            }
            if ((namePosition == -1) || (typePosition == -1))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de l'entête du fichier : bin/dictionary/reproductionVariables\nLe nom des variables ou le type des variables n'est pas trouvé dans le fichier.");
                urgentStop();
                return;
            }else{
                while (!reproVarStream.atEnd())
                {
                    dataRow = reproVarStream.readLine().split("\t");
                    reproVarList.insert(dataRow.at(0).toInt(), dataRow);
                }
            }
        }
        reproVarFile.close();
    }
    row = -1;
    for (QMap<int, QStringList>::iterator it(reproVarList.begin()); it != reproVarList.end(); it++)
    {
        row++;
        reproVarTab->insertRow(row);
        cell = new QTableWidgetItem;
        if (it.value().size() > namePosition)
        {
            cell->setText(it.value().at(namePosition));
        }
        cell->setData(Qt::UserRole, QVariant(it.key()));
        reproVarTab->setItem(row, 0, cell);
        cell = new QTableWidgetItem;
        if (it.value().size() > typePosition)
        {
            if (it.value().at(typePosition) == "INTEGER")
            {
                cell->setText("ENTIER");
            }else if (it.value().at(typePosition) == "DOUBLE")
            {
                cell->setText("REEL");
            }else if (it.value().at(typePosition) == "STRING")
            {
                cell->setText("TEXTE");
            }else if (it.value().at(typePosition).startsWith("eKEY:"))
            {
                cell->setText("FACTORISEE (" + it.value().at(typePosition).split("eKEY:").at(1) + ")");
            }
        }
        cell->setData(Qt::UserRole, QVariant(it.key()));
        reproVarTab->setItem(row, 1, cell);
    }

    // Then we fill newVarTab
    QList<QString> header;
    header << "Localisation";
    QComboBox *factorEdit = new QComboBox;
    factorEdit->setToolTip("Localisation (UC ou reproduction) concernée par la variable à ajouter");
    factorEdit->insertItem(0, "UC");
    factorEdit->insertItem(1, "reproduction");
    factorEdit->setCurrentIndex(-1);
    QObject::connect(factorEdit, SIGNAL(currentIndexChanged(int)), this, SLOT(changeLocalisationParameter(int)));
    newVarTab->setCellWidget(0, 0, factorEdit);
    header << "Nom";
    QLineEdit *textEdit = new QLineEdit;
    textEdit->setToolTip("Nom de la variable utilisé pour la base de données, ne doit pas contenir d'espace, et doit être unique");
    newVarTab->setCellWidget(0, 1, textEdit);
    header << "Nom court";
    textEdit = new QLineEdit;
    textEdit->setToolTip("Nom court de la variable utilisé pour les colonnes des tableaux (doit contenir un # pour faire apparaître la mention mâle ou femelle, s'il s'agit d'une variable de reproduction)");
    newVarTab->setCellWidget(0, 2, textEdit);
    header << "Nom moyen";
    textEdit = new QLineEdit;
    textEdit->setToolTip("Nom moyen de la variable utilisé pour les fiches de terrain (doit contenir un # pour faire apparaître la mention mâle ou femelle, s'il s'agit d'une variable de reproduction)");
    newVarTab->setCellWidget(0, 3, textEdit);
    header << "Nom long";
    textEdit = new QLineEdit;
    textEdit->setToolTip("Nom long de la variable utilisé pour les exportations de données (doit contenir un # pour faire apparaître la mention mâle ou femelle, s'il s'agit d'une variable de reproduction)");
    newVarTab->setCellWidget(0, 4, textEdit);
    header << "Explication";
    textEdit = new QLineEdit;
    textEdit->setToolTip("Explicitation de la variable utilisé pour l'aide dans les tables (peut contenir un # pour faire apparaître la mention mâle ou femelle, s'il s'agit d'une variable de reproduction)");
    newVarTab->setCellWidget(0, 5, textEdit);
    header << "Type";
    factorEdit = new QComboBox;
    factorEdit->setToolTip("Type de la variable à ajouter : entier, réel (à virgule), texte ou factorisée (code à récupérer dans une liste comme un code phéno)");
    factorEdit->insertItem(0, "ENTIER");
    factorEdit->insertItem(1, "REEL");
    factorEdit->insertItem(2, "TEXTE");
    factorEdit->insertItem(3, "FACTORISEE");
    factorEdit->setCurrentIndex(-1);
    QObject::connect(factorEdit, SIGNAL(currentIndexChanged(int)), this, SLOT(changeTypeParameter(int)));
    newVarTab->setCellWidget(0, 6, factorEdit);
    header << "Sexe";
    factorEdit = new QComboBox;
    factorEdit->setToolTip("en cas de variable de reproduction, cible sexuelle (mâle, femelle ou les deux)");
    factorEdit->insertItem(0, "Les deux");
    factorEdit->insertItem(1, "Mâle");
    factorEdit->insertItem(2, "Femelle");
    factorEdit->setCurrentIndex(-1);
    newVarTab->setCellWidget(0, 7, factorEdit);
    header << "Vérification";
    factorEdit = new QComboBox;
    factorEdit->setToolTip("vérifications à faire sur la variable : croissante, décroissante ou aucun vérification");
    factorEdit->insertItem(0, "Aucune vérification");
    factorEdit->insertItem(1, "Croissante");
    factorEdit->insertItem(2, "Décroissante");
    factorEdit->setCurrentIndex(-1);
    newVarTab->setCellWidget(0, 8, factorEdit);
    header << "Minimum";
    textEdit = new QLineEdit;
    textEdit->setToolTip("valeur minimale");
    QRegExp re("#^[0-9][0-9]*[^a-Z][0-9]$|^[0-9]+.[0-9]*$#");
    QRegExpValidator *validator = new QRegExpValidator(re, this);
    textEdit->setValidator(validator);
    newVarTab->setCellWidget(0, 9, textEdit);
    header << "Maximum";
    textEdit = new QLineEdit;
    textEdit->setToolTip("valeur maximale (optionnel)");
    validator = new QRegExpValidator(re, this);
    textEdit->setValidator(validator);
    newVarTab->setCellWidget(0, 10, textEdit);
    header << "Unité";
    textEdit = new QLineEdit;
    textEdit->setToolTip("unité de la variable (optionnel)");
    newVarTab->setCellWidget(0, 11, textEdit);
    newVarTab->setHorizontalHeaderLabels(QStringList(header));

    // We connect the signals to the slots
    QObject::connect(addParameterButton, SIGNAL(clicked()), this, SLOT(addParameterSlot()));
    QObject::connect(backButton, SIGNAL(clicked()), this, SLOT(modifiesDataBase()));
    QObject::connect(backToMainButton, SIGNAL(clicked()), this, SLOT(backTo0Slot()));

    // We display the window with the widgets
    mainLayout->addWidget(overallTitle, 0, 0, 1, 2);
    mainLayout->addWidget(GUTitle, 1, 0, 1, 1);
    mainLayout->addWidget(reproTitle, 1, 1, 1, 1);
    mainLayout->addWidget(GUVarTab, 2, 0, 1, 1);
    mainLayout->addWidget(reproVarTab, 2, 1, 1, 1);
    mainLayout->addWidget(newParameterTitle, 3, 0, 1, 2);
    mainLayout->addWidget(newVarTab, 4, 0, 1, 2);
    mainLayout->addWidget(addParameterButton, 5, 0, 1, 2);
    mainLayout->addWidget(backButton, 6, 0, 1, 2);
    mainLayout->addWidget(backToMainButton, 7, 0, 1, 2);
    setObjectName("addParameter");
}
