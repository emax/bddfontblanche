#include <axis.h>


Axis::Axis()
{
    id = -1;
    treeNumber = -1;
    axisPosition = -1;
    axisDominance = -1;
    axisNumber = "";
    firstAS = -1;
    lastAS = -1;
    status = -1;
    axisName = "";
}

Axis::Axis(QString line, int idPos, int posPos, int domPos, int numPos, int fASPos, int lASPos, int statPos, int fObsPos, int lObsPos)
{
    id = line.split("\t").at(idPos).toInt();
    axisPosition = line.split("\t").at(posPos).toInt();
    axisDominance= line.split("\t").at(domPos).toInt();
    axisNumber = line.split("\t").at(numPos);
    firstAS = line.split("\t").at(fASPos).toInt();
    lastAS = line.split("\t").at(lASPos).toInt();
    status = line.split("\t").at(statPos).toInt();
    QStringList dateObserve;
    dateObserve = line.split("\t").at(fObsPos).split(".");
    if (dateObserve.size() == 3)
    {
        firstObserve = QDate(dateObserve.at(0).toInt(), dateObserve.at(1).toInt(), dateObserve.at(2).toInt());
    }
    dateObserve = line.split("\t").at(lObsPos).split(".");
    if (dateObserve.size() == 3)
    {
        lastObserve = QDate(dateObserve.at(0).toInt(), dateObserve.at(1).toInt(), dateObserve.at(2).toInt());
    }
}

int Axis::getId()
{
    return id;
}

void Axis::setId(int ident)
{
    if (ident >= 0) id = ident;
}

int Axis::getTreeNumber()
{
    return treeNumber;
}

void Axis::setTreeNumber(int number)
{
    if (number >= 0) treeNumber = number;
}

int Axis::getAxisPosition()
{
    return axisPosition;
}

void Axis::setAxisPosition(int position)
{
    if (position >= 0) axisPosition = position;
}

int Axis::getAxisDominance()
{
    return axisDominance;
}

void Axis::setAxisDominance(int dominance)
{
    if (dominance >= 0) axisDominance = dominance;
}

QString Axis::getAxisNumber()
{
    return axisNumber;
}

void Axis::setAxisNumber(QString number)
{
    axisNumber = number;
}

int Axis::getFirstAS()
{
    return firstAS;
}

void Axis::setFirstAS(int AS)
{
    if (AS >= 0) firstAS = AS;
}

int Axis::getLastAS()
{
    return lastAS;
}

void Axis::setLastAS(int AS)
{
    if (AS >=0) lastAS = AS;
}

int Axis::getStatus()
{
    return status;
}

void Axis::setStatus(int stat)
{
    if (stat >= 0) status = stat;
}

QDate Axis::getFirstObserve()
{
    return firstObserve;
}

void Axis::setFirstObserve(QDate observe)
{
    firstObserve = observe;
}

QDate Axis::getLastObserve()
{
    return lastObserve;
}

void Axis::setLastObserve(QDate observe)
{
    lastObserve = observe;
}

QString Axis::getAxisName()
{
    return axisName;
}

void Axis::setAxisName(QString name)
{
    axisName = name;
}

bool operator>(Axis const& a, Axis const& b)
{
    return a.getAxisName() > b.getAxisName();
}

bool operator>=(Axis const& a, Axis const& b)
{
    return a.getAxisName() >= b.getAxisName();
}

bool operator<(Axis const& a, Axis const& b)
{
    return a.getAxisName() < b.getAxisName();
}

bool operator<=(Axis const& a, Axis const& b)
{
    return a.getAxisName() <= b.getAxisName();
}

bool operator==(Axis const& a, Axis const& b)
{
    return a.getAxisName() == b.getAxisName();
}

bool operator!=(Axis const& a, Axis const& b)
{
    return a.getAxisName() != b.getAxisName();
}
