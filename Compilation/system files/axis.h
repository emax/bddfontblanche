#ifndef AXIS
#define AXIS

#endif // AXIS
#include <QString>
#include <QDate>
#include <QStringList>

class Axis
{
public:
    Axis();
    Axis(QString line, int idPos, int posPos, int domPos, int numPos, int fASPos, int lASPos, int statPos, int fObsPos, int lObsPos);
    int getId();
    void setId(int ident);
    int getTreeNumber();
    void setTreeNumber(int number);
    int getAxisPosition();
    void setAxisPosition(int position);
    int getAxisDominance();
    void setAxisDominance(int dominance);
    QString getAxisNumber();
    void setAxisNumber(QString number);
    int getFirstAS();
    void setFirstAS(int AS);
    int getLastAS();
    void setLastAS(int AS);
    int getStatus();
    void setStatus(int stat);
    QDate getFirstObserve();
    void setFirstObserve(QDate observe);
    QDate getLastObserve();
    void setLastObserve(QDate observe);
    QString getAxisName();
    void setAxisName(QString name);

private:
    int id;
    int treeNumber;
    int axisPosition;
    int axisDominance;
    QString axisNumber;
    int firstAS;
    int lastAS;
    int status;
    QDate firstObserve;
    QDate lastObserve;
    QString axisName;
};

bool operator>(Axis const& a, Axis const& b);
bool operator>=(Axis const& a, Axis const& b);
bool operator<(Axis const& a, Axis const& b);
bool operator<=(Axis const& a, Axis const& b);
bool operator==(Axis const& a, Axis const& b);
bool operator!=(Axis const& a, Axis const& b);
