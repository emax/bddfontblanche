#include <MainWindow.h>

void MainWindow::changeComment(int a)
{
    QTableWidget *tab = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(7, 1)->widget());
    while (tab->rowCount() > 0)
    {
        tab->removeRow(0);
    }
    if (a != -1)
    {
        QComboBox *combo = qobject_cast<QComboBox *>(QObject::sender());
        int targetId;
        targetId = combo->itemData(a).toInt();
        QMap<QString, int> commentPosition;
        QMap<int, QStringList> commentMap;
        QMap<QString, int> commentNames;
        QString value;
        int Id;
        QFile commentFile("bin/comment");
        int countLine(-1);
        QString line;
        QStringList dataRow;
        if (!commentFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/comment.");
            urgentStop();
            return;
        }else{
            QTextStream commentStream(&commentFile);
            line = commentStream.readLine();
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/comment\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!commentStream.atEnd()  && line != "###")
                {
                    line = commentStream.readLine();
                    commentPosition.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
                while(!commentStream.atEnd())
                {
                    dataRow = commentStream.readLine().split("\t");
                    Id = dataRow.at(0).toInt();
                    dataRow.removeFirst();
                    if (dataRow.size() > commentPosition["Level"] && dataRow.size() > commentPosition["IdLev"]) if ((dataRow.at(commentPosition["Level"]).toInt() == a) && (dataRow.at(commentPosition["IdLev"]).toInt() == targetId))
                    {
                        commentMap.insert(Id, dataRow);
                        if (dataRow.size() > commentPosition["DateC"] && dataRow.size() > commentPosition["AuthorC"]) commentNames.insert(QString("%1 . %2").arg(dataRow.at(commentPosition["DateC"])).arg(dataRow.at(commentPosition["AuthorC"])), Id);
                    }
                }
            }
        }
        int row(0);
        QStringList temp;
        QTableWidgetItem *cell;
        for (QMap<QString, int>::iterator it(commentNames.begin()); it != commentNames.end(); it++)
        {
            cell = new QTableWidgetItem;
            temp.clear();
            if (commentMap[it.value()].size() > commentPosition["DateC"])
            {
                if (commentMap[it.value()].size() > commentPosition["DateC"]) temp = commentMap[it.value()].at(commentPosition["DateC"]).split(".");
            }else{
                temp << "0000" << "00" << "00";
            }
            if (temp.size() < 3)
            {
                value = "00-00-0000";
            }else{
                if (temp.size() > 2) value = QString("%1-%2-%3").arg(temp.at(2)).arg(temp.at(1)).arg(temp.at(0));
            }
            cell->setText(value);
            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
            cell->setData(Qt::UserRole, QVariant(it.value()));
            tab->insertRow(row);
            tab->setItem(row, 0, cell);
            cell = new QTableWidgetItem;
            if (commentMap[it.value()].size() > commentPosition["AuthorC"])
            {
                if (commentMap[it.value()].size() > commentPosition["AuthorC"]) value = commentMap[it.value()].at(commentPosition["AuthorC"]);
            }else{
                value = "";
            }
            cell->setText(value);
            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
            cell->setData(Qt::UserRole, QVariant(it.value()));
            tab->setItem(row, 1, cell);
            cell = new QTableWidgetItem;
            if (commentMap[it.value()].size() > commentPosition["Comment"])
            {
                if (commentMap[it.value()].size() > commentPosition["Comment"]) value = commentMap[it.value()].at(commentPosition["Comment"]);
            }else{
                value = "";
            }
            cell->setText(value);
            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
            cell->setData(Qt::UserRole, QVariant(it.value()));
            tab->setItem(row, 2, cell);
            row++;
        }
    }
}
