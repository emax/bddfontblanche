#include <MainWindow.h>

void MainWindow::changeDateDataConsult(int index)
{
    QWidget * dateComboBox;
    dateComboBox = mainLayout->itemAtPosition(4, 1)->widget();
    QString currentDate(qobject_cast<QComboBox *>(dateComboBox)->itemData(index).toString());
    QTableWidget * dataTable = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(5, 0)->widget());
    QTableWidgetItem * cell;
    QMap<QString, QVariant> datas;
    QMap<QString, QVariant>::iterator it;
    for (int r(0); r < dataTable->rowCount(); r++)
    {
        for (int c(1); c < dataTable->columnCount(); c++)
        {
            cell = dataTable->item(r, c);
            if (cell != 0)
            {
                cell->setText("");
                datas = cell->data(Qt::UserRole).toMap();
                if (datas.size() > 0)
                {
                    it = datas.begin();
                    while (it != datas.end() && it.key() <= currentDate)
                    {
                        cell->setText(it.value().toString());
                        it++;
                    }
                }
            }
        }
    }
}
