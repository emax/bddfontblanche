#include <changeparameterclass.h>

changeParameterClass::changeParameterClass(QVariant param, QString previousName, QDate mini): QDialog()
{
    QGridLayout *mainLayout = new QGridLayout;
    parameters = param;

    // First the QLabels titles
    QLabel *mainTitle = new QLabel("Changement de caractéristiques de l'axe");
    QLabel *prevPosTitle = new QLabel("Position précédente :");
    QLabel *currPosTitle = new QLabel("Nouvelle position :");
    QLabel *prevExpTitle = new QLabel("Exposition précédente :");
    QLabel *currExpTitle = new QLabel("Nouvelle exposition :");
    QLabel *prevDomTitle = new QLabel("Dominance précédente :");
    QLabel *currDomTitle = new QLabel("Nouvelle dominance :");
    QLabel *dateTitle = new QLabel("Date d'application :");
    QLabel *prevNameTitle = new QLabel("Nom précédent :");
    QLabel *currNameTitle = new QLabel("Nouveau nom :");

    // Then the QLabels with datas
    prevPos = new QLabel;
    prevExp = new QLabel;
    prevDom = new QLabel;
    prevName = new QLabel(previousName);

    // Then the QComboBoxes, the QLineEdit & the QDateEdit
    position = new QComboBox;
    exposition = new QComboBox;
    dominance = new QComboBox;
    axisName = new QLineEdit(previousName);
    dateEdit = new QDateEdit;
    position->setEditable(true);
    exposition->setEditable(true);
    dominance->setEditable(true);
    dateEdit->setMaximumDate(QDate::currentDate());
    dateEdit->setMinimumDate(mini);
    dateEdit->setDate(QDate::currentDate());

    // To finish the buttons Validate and Cancel
    cancel = new QPushButton("Annuler");
    validate = new QPushButton("Valider");

    // The we fill what can be filled
    // so we first read list1
    QFile treeFile("bin/list1");
    int countLine(-1);
    QMap<QString, int> treePosition;
    QMap<int, QStringList> treeMap;
    QMap<int, QString> treeNames;
    QStringList dataRow;
    int Id;
    QString line;
    if (!treeFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list1.");
        urgentStop();
        return;
    }else{
        QTextStream treeStream(&treeFile);
        line = treeStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list1\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!treeStream.atEnd()  && line != "###")
            {
                line = treeStream.readLine();
                treePosition.insert(line.split("\t").at(0), countLine);
                countLine++;
            }
            while(!treeStream.atEnd())
            {
                dataRow = treeStream.readLine().split("\t");
                Id = dataRow.at(0).toInt();
                dataRow.removeFirst();
                treeMap.insert(Id, dataRow);
                if (dataRow.size() > treePosition["NumArb"]) treeNames.insert(Id, QString("PA%1").arg(dataRow.at(treePosition["NumArb"])));
            }
        }
        // then position
        QFile posFile("bin/dictionary/position");
        countLine = -1;
        QMap<QString, int> posPosition;
        QMap<int, QStringList> posMap;
        if (!posFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/position.");
            urgentStop();
            return;
        }else{
            QTextStream posStream(&posFile);
            line = posStream.readLine();
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/position\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!posStream.atEnd()  && line != "###")
                {
                    line = posStream.readLine();
                    posPosition.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
                while(!posStream.atEnd())
                {
                    dataRow = posStream.readLine().split("\t");
                    Id = dataRow.at(0).toInt();
                    dataRow.removeFirst();
                    posMap.insert(Id, dataRow);
                }
            }
            // then exposition
            QFile expFile("bin/dictionary/exposition");
            countLine = -1;
            QMap<QString, int> expPosition;
            QMap<int, QStringList> expMap;
            if (!expFile.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/exposition.");
                urgentStop();
                return;
            }else{
                QTextStream expStream(&expFile);
                line = expStream.readLine();
                if (line != "###")
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/exposition\nMauvaise entête (\"###\" attendu)");
                    urgentStop();
                    return;
                }else{
                    line = "";
                    while (!expStream.atEnd()  && line != "###")
                    {
                        line = expStream.readLine();
                        expPosition.insert(line.split("\t").at(0), countLine);
                        countLine++;
                    }
                    while(!expStream.atEnd())
                    {
                        dataRow = expStream.readLine().split("\t");
                        Id = dataRow.at(0).toInt();
                        dataRow.removeFirst();
                        expMap.insert(Id, dataRow);
                    }
                }
                // then dominance
                QFile domFile("bin/dictionary/dominance");
                countLine = -1;
                QMap<QString, int> domPosition;
                QMap<int, QStringList> domMap;
                if (!domFile.open(QIODevice::ReadOnly | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/dominance.");
                    urgentStop();
                    return;
                }else{
                    QTextStream domStream(&domFile);
                    line = domStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/dominance\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = "";
                        while (!domStream.atEnd()  && line != "###")
                        {
                            line = domStream.readLine();
                            domPosition.insert(line.split("\t").at(0), countLine);
                            countLine++;
                        }
                        while(!domStream.atEnd())
                        {
                            dataRow = domStream.readLine().split("\t");
                            Id = dataRow.at(0).toInt();
                            dataRow.removeFirst();
                            domMap.insert(Id, dataRow);
                        }
                    }
                    // then list2
                    QFile axisFile("bin/list2");
                    countLine = -1;
                    QMap<QString, int> axisPosition;
                    QMap<int, QStringList> axisMap;
                    if (!axisFile.open(QIODevice::ReadOnly | QIODevice::Text))
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list2.");
                        urgentStop();
                        return;
                    }else{
                        QTextStream axisStream(&axisFile);
                        line = axisStream.readLine();
                        if (line != "###")
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list2\nMauvaise entête (\"###\" attendu)");
                            urgentStop();
                            return;
                        }else{
                            line = "";
                            while (!axisStream.atEnd()  && line != "###")
                            {
                                line = axisStream.readLine();
                                axisPosition.insert(line.split("\t").at(0), countLine);
                                countLine++;
                            }
                            while(!axisStream.atEnd())
                            {
                                dataRow = axisStream.readLine().split("\t");
                                Id = dataRow.at(0).toInt();
                                dataRow.removeFirst();
                                if (dataRow.size() > axisPosition["Id1"])
                                {
                                    if (treeMap.contains(dataRow.at(axisPosition["Id1"]).toInt()))
                                    {
                                        axisMap.insert(Id, dataRow);
                                    }
                                }
                                if (dataRow.size() > axisPosition["Id1"] && dataRow.size() > axisPosition["IdPos"] && dataRow.size() > axisPosition["IdExp"] && dataRow.size() > axisPosition["IdDom"] && dataRow.size() > axisPosition["NRam"])
                                {
                                    if (posMap[dataRow.at(axisPosition["IdPos"]).toInt()].size() > posPosition["ShortNamePosition"] && expMap[dataRow.at(axisPosition["IdExp"]).toInt()].size() > expPosition["ShortNameExposition"] && domMap[dataRow.at(axisPosition["IdDom"]).toInt()].size() > domPosition["ShortNameDominance"])
                                    {
                                        axisNames.append(QString("%1 %2%3 %4%5").arg(treeNames[dataRow.at(axisPosition["Id1"]).toInt()]).arg(posMap[dataRow.at(axisPosition["IdPos"]).toInt()].at(posPosition["ShortNamePosition"])).arg(expMap[dataRow.at(axisPosition["IdExp"]).toInt()].at(expPosition["ShortNameExposition"])).arg(domMap[dataRow.at(axisPosition["IdDom"]).toInt()].at(domPosition["ShortNameDominance"])).arg(dataRow.at(axisPosition["NRam"])));
                                    }
                                }
                            }
                        }
                        // -> now we load axis names from axischaracteristics
                        QFile paramFile("bin/axisCharacteristics");
                        QMap<QString, int> charactPosition;
                        countLine = -1;
                        if (!paramFile.open(QIODevice::ReadOnly | QIODevice::Text))
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/axisCharacteristics.");
                            urgentStop();
                            return;
                        }else{
                            QTextStream paramStream(&paramFile);
                            line = paramStream.readLine();
                            if (line != "###")
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/axisCharacteristics\nMauvaise entête (\"###\" attendu)");
                                urgentStop();
                                return;
                            }else{
                                line = "";
                                while (!paramStream.atEnd()  && line != "###")
                                {
                                    line = paramStream.readLine();
                                    if (line != "###")
                                    {
                                        charactPosition.insert(line.split("\t").at(0), countLine);
                                        countLine++;
                                    }
                                }
                                while(!paramStream.atEnd())
                                {
                                    dataRow = paramStream.readLine().split("\t");
                                    dataRow.removeFirst();
                                    if (dataRow.size() > charactPosition["CurrName"]) axisNames.append(dataRow.at(charactPosition["CurrName"]));
                                }
                            }
                            paramFile.close();
                        }
                        // Now we can fill the comboboxes
                        position->setInsertPolicy(QComboBox::InsertAtTop);
                        for (QMap<int, QStringList>::iterator it(posMap.begin()); it != posMap.end(); it++)
                        {
                            position->addItem(it->at(posPosition["ShortNamePosition"]), QVariant(it.key()));
                        }
                        if (parameters.toList().size() > 2)
                        {
                            if (posMap[parameters.toList().at(2).toInt()].size() > posPosition["ShortNamePosition"]) prevPos->setText(posMap[parameters.toList().at(2).toInt()].at(posPosition["ShortNamePosition"]));
                            position->setCurrentIndex(parameters.toList().at(2).toInt());
                        }
                        position->model()->sort(0);
                        exposition->setInsertPolicy(QComboBox::InsertAtTop);
                        for (QMap<int, QStringList>::iterator it(expMap.begin()); it != expMap.end(); it++)
                        {
                            exposition->addItem(it->at(expPosition["ShortNameExposition"]), QVariant(it.key()));
                        }
                        if (parameters.toList().size() > 3) exposition->setCurrentIndex(parameters.toList().at(3).toInt());
                        exposition->model()->sort(0);
                        if (parameters.toList().size() > 3)
                        {
                            if (expMap[parameters.toList().at(3).toInt()].size() > posPosition["ShortNameExposition"]) prevExp->setText(expMap[parameters.toList().at(3).toInt()].at(posPosition["ShortNameExposition"]));
                        }
                        dominance->setInsertPolicy(QComboBox::InsertAtTop);
                        for (QMap<int, QStringList>::iterator it(domMap.begin()); it != domMap.end(); it++)
                        {
                            if (it->size() > domPosition["ShortNameDominance"]) dominance->addItem(it->at(domPosition["ShortNameDominance"]), QVariant(it.key()));
                        }
                        if (parameters.toList().size() > 4) dominance->setCurrentIndex(parameters.toList().at(4).toInt());
                        dominance->model()->sort(0);
                        if (parameters.toList().size() > 4)
                        {
                            if (domMap[parameters.toList().at(4).toInt()].size() > posPosition["ShortNameDominance"]) prevDom->setText(domMap[parameters.toList().at(4).toInt()].at(posPosition["ShortNameDominance"]));
                        }
                    }
                    axisFile.close();
                }
                domFile.close();
            }
            expFile.close();
        }
        posFile.close();
    }
    treeFile.close();

    // Connect the signals to slots
    /*QRegExp rx("^PA" + parameters.toList().at(5).toString());
    QValidator *validator = new QRegExpValidator(rx, this);
    axisName->setValidator(validator);*/
    QObject::connect(validate, SIGNAL(clicked()), this, SLOT(validation()));
    QObject::connect(cancel, SIGNAL(clicked()), this, SLOT(cancelling()));

    // We put the widgets in the layout
    mainLayout->addWidget(mainTitle, 0, 0, 1, 4);
    mainLayout->addWidget(prevPosTitle, 1, 0);
    mainLayout->addWidget(prevPos, 1, 1);
    mainLayout->addWidget(currPosTitle, 1, 2);
    mainLayout->addWidget(position, 1, 3);
    mainLayout->addWidget(prevExpTitle, 2, 0);
    mainLayout->addWidget(prevExp, 2, 1);
    mainLayout->addWidget(currExpTitle, 2, 2);
    mainLayout->addWidget(exposition, 2, 3);
    mainLayout->addWidget(prevDomTitle, 3, 0);
    mainLayout->addWidget(prevDom, 3, 1);
    mainLayout->addWidget(currDomTitle, 3, 2);
    mainLayout->addWidget(dominance, 3, 3);
    mainLayout->addWidget(prevNameTitle, 4, 0);
    mainLayout->addWidget(prevName, 4, 1);
    mainLayout->addWidget(currNameTitle, 4, 2);
    mainLayout->addWidget(axisName, 4, 3);
    mainLayout->addWidget(dateTitle, 5, 0, 1, 2);
    mainLayout->addWidget(dateEdit, 5, 2, 1, 2);
    mainLayout->addWidget(cancel, 6, 0, 1, 2);
    mainLayout->addWidget(validate, 6, 2, 1, 2);

    setLayout(mainLayout);
    setModal(true);
    show();
}

void changeParameterClass::validation()
{
    if (!(axisNames.contains(axisName->text())) || (axisName->text() == prevName->text()))
    {
        if ((position->currentText() == "") || (exposition->currentText() == "") || (dominance->currentText() == "") || (axisName->text() == ""))
        {
            QMessageBox::critical(NULL, "Erreur", "Il faut renseigner tous les champs. Merci.");
        }else{
            posCode = position->currentIndex();
            bool savePos(false);
            if (posCode == -1)
            {
                savePos = true;
            }else{
                if (position->itemText(position->currentIndex()) != position->currentText())
                {
                    savePos = true;
                }
            }
            int countLine;
            QString cat;
            if (savePos)
            {
                // we add the position to the table
                QFile posFile("bin/dictionary/position");
                countLine = -1;
                QList<QString> posPosition;
                posPosition.clear();
                QString line;
                if (!posFile.open(QIODevice::ReadWrite | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/position.");
                    urgentStop();
                    return;
                }else{
                    QTextStream posStream(&posFile);
                    line = posStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/position\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = posStream.readLine();
                        while (!posStream.atEnd()  && line != "###")
                        {
                            cat = line.split("\t").at(0);
                            posPosition.append(cat);
                            line = posStream.readLine();
                        }
                        while(!posStream.atEnd())
                        {
                            posCode = posStream.readLine().split("\t").at(0).toInt();
                        }
                        posCode++;
                        for (QList<QString>::iterator it(posPosition.begin()); it != posPosition.end(); it++)
                        {
                            if (*it == "IdPos")
                            {
                                posStream << posCode;
                            }else if (*it == "ShortNamePosition")
                            {
                                posStream << position->currentText();
                            }else if (*it == "LongNamePosition")
                            {
                                QString positionName;
                                positionName = QInputDialog::getText(NULL, "position", QString("La position \"%1\" n'existe pas dans la base de données, merci de renseigner ici à quoi correspond cette position\n(par exemple \"Bas\" pour la position \"B\")").arg(position->currentText()));
                                while (positionName.isEmpty())
                                {
                                    positionName = QInputDialog::getText(NULL, "position", QString("Ne crois pas que tu vas y échapper!\nLa position \"%1\" n'existe pas dans la base de données, merci de renseigner ici à quoi correspond cette position\n(par exemple \"Bas\" pour la position \"B\")").arg(position->currentText()));
                                }
                                posStream << positionName;
                            }
                            if ((*it != cat) && (*it != ""))
                            {
                                posStream << "\t";
                            }
                        }
                        posStream << "\n";
                    }
                }
                posFile.close();
            }else{
                posCode = position->itemData(posCode).toInt();
            }
            expCode = exposition->currentIndex();
            bool saveExp(false);
            if (expCode == -1)
            {
                saveExp = true;
            }else{
                if (exposition->itemText(exposition->currentIndex()) != exposition->currentText())
                {
                    saveExp = true;
                }
            }
            QString line;
            if (saveExp)
            {
                // we add the exposition to the table
                QFile expFile("bin/dictionary/exposition");
                countLine = -1;
                QList<QString> expPosition;
                expPosition.clear();
                if (!expFile.open(QIODevice::ReadWrite | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/exposition.");
                    urgentStop();
                    return;
                }else{
                    QTextStream expStream(&expFile);
                    line = expStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/exposition\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = expStream.readLine();
                        while (!expStream.atEnd()  && line != "###")
                        {
                            cat = line.split("\t").at(0);
                            expPosition.append(cat);
                            line = expStream.readLine();
                        }
                        while(!expStream.atEnd())
                        {
                            expCode = expStream.readLine().split("\t").at(0).toInt();
                        }
                        expCode++;
                        for (QList<QString>::iterator it(expPosition.begin()); it != expPosition.end(); it++)
                        {
                            if (*it == "IdExp")
                            {
                                expStream << expCode;
                            }else if (*it == "ShortNameExposition")
                            {
                                expStream << exposition->currentText();
                            }else if (*it == "LongNameExposition")
                            {
                                QString expositionName;
                                expositionName = QInputDialog::getText(NULL, "exposition", QString("L'exposition \"%1\" n'existe pas dans la base de données, merci de renseigner ici à quoi correspond cette exposition\n(par exemple \"Sud\" pour la position \"S\")").arg(exposition->currentText()));
                                while (expositionName.isEmpty())
                                {
                                    expositionName = QInputDialog::getText(NULL, "exposition", QString("Ne crois pas que tu vas y échapper!\nLexposition \"%1\" n'existe pas dans la base de données, merci de renseigner ici à quoi correspond cette exposition\n(par exemple \"Sud\" pour la position \"S\")").arg(exposition->currentText()));
                                }
                                expStream << expositionName;
                            }
                            if ((*it != cat) && (*it != ""))
                            {
                                expStream << "\t";
                            }
                        }
                        expStream << "\n";
                    }
                }
                expFile.close();
            }else{
                expCode = exposition->itemData(expCode).toInt();
            }
            domCode = dominance->currentIndex();
            bool saveDom(false);
            if (domCode == -1)
            {
                saveDom = true;
            }else{
                if (dominance->itemText(dominance->currentIndex()) != dominance->currentText())
                {
                    saveDom = true;
                }
            }
            if (saveDom)
            {
                // we add the dominance to the table
                QFile domFile("bin/dictionary/dominance");
                countLine = -1;
                QList<QString> domPosition;
                domPosition.clear();
                if (!domFile.open(QIODevice::ReadWrite | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/dominance.");
                    urgentStop();
                    return;
                }else{
                    QTextStream domStream(&domFile);
                    line = domStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/dominance\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = domStream.readLine();
                        while (!domStream.atEnd()  && line != "###")
                        {
                            cat = line.split("\t").at(0);
                            domPosition.append(cat);
                            line = domStream.readLine();
                        }
                        while(!domStream.atEnd())
                        {
                            domCode = domStream.readLine().split("\t").at(0).toInt();
                        }
                        domCode++;
                        for (QList<QString>::iterator it(domPosition.begin()); it != domPosition.end(); it++)
                        {
                            if (*it == "IdDom")
                            {
                                domStream << domCode;
                            }else if (*it == "ShortNameDominance")
                            {
                                domStream << dominance->currentText();
                            }else if (*it == "LongNameDominance")
                            {
                                QString dominanceName;
                                dominanceName = QInputDialog::getText(NULL, "dominance", QString("La dominance \"%1\" n'existe pas dans la base de données, merci de renseigner ici à quoi correspond cette dominance\n(par exemple \"Principal\" pour la dominance \"P\")").arg(dominance->currentText()));
                                while (dominanceName.isEmpty())
                                {
                                    dominanceName = QInputDialog::getText(NULL, "dominance", QString("Ne crois pas que tu vas y échapper!\nLa dominance \"%1\" n'existe pas dans la base de données, merci de renseigner ici à quoi correspond cette dominance\n(par exemple \"Principal\" pour la dominance \"P\")").arg(dominance->currentText()));
                                }
                                domStream << dominanceName;
                            }
                            if ((*it != cat) && (*it != ""))
                            {
                                domStream << "\t";
                            }
                        }
                        domStream << "\n";
                    }
                }
                domFile.close();
            }else{
                domCode = dominance->itemData(domCode).toInt();
            }
            accept();
            // We can now register the new parameters
            QFile axisFile("bin/axisCharacteristics");
            countLine = -1;
            QList<QString> axisPosition;
            axisPosition.clear();
            int IdAxis(-1);
            if (!axisFile.open(QIODevice::ReadWrite | QIODevice::Text))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/axisCharacteristics.");
                urgentStop();
                return;
            }else{
                QTextStream axisStream(&axisFile);
                line = axisStream.readLine();
                if (line != "###")
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/axisCharacteristics\nMauvaise entête (\"###\" attendu)");
                    urgentStop();
                    return;
                }else{
                    line = axisStream.readLine();
                    while (!axisStream.atEnd()  && line != "###")
                    {
                        cat = line.split("\t").at(0);
                        axisPosition.append(cat);
                        line = axisStream.readLine();
                    }
                    while(!axisStream.atEnd())
                    {
                        IdAxis = axisStream.readLine().split("\t").at(0).toInt();
                    }
                    IdAxis++;
                    for (QList<QString>::iterator it(axisPosition.begin()); it != axisPosition.end(); it++)
                    {
                        if (*it == "IdAC")
                        {
                            axisStream << IdAxis;
                        }else if (*it == "Id2")
                        {
                            axisStream << parameters.toList().at(0).toInt();
                        }else if (*it == "PrevPos")
                        {
                            if (parameters.toList().size() > 2) axisStream << parameters.toList().at(2).toInt();
                        }else if (*it == "CurrPos")
                        {
                            axisStream << posCode;
                        }else if (*it == "PrevExp")
                        {
                            if (parameters.toList().size() > 3) axisStream << parameters.toList().at(3).toInt();
                        }else if (*it == "CurrExp")
                        {
                            axisStream << expCode;
                        }else if (*it == "PrevDom")
                        {
                            if (parameters.toList().size() > 4) axisStream << parameters.toList().at(4).toInt();
                        }else if (*it == "CurrDom")
                        {
                            axisStream << domCode;
                        }else if (*it == "PrevName")
                        {
                            axisStream << prevName->text();
                        }else if (*it == "CurrName")
                        {
                            axisStream << axisName->text();
                        }else if (*it == "Date")
                        {
                            axisStream << dateEdit->date().toString("yyyy.MM.dd");
                        }
                        if ((*it != cat) && (*it != ""))
                        {
                            axisStream << "\t";
                        }
                    }
                    axisStream << "\n";
                }
            }
            axisFile.close();
            QMessageBox::information(NULL, "Enregistré", QString("La modification dans les caractéristiques de l'axe a bien été enregistrée."));
            paramCode = IdAxis;
        }
    }else{
        QMessageBox::critical(NULL, "Erreur", "Il existe déjà un axe avec ce nom");
    }
}

QString changeParameterClass::getAxisName()
{
    return axisName->text();
}

QString changeParameterClass::getPosCode()
{
    return position->currentText();
}

QString changeParameterClass::getExpCode()
{
    return exposition->currentText();
}

QString changeParameterClass::getDomCode()
{
    return dominance->currentText();
}

QString changeParameterClass::getPrevPosCode()
{
    return prevPos->text();
}

QString changeParameterClass::getPrevExpCode()
{
    return prevExp->text();
}

QString changeParameterClass::getPrevDomCode()
{
    return prevDom->text();
}

int changeParameterClass::getParamCode()
{
    return paramCode;
}

QDate changeParameterClass::getDate()
{
    return dateEdit->date();
}

void changeParameterClass::cancelling()
{
    urgentStop();
}

QList<QVariant> changeParameterClass::newParameters(QWidget *parent, QVariant param, QString previousName, QDate mini)
{
    changeParameterClass *window = new changeParameterClass(param, previousName, mini);
    window->setParent(parent);
    QList<QVariant> value;

    if(window->exec())
    {
        value.append(QVariant(window->getParamCode()));
        value.append(QVariant(window->getPrevPosCode()));
        value.append(QVariant(window->getPosCode()));
        value.append(QVariant(window->getPrevExpCode()));
        value.append(QVariant(window->getExpCode()));
        value.append(QVariant(window->getPrevDomCode()));
        value.append(QVariant(window->getDomCode()));
        value.append(QVariant(previousName));
        value.append(QVariant(window->getAxisName()));
        value.append(QVariant(window->getDate()));
        return value;
    }
    else
    {
        return value;
    }
}
