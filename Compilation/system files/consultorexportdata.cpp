#include "MainWindow.h"

void MainWindow::consultOrExportData()
{
    // Empty mainLayout
    while(mainLayout->count() > 0){
        QLayoutItem *item = mainLayout->takeAt(0);
        delete item->widget();
        delete item;
    }
    delete mainLayout;
    mainLayout = new QGridLayout;
    setLayout(mainLayout);

    // 6 buttons to create
    QPushButton *controlLabel = new QPushButton;
    QPushButton *rainExclusionLabel = new QPushButton;
    QPushButton *lightExclusionLabel = new QPushButton;
    QPushButton *irrigationLabel = new QPushButton;
    QPushButton *exportDatas = new QPushButton;
    QPushButton *backButton = new QPushButton;

    // Labels for the buttons
    controlLabel->setText("Témoin");
    rainExclusionLabel->setText("Exclusion");
    lightExclusionLabel->setText("Renversé");
    irrigationLabel->setText("Irrigation");
    exportDatas->setText("Exporter les données");
    backButton->setText("Retour");
    controlLabel->setFlat(true);
    rainExclusionLabel->setFlat(true);
    lightExclusionLabel->setFlat(true);
    irrigationLabel->setFlat(true);

    // 4 plots button need to have a name
    controlLabel->setObjectName("Témoin");
    rainExclusionLabel->setObjectName("Exclusion");
    lightExclusionLabel->setObjectName("Renversé");
    irrigationLabel->setObjectName("Irrigation");

    // Connect slots to signals clicked
    QObject::connect(controlLabel, SIGNAL(clicked()), this, SLOT(consultData()));
    QObject::connect(rainExclusionLabel, SIGNAL(clicked()), this, SLOT(consultData()));
    QObject::connect(lightExclusionLabel, SIGNAL(clicked()), this, SLOT(consultData()));
    QObject::connect(irrigationLabel, SIGNAL(clicked()), this, SLOT(consultData()));
    QObject::connect(exportDatas, SIGNAL(clicked()), this, SLOT(exportDatasSlot()));
    QObject::connect(backButton, SIGNAL(clicked()), this, SLOT(backTo0Slot()));

    //Display buttons into the mainLayout
    mainLayout->addWidget(controlLabel, 0, 0);
    mainLayout->addWidget(rainExclusionLabel, 1, 0);
    mainLayout->addWidget(lightExclusionLabel, 0, 1);
    mainLayout->addWidget(irrigationLabel, 1, 1);
    mainLayout->addWidget(exportDatas, 2, 0, 1, 2);
    mainLayout->addWidget(backButton, 3, 0, 1, 2);

    setObjectName("consultOrExportData");
}
