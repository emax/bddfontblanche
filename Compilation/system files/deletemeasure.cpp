#include <MainWindow.h>

void MainWindow::deleteMeasure()
{
    if (QMessageBox::critical(NULL, "Confirmation", "Merci de me confirmer la suppression de cet enregistrement.", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
    {
        QString fileName;
        int Id, row;
        fileName = QObject::sender()->objectName();
        fileName.remove("Row");
        row = fileName.toInt();
        Id = qobject_cast<QTableWidget *>(QObject::sender()->parent()->parent())->item(row, 0)->data(Qt::UserRole).toInt();
        if (QObject::sender()->parent()->parent()->objectName() == "GUTable")
        {
            fileName = "bin/measure";
        }else{
            fileName = "bin/reproductionMeasure";
        }

        // Now we can delete the line
        QFile measureFile(fileName);
        QString s;
        QStringList dataRow;
        s = "";
        if (!measureFile.open(QIODevice::ReadWrite | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : " + fileName + ".");
            urgentStop();
            return;
        }else{
            QTextStream measureStream(&measureFile);
            while (!measureStream.atEnd())
            {
                dataRow = measureStream.readLine().split("\t");
                if (dataRow.at(0).toInt() != Id)
                {
                    s += dataRow.join("\t") + "\n";
                }
            }
            measureFile.resize(0);
            measureStream << s;
            measureFile.close();
        }
        qobject_cast<QTableWidget *>(QObject::sender()->parent()->parent())->removeRow(row);
    }
}
