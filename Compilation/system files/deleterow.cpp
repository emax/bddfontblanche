#include <MainWindow.h>

void MainWindow::deleteRow()
{
    if (mainLayout->rowCount() == 15)
    {
        QTableWidget *dataTable = qobject_cast<QTableWidget*>(mainLayout->itemAtPosition(11, 0)->widget());
        int row(dataTable->currentRow());
        if ((dataTable->hasFocus()) && (row != -1))
        {
            if ((dataTable->currentColumn() == 0) && (dataTable->item(row, 0)->font().italic()))
            {
                QFont font;
                if (row == 0)
                {
                    if (dataTable->rowCount() == 1)
                    {
                    }else{
                        if (dataTable->item(1, 0)->text().startsWith("----"))
                        {
                            // we have to update first the name of GUs below
                            for (int r(row + 1); r < dataTable->rowCount(); r++)
                            {
                                dataTable->item(r, 0)->setText("---- . -" + QString::number(r));
                                font = dataTable->item(r, 0)->font();
                                font.setBold(true);
                                dataTable->item(r, 0)->setFont(font);
                            }
                        }else{
                        }
                    }
                }else{
                    if (row == (dataTable->rowCount() - 1))
                    {
                        if (dataTable->item(row, 0)->text().startsWith("----"))
                        {
                        }else{
                            int r(row - 1), year, count(0);
                            year = dataTable->item(row, 0)->text().split(" . ").at(0).toInt();
                            bool finish(false);
                            if (r == 0) finish = true;
                            while (!finish)
                            {
                                if (dataTable->item(r, 0)->text().split(" . ").at(0).toInt() == year)
                                {
                                    count++;
                                    dataTable->item(r, 0)->setText(QString::number(year) + " . " + QString::number(count));
                                    font = dataTable->item(r, 0)->font();
                                    font.setBold(true);
                                    dataTable->item(r, 0)->setFont(font);
                                }else{
                                    finish = true;
                                }
                                r--;
                                if (r == 0) finish = true;
                            }
                        }
                    }else{
                        if (dataTable->item(row, 0)->text().startsWith("----"))
                        {
                            int count;
                            if (dataTable->item(row, 0)->text().split(" . ").size() > 1) count = dataTable->item(row, 0)->text().split(" . ").at(1).toInt();
                            for (int r(row + 1); r < dataTable->rowCount(); r++)
                            {
                                dataTable->item(r, 0)->setText("---- . " + QString::number(count));
                                font = dataTable->item(r, 0)->font();
                                font.setBold(true);
                                dataTable->item(r, 0)->setFont(font);
                                count--;
                            }
                        }else{
                            int r(row - 1), year, count;
                            year = dataTable->item(row, 0)->text().split(" . ").at(0).toInt();
                            if (dataTable->item(row, 0)->text().split(" . ").size() > 1) count = dataTable->item(row, 0)->text().split(" . ").at(1).toInt();
                            bool finish(false);
                            while (!finish)
                            {
                                if (dataTable->item(r, 0)->text().split(" . ").at(0).toInt() == year)
                                {
                                    dataTable->item(r, 0)->setText(QString::number(year) + " . " + QString::number(count));
                                    count++;
                                    font = dataTable->item(r, 0)->font();
                                    font.setBold(true);
                                    dataTable->item(r, 0)->setFont(font);
                                }else{
                                    finish = true;
                                }
                                r--;
                                if (r == -1) finish = true;
                            }
                        }
                    }
                }
                // we can now delete the line
                dataTable->removeRow(row);
            }
        }
    }
}
