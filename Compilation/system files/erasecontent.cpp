#include <MainWindow.h>

void MainWindow::eraseContent()
{
    if (mainLayout->rowCount() == 15)
    {
        QTableWidget *dataTable = qobject_cast<QTableWidget*>(mainLayout->itemAtPosition(11, 0)->widget());
        bool follow(false);
        if (QApplication::focusWidget()->objectName() == "dataTable") follow = true;
        if (QApplication::focusWidget()->parentWidget() != 0)
        {
            if (QApplication::focusWidget()->parentWidget()->objectName() == "dataTable") follow = true;
            if (QApplication::focusWidget()->parentWidget()->parentWidget() != 0)
            {
                if (QApplication::focusWidget()->parentWidget()->parentWidget()->objectName() == "dataTable") follow = true;
            }
        }
        if (follow)
        {
            QWidget *focused = QApplication::focusWidget();
            QString style, className;
            QMap<QString, QVariant> datas;
            className = focused->metaObject()->className();
            if (className == "QSpinBox")
            {
                style = qobject_cast<QSpinBox *>(focused)->styleSheet();
                if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                {
                    // we set the old value
                    datas = qobject_cast<QSpinBox *>(focused)->property("d").toMap();
                    if (datas.size() > 0)
                    {
                        qobject_cast<QSpinBox *>(focused)->setValue(datas[datas.keys().at(datas.size() - 1)].toString().toInt());
                    }else{
                        qobject_cast<QSpinBox *>(focused)->setValue(0);
                    }
                    // we remove the color of the cell
                    qobject_cast<QSpinBox *>(focused)->setStyleSheet("");
                }
            }else if (className == "QDoubleSpinBox")
            {
                style = qobject_cast<QDoubleSpinBox *>(focused)->styleSheet();
                if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                {
                    // we set the old value
                    datas = qobject_cast<QDoubleSpinBox *>(focused)->property("d").toMap();
                    if (datas.size() > 0)
                    {
                        qobject_cast<QDoubleSpinBox *>(focused)->setValue(datas[datas.keys().at(datas.size() - 1)].toString().toDouble());
                    }else{
                        qobject_cast<QDoubleSpinBox *>(focused)->setValue(0.0);
                    }
                    // we remove the color of the cell
                    qobject_cast<QDoubleSpinBox *>(focused)->setStyleSheet("");
                }
            }else if (className == "QComboBox")
            {
                style = qobject_cast<QComboBox *>(focused)->styleSheet();
                if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                {
                    // we set the old value
                    datas = qobject_cast<QComboBox *>(focused)->property("d").toMap();
                    if (datas.size() > 0)
                    {
                        QString value(datas[datas.keys().at(datas.size() - 1)].toString());
                        qobject_cast<QComboBox *>(focused)->setCurrentIndex(qobject_cast<QComboBox *>(focused)->findText(value));
                    }else{
                        qobject_cast<QComboBox *>(focused)->setCurrentIndex(-1);
                    }
                    // we remove the color of the cell
                    qobject_cast<QComboBox *>(focused)->setStyleSheet("");
                }
            }else if (className == "QLineEdit")
            {
                style = qobject_cast<QLineEdit *>(focused)->styleSheet();
                if (style.contains("background-color: salmon") || style.contains("background-color: greenyellow"))
                {
                    qobject_cast<QLineEdit *>(focused)->setText("");
                    qobject_cast<QLineEdit *>(focused)->setStyleSheet("");
                }
            }
        }
    }
}
