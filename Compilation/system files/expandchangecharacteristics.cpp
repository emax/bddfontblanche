#include <MainWindow.h>

void MainWindow::expandchangeCharacteristics(int a, int b)
{
    // we put the table with the datas
    QTableWidget *ParamTab;
    QPushButton *paramButton;
    QLabel *title;
    if (mainLayout->rowCount() == 5)
    {
        QWidget * buttonBackToMain = mainLayout->itemAtPosition(4, 0)->widget();
        mainLayout->removeWidget(buttonBackToMain);
        mainLayout->addWidget(buttonBackToMain, 7, 0, 1, 2);
        QWidget * buttonBackToBack = mainLayout->itemAtPosition(3, 0)->widget();
        mainLayout->removeWidget(buttonBackToBack);
        mainLayout->addWidget(buttonBackToBack, 6, 0, 1, 2);
        ParamTab = new QTableWidget(0, 10);
        ParamTab->setObjectName("ParamTable");
        mainLayout->addWidget(ParamTab, 4, 0, 1, 2);
        paramButton = new QPushButton("Changements de paramètres des axes");
        mainLayout->addWidget(paramButton, 5, 0, 1, 2);
        title = new QLabel;
        mainLayout->addWidget(title, 3, 0, 1, 2);
        QObject::connect(paramButton, SIGNAL(clicked()), this, SLOT(newParameter()));
    }else{
        ParamTab = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(4, 0)->widget());
        for (int i = ParamTab->rowCount(); i>0; i--)
        {
            ParamTab->removeRow(i - 1);
        }
        for (int j = ParamTab->columnCount(); j>0; j--)
        {
            ParamTab->removeColumn(j - 1);
        }
        title = qobject_cast<QLabel *>(mainLayout->itemAtPosition(3, 0)->widget());
        QObject::disconnect(ParamTab, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(deleteParameter(int, int)));
        ParamTab->setColumnCount(10);
    }

    // we need now to know which was the selected Axis
    int axisSelected(-1);
    axisSelected = qobject_cast<QTableWidget *>(QObject::sender())->item(a, b)->data(Qt::UserRole).toList().at(0).toInt();
    title->setText(qobject_cast<QTableWidget *>(QObject::sender())->item(a, 0)->text());
    userData = qobject_cast<QTableWidget *>(QObject::sender())->item(a, b)->data(Qt::UserRole);

    // we fill ParamTab, reading bin/axisCharacteristics
    // First we need the Position dictionary
    QFile posFile("bin/dictionary/position");
    QString line;
    int countLine(-1), Id;
    QStringList dataRow;
    QMap<QString, int> posPosition;
    QMap<int, QString> posMap;
    if (!posFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/position.");
        urgentStop();
        return;
    }else{
        QTextStream posStream(&posFile);
        line = posStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/position\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!posStream.atEnd()  && line != "###")
            {
                line = posStream.readLine();
                if (line != "###")
                {
                    posPosition.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
            }
            while(!posStream.atEnd())
            {
                dataRow = posStream.readLine().split("\t");
                Id = dataRow.at(0).toInt();
                dataRow.removeFirst();
                if (dataRow.size() > posPosition["ShortNamePosition"]) posMap.insert(Id, dataRow.at(posPosition["ShortNamePosition"]));
            }
        }
        posFile.close();
    }

    // Then the Exposition dictionary
    QFile expFile("bin/dictionary/exposition");
    countLine = -1;
    QMap<QString, int> expPosition;
    QMap<int, QString> expMap;
    if (!expFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/exposition.");
        urgentStop();
        return;
    }else{
        QTextStream expStream(&expFile);
        line = expStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/exposition\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!expStream.atEnd()  && line != "###")
            {
                line = expStream.readLine();
                if (line != "###")
                {
                    expPosition.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
            }
            while(!expStream.atEnd())
            {
                dataRow = expStream.readLine().split("\t");
                Id = dataRow.at(0).toInt();
                dataRow.removeFirst();
                if (dataRow.size() > expPosition["ShortNameExposition"]) expMap.insert(Id, dataRow.at(expPosition["ShortNameExposition"]));
            }
        }
        expFile.close();
    }

    // Then the Dominance dictionary
    QFile domFile("bin/dictionary/dominance");
    countLine = -1;
    QMap<QString, int> domPosition;
    QMap<int, QString> domMap;
    if (!domFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/dominance.");
        urgentStop();
        return;
    }else{
        QTextStream domStream(&domFile);
        line = domStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/dominance\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!domStream.atEnd()  && line != "###")
            {
                line = domStream.readLine();
                if (line != "###")
                {
                    domPosition.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
            }
            while(!domStream.atEnd())
            {
                dataRow = domStream.readLine().split("\t");
                Id = dataRow.at(0).toInt();
                dataRow.removeFirst();
                if (dataRow.size() > domPosition["ShortNameDominance"]) domMap.insert(Id, dataRow.at(domPosition["ShortNameDominance"]));
            }
        }
        domFile.close();
    }

    // Let's go
    QFile File("bin/axisCharacteristics");
    countLine = -1;
    QMap<QString, int> Position;
    QTableWidgetItem *cell;
    int row(-1);
    if (!File.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/axisCharacteristics.");
        urgentStop();
        return;
    }else{
        QTextStream Stream(&File);
        line = Stream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/axisCharacteristics\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!Stream.atEnd()  && line != "###")
            {
                line = Stream.readLine();
                if (line != "###")
                {
                    Position.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
            }
            while(!Stream.atEnd())
            {
                dataRow = Stream.readLine().split("\t");
                Id = dataRow.at(0).toInt();
                dataRow.removeFirst();
                if (dataRow.size() > 0)
                {
                    if (dataRow.size() > Position["Id2"])
                    {
                        if (dataRow.at(Position["Id2"]).toInt() == axisSelected)
                        {
                            // We add this to the table
                            row++;
                            ParamTab->insertRow(row);
                            cell = new QTableWidgetItem;
                            cell->setText(QString::number(Id));
                            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                            ParamTab->setItem(row, 0, cell);
                            cell = new QTableWidgetItem;
                            if (dataRow.size() > Position["Date"]) if (dataRow.at(Position["Date"]).split(".").size() > 2) cell->setText(QDate(dataRow.at(Position["Date"]).split(".").at(0).toInt(), dataRow.at(Position["Date"]).split(".").at(1).toInt(), dataRow.at(Position["Date"]).split(".").at(2).toInt()).toString("dd/MM/yyyy"));
                            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                            ParamTab->setItem(row, 1, cell);
                            cell = new QTableWidgetItem;
                            if (dataRow.size() > Position["PrevPos"]) cell->setText(posMap[dataRow.at(Position["PrevPos"]).toInt()]);
                            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                            ParamTab->setItem(row, 2, cell);
                            cell = new QTableWidgetItem;
                            if (dataRow.size() > Position["CurrPos"]) cell->setText(posMap[dataRow.at(Position["CurrPos"]).toInt()]);
                            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                            ParamTab->setItem(row, 3, cell);
                            cell = new QTableWidgetItem;
                            if (dataRow.size() > Position["PrevExp"]) cell->setText(expMap[dataRow.at(Position["PrevExp"]).toInt()]);
                            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                            ParamTab->setItem(row, 4, cell);
                            cell = new QTableWidgetItem;
                            if (dataRow.size() > Position["CurrExp"]) cell->setText(expMap[dataRow.at(Position["CurrExp"]).toInt()]);
                            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                            ParamTab->setItem(row, 5, cell);
                            cell = new QTableWidgetItem;
                            if (dataRow.size() > Position["PrevDom"]) cell->setText(domMap[dataRow.at(Position["PrevDom"]).toInt()]);
                            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                            ParamTab->setItem(row, 6, cell);
                            cell = new QTableWidgetItem;
                            if (dataRow.size() > Position["CurrDom"]) cell->setText(domMap[dataRow.at(Position["CurrDom"]).toInt()]);
                            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                            ParamTab->setItem(row, 7, cell);
                            cell = new QTableWidgetItem;
                            if (dataRow.size() > Position["PrevName"]) cell->setText(dataRow.at(Position["PrevName"]));
                            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                            ParamTab->setItem(row, 8, cell);
                            cell = new QTableWidgetItem;
                            if (dataRow.size() > Position["CurrName"]) cell->setText(dataRow.at(Position["CurrName"]));
                            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                            ParamTab->setItem(row, 9, cell);
                        }
                    }
                }
            }
        }
        File.close();
    }

    QList<QString> tabHeader;
    tabHeader.clear();
    tabHeader << "Id" << "Date" << "Position Préc." << "Position Nouv." << "Exposition Préc." << "Exposition Nouv." << "Dominance Préc." << "Dominance Nouv." << "Nom Préc." << "Nom Nouv.";
    ParamTab->setHorizontalHeaderLabels(QStringList(tabHeader));

    // now we connect signal to slot
    QObject::connect(ParamTab, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(deleteParameter(int, int)));
}
