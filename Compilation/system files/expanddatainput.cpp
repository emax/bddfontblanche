#include <MainWindow.h>

void MainWindow::expandDataInput(int a, int b)
{
    if (!(qobject_cast<QTableWidget *>(QObject::sender())->item(a, b)->font().bold()))
    {
        // No data were registered for the current date
        // we put the table with the datas
        QWidget *dataTab;
        QWidget *title;
        QPushButton *saveButton;
        if (mainLayout->rowCount()<=12)
        {
            QWidget * buttonBackToMain = mainLayout->itemAtPosition(10, 0)->widget();
            mainLayout->removeWidget(buttonBackToMain);
            mainLayout->addWidget(buttonBackToMain, 13, 0, 1, 4);
            QWidget * buttonBackToBack = mainLayout->itemAtPosition(11, 0)->widget();
            mainLayout->removeWidget(buttonBackToBack);
            mainLayout->addWidget(buttonBackToBack, 14, 0, 1, 4);
            dataTab = new QTableWidget(0, 0);
            dataTab->setObjectName("dataTable");
            mainLayout->addWidget(dataTab, 11, 0, 1, 4);
            qobject_cast<QTableWidget *>(dataTab)->verticalHeader()->hide();
            title = new QLabel;
            mainLayout->addWidget(title, 10, 0, 1, 4);
            saveButton = new QPushButton("Enregistrer");
            mainLayout->addWidget(saveButton, 12, 0, 1, 4);
            QShortcut* shortcut = new QShortcut(QKeySequence("Ctrl+A"), dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(modifiesInputTable()));
            shortcut = new QShortcut(Qt::Key_Delete, dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(deleteRow()));
            shortcut = new QShortcut(QKeySequence("Alt+Up"), dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(moveUp()));
            shortcut = new QShortcut(QKeySequence("Alt+Right"), dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(moveRight()));
            shortcut = new QShortcut(QKeySequence("Alt+Down"), dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(moveDown()));
            shortcut = new QShortcut(QKeySequence("Alt+Left"), dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(moveLeft()));
            shortcut = new QShortcut(Qt::Key_Escape, dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(eraseContent()));
        }else{
            dataTab = mainLayout->itemAtPosition(11, 0)->widget();
            for (int i = qobject_cast<QTableWidget *>(dataTab)->rowCount(); i>0; i--)
            {
                qobject_cast<QTableWidget *>(dataTab)->removeRow(i - 1);
            }
            for (int j = qobject_cast<QTableWidget *>(dataTab)->columnCount(); j>0; j--)
            {
                qobject_cast<QTableWidget *>(dataTab)->removeColumn(j - 1);
            }
            delete dataTab;
            dataTab = new QTableWidget(0, 0);
            dataTab->setObjectName("dataTable");
            mainLayout->addWidget(dataTab, 11, 0, 1, 4);
            qobject_cast<QTableWidget *>(dataTab)->verticalHeader()->hide();
            title = mainLayout->itemAtPosition(10, 0)->widget();
            saveButton = qobject_cast<QPushButton *>(mainLayout->itemAtPosition(12, 0)->widget());
            QShortcut* shortcut = new QShortcut(QKeySequence("Ctrl+A"), dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(modifiesInputTable()));
            shortcut = new QShortcut(Qt::Key_Delete, dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(deleteRow()));
            shortcut = new QShortcut(QKeySequence("Alt+Up"), dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(moveUp()));
            shortcut = new QShortcut(QKeySequence("Alt+Right"), dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(moveRight()));
            shortcut = new QShortcut(QKeySequence("Alt+Down"), dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(moveDown()));
            shortcut = new QShortcut(QKeySequence("Alt+Left"), dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(moveLeft()));
            shortcut = new QShortcut(Qt::Key_Escape, dataTab);
            QObject::connect(shortcut, SIGNAL(activated()), this, SLOT(eraseContent()));
            QObject::disconnect(saveButton, SIGNAL(clicked()), this, SLOT(saveRecord()));
            QObject::disconnect(qobject_cast<QTableWidget *>(dataTab), SIGNAL(cellClicked(int, int)), this, SLOT(updateCommentAndModifyMeasure(int, int)));
        }

        // we need now to know which was the selected Axis
        int axisSelected(-1);
        axisSelected = qobject_cast<QTableWidget *>(QObject::sender())->item(a, b)->data(Qt::UserRole).toList().at(0).toInt();
        QList<QVariant> datas(userData.toList());
        if (datas.size() == 3)
        {
            datas.append(QVariant(axisSelected));
        }else{
            datas.removeAt(3);
            datas.insert(3, QVariant(axisSelected));
        }
        userData = QVariant(datas);
        qobject_cast<QLabel *>(title)->setText(qobject_cast<QTableWidget *>(QObject::sender())->item(a, 0)->text());
        QFile GUFile("bin/list3");
        int countLine(-1);
        QMap<QString, int> GUPosition;
        QMap<int, QStringList> GUMap;
        QMap<QString, int> GUNames;
        QMap<int, QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > > > dataFrame;
        QString line, value;
        QStringList dataRow;
        int Id;
        if (!GUFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list3.");
            urgentStop();
            return;
        }else{
            QTextStream GUStream(&GUFile);
            line = GUStream.readLine();
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list3\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!GUStream.atEnd()  && line != "###")
                {
                    line = GUStream.readLine();
                    GUPosition.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
                while(!GUStream.atEnd())
                {
                    dataRow = GUStream.readLine().split("\t");
                    Id = dataRow.at(0).toInt();
                    dataRow.removeFirst();
                    if (dataRow.size() > GUPosition["Id2"])
                    {
                        if (dataRow.at(GUPosition["Id2"]).toInt() == axisSelected)
                        {
                            GUMap.insert(Id, dataRow);
                            dataFrame.insert(Id, *(new QPair<QMap<QString, int>, QPair<QPair<int, QMap<QString, int> >,QMap<int, QMap<QString, int> > > >));
                            dataFrame[Id].second.first.first = -1; // -1 = No male reproduction
                            if (dataRow.size() > GUPosition["Year"] && dataRow.size() > GUPosition["NGU"])
                            {
                                if ((dataRow.at(GUPosition["Year"]) == "0") || (dataRow.at(GUPosition["Year"]) == ""))
                                {
                                    GUNames.insert(QString("---- . %1").arg(dataRow.at(GUPosition["NGU"])), Id);
                                }else{
                                    GUNames.insert(QString("%1 . %2").arg(dataRow.at(GUPosition["Year"])).arg(dataRow.at(GUPosition["NGU"])), Id);
                                }
                            }
                        }
                    }
                }
            }
            // then list4
            QFile reproFile("bin/list4");
            countLine = -1;
            QMap<QString, int> reproPosition;
            QMap<int, QStringList> reproMap;
            int maxMale(0), maxFemale(0);
            if (!reproFile.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list4.");
                urgentStop();
                return;
            }else{
                QTextStream reproStream(&reproFile);
                line = reproStream.readLine();
                if (line != "###")
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list4\nMauvaise entête (\"###\" attendu)");
                    urgentStop();
                    return;
                }else{
                    line = "";
                    while (!reproStream.atEnd()  && line != "###")
                    {
                        line = reproStream.readLine();
                        reproPosition.insert(line.split("\t").at(0), countLine);
                        countLine++;
                    }
                    while(!reproStream.atEnd())
                    {
                        dataRow = reproStream.readLine().split("\t");
                        Id = dataRow.at(0).toInt();
                        dataRow.removeFirst();
                        if (dataRow.size() > reproPosition["Id3"])
                        {
                            if (GUMap.contains(dataRow.at(reproPosition["Id3"]).toInt()))
                            {
                                reproMap.insert(Id, dataRow);
                                if (dataRow.size() > reproPosition["IdSex"])
                                {
                                    if (dataRow.at(reproPosition["IdSex"]) == "1")
                                    {
                                        dataFrame[dataRow.at(reproPosition["Id3"]).toInt()].second.first.first = Id;
                                        maxMale = 1;
                                    }else{
                                        dataFrame[dataRow.at(reproPosition["Id3"]).toInt()].second.second.insert(Id, *(new QMap<QString, int>));
                                        if (dataFrame[dataRow.at(reproPosition["Id3"]).toInt()].second.second.size() > maxFemale)
                                        {
                                            maxFemale = dataFrame[dataRow.at(reproPosition["Id3"]).toInt()].second.second.size();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                // then sex
                QFile sexFile("bin/dictionary/sex");
                countLine = -1;
                QMap<QString, int> sexPosition;
                QMap<int, QStringList> sexMap;
                if (!sexFile.open(QIODevice::ReadOnly | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/sex.");
                    urgentStop();
                    return;
                }else{
                    QTextStream sexStream(&sexFile);
                    line = sexStream.readLine();
                    if (line != "###")
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/sex\nMauvaise entête (\"###\" attendu)");
                        urgentStop();
                        return;
                    }else{
                        line = "";
                        while (!sexStream.atEnd()  && line != "###")
                        {
                            line = sexStream.readLine();
                            sexPosition.insert(line.split("\t").at(0), countLine);
                            countLine++;
                        }
                        while(!sexStream.atEnd())
                        {
                            dataRow = sexStream.readLine().split("\t");
                            Id = dataRow.at(0).toInt();
                            dataRow.removeFirst();
                            sexMap.insert(Id, dataRow);
                        }
                    }
                    // then variables
                    QFile varFile("bin/dictionary/variables");
                    countLine = -1;
                    QMap<QString, int> reproVar;
                    QMap<int, QStringList> varMap;
                    QFile * tempFile;
                    QMap<QString, QMap<QString, int> > factorPosition;
                    QMap<QString, QMap<int, QStringList> > factorMap;
                    QMap<QString, int> tempPosition;
                    QMap<int, QStringList> tempMap;
                    int tempId;
                    if (!varFile.open(QIODevice::ReadOnly | QIODevice::Text))
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/variables.");
                        urgentStop();
                        return;
                    }else{
                        QTextStream varStream(&varFile);
                        line = varStream.readLine();
                        if (line != "###")
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/variables\nMauvaise entête (\"###\" attendu)");
                            urgentStop();
                            return;
                        }else{
                            line = "";
                            while (!varStream.atEnd()  && line != "###")
                            {
                                line = varStream.readLine();
                                reproVar.insert(line.split("\t").at(0), countLine);
                                countLine++;
                            }
                            while(!varStream.atEnd())
                            {
                                dataRow = varStream.readLine().split("\t");
                                Id = dataRow.at(0).toInt();
                                dataRow.removeFirst();
                                varMap.insert(Id, dataRow);
                                if (varMap[Id].size() > reproVar["TypeVar"])
                                {
                                    if (varMap[Id].at(reproVar["TypeVar"]).startsWith("eKEY"))
                                    {
                                        // factor variable
                                        if (dataRow.size() > reproVar["TypeVar"]) tempFile = new QFile(QString("bin/%1").arg(dataRow.at(reproVar["TypeVar"]).split(":").at(1)));
                                        countLine = -1;
                                        tempPosition.clear();
                                        tempMap.clear();
                                        if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                                        {
                                            // Error
                                            QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                            urgentStop();
                                            return;
                                        }else{
                                            QTextStream tempStream(tempFile);
                                            line = tempStream.readLine();
                                            if (line != "###")
                                            {
                                                // Error
                                                QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                                urgentStop();
                                                return;
                                            }else{
                                                line = "";
                                                while (!tempStream.atEnd()  && line != "###")
                                                {
                                                    line = tempStream.readLine();
                                                    tempPosition.insert(line.split("\t").at(0), countLine);
                                                    countLine++;
                                                }
                                                while(!tempStream.atEnd())
                                                {
                                                    dataRow = tempStream.readLine().split("\t");
                                                    tempId = dataRow.at(0).toInt();
                                                    dataRow.removeFirst();
                                                    tempMap.insert(tempId, dataRow);
                                                }
                                            }
                                        }
                                        tempFile->close();
                                        if (varMap[Id].size() > reproVar["Name"])
                                        {
                                            factorPosition.insert(varMap[Id].at(reproVar["Name"]), tempPosition);
                                            factorMap.insert(varMap[Id].at(reproVar["Name"]), tempMap);
                                        }
                                    }
                                }
                            }
                        }
                        // then reproduction variables
                        QFile reproVarFile("bin/dictionary/reproductionVariables");
                        countLine = -1;
                        QMap<QString, int> reproreproVar;
                        QMap<int, QStringList> reproVarMap;
                        if (!reproVarFile.open(QIODevice::ReadOnly | QIODevice::Text))
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/reproductionVariables.");
                            urgentStop();
                            return;
                        }else{
                            QTextStream reproVarStream(&reproVarFile);
                            line = reproVarStream.readLine();
                            if (line != "###")
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/reproductionVariables\nMauvaise entête (\"###\" attendu)");
                                urgentStop();
                                return;
                            }else{
                                line = "";
                                while (!reproVarStream.atEnd()  && line != "###")
                                {
                                    line = reproVarStream.readLine();
                                    reproreproVar.insert(line.split("\t").at(0), countLine);
                                    countLine++;
                                }
                                while(!reproVarStream.atEnd())
                                {
                                    dataRow = reproVarStream.readLine().split("\t");
                                    Id = dataRow.at(0).toInt();
                                    dataRow.removeFirst();
                                    reproVarMap.insert(Id, dataRow);
                                    if (reproVarMap[Id].size() > reproreproVar["TypeVar"])
                                    {
                                        if (reproVarMap[Id].at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                                        {
                                            // factor variable
                                            if (dataRow.size() > reproreproVar["TypeVar"]) if (dataRow.at(reproreproVar["TypeVar"]).split(":").size() > 1) tempFile = new QFile(QString("bin/%1").arg(dataRow.at(reproreproVar["TypeVar"]).split(":").at(1)));
                                            countLine = -1;
                                            tempPosition.clear();
                                            tempMap.clear();
                                            if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                                            {
                                                // Error
                                                QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                                urgentStop();
                                                return;
                                            }else{
                                                QTextStream tempStream(tempFile);
                                                line = tempStream.readLine();
                                                if (line != "###")
                                                {
                                                    // Error
                                                    QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                                    urgentStop();
                                                    return;
                                                }else{
                                                    line = "";
                                                    while (!tempStream.atEnd()  && line != "###")
                                                    {
                                                        line = tempStream.readLine();
                                                        tempPosition.insert(line.split("\t").at(0), countLine);
                                                        countLine++;
                                                    }
                                                    while(!tempStream.atEnd())
                                                    {
                                                        dataRow = tempStream.readLine().split("\t");
                                                        tempId = dataRow.at(0).toInt();
                                                        dataRow.removeFirst();
                                                        tempMap.insert(tempId, dataRow);
                                                    }
                                                }
                                            }
                                            tempFile->close();
                                            if (reproVarMap[Id].size() > reproreproVar["Name"])
                                            {
                                                factorPosition.insert(reproVarMap[Id].at(reproreproVar["Name"]), tempPosition);
                                                factorMap.insert(reproVarMap[Id].at(reproreproVar["Name"]), tempMap);
                                            }
                                        }
                                    }
                                }
                            }
                            // then we load the measures
                            QFile measureFile("bin/measure");
                            countLine = -1;
                            QMap<QString, int> measurePosition;
                            QMap<int, QStringList> measureMap;
                            if (!measureFile.open(QIODevice::ReadOnly | QIODevice::Text))
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/measure.");
                                urgentStop();
                                return;
                            }else{
                                QTextStream measureStream(&measureFile);
                                line = measureStream.readLine();
                                if (line != "###")
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/measure\nMauvaise entête (\"###\" attendu)");
                                    urgentStop();
                                    return;
                                }else{
                                    line = "";
                                    while (!measureStream.atEnd()  && line != "###")
                                    {
                                        line = measureStream.readLine();
                                        measurePosition.insert(line.split("\t").at(0), countLine);
                                        countLine++;
                                    }
                                    while(!measureStream.atEnd())
                                    {
                                        dataRow = measureStream.readLine().split("\t");
                                        Id = dataRow.at(0).toInt();
                                        dataRow.removeFirst();
                                        if (dataRow.size() > measurePosition["Id3"])
                                        {
                                            if (GUMap.contains(dataRow.at(measurePosition["Id3"]).toInt()))
                                            {
                                                measureMap.insert(Id, dataRow);
                                                if (dataRow.size() > measurePosition["DateM"]) dataFrame[dataRow.at(measurePosition["Id3"]).toInt()].first.insert(dataRow.at(measurePosition["DateM"]), Id);
                                            }
                                        }
                                    }
                                }
                                // and then we load the reproduction measures
                                QFile reproMeasureFile("bin/reproductionMeasure");
                                countLine = -1;
                                QMap<QString, int> reproMeasurePosition;
                                QMap<int, QStringList> reproMeasureMap;
                                if (!reproMeasureFile.open(QIODevice::ReadOnly | QIODevice::Text))
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/reproductionMeasure.");
                                    urgentStop();
                                    return;
                                }else{
                                    QTextStream reproMeasureStream(&reproMeasureFile);
                                    line = reproMeasureStream.readLine();
                                    if (line != "###")
                                    {
                                        // Error
                                        QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/reproductionMeasure\nMauvaise entête (\"###\" attendu)");
                                        urgentStop();
                                        return;
                                    }else{
                                        line = "";
                                        while (!reproMeasureStream.atEnd()  && line != "###")
                                        {
                                            line = reproMeasureStream.readLine();
                                            reproMeasurePosition.insert(line.split("\t").at(0), countLine);
                                            countLine++;
                                        }
                                        while(!reproMeasureStream.atEnd())
                                        {
                                            dataRow = reproMeasureStream.readLine().split("\t");
                                            Id = dataRow.at(0).toInt();
                                            dataRow.removeFirst();
                                            if (dataRow.size() > reproMeasurePosition["Id4"])
                                            {
                                                if (reproMap.contains(dataRow.at(reproMeasurePosition["Id4"]).toInt()))
                                                {
                                                    reproMeasureMap.insert(Id, dataRow);
                                                    if (reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].size() > reproPosition["IdSex"] && dataRow.size() > reproMeasurePosition["DateRM"])
                                                    {
                                                        if (reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["IdSex"]) == "1")
                                                        {
                                                            dataFrame[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].second.first.second.insert(dataRow.at(reproMeasurePosition["DateRM"]), Id);
                                                        }else{
                                                            dataFrame[reproMap[dataRow.at(reproMeasurePosition["Id4"]).toInt()].at(reproPosition["Id3"]).toInt()].second.second[dataRow.at(reproMeasurePosition["Id4"]).toInt()].insert(dataRow.at(reproMeasurePosition["DateRM"]), Id);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // then we can put the datas
                                    QTableWidgetItem * cell;
                                    QList<QString> tabHeader;
                                    tabHeader.clear();
                                    tabHeader << "UC";
                                    int col(0);
                                    qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                    QString varName;
                                    QLineEdit *textEdit;
                                    QSpinBox *intEdit;
                                    QDoubleSpinBox *doubleEdit;
                                    QComboBox *factorEdit;
                                    QString historic;
                                    QMap<QString, QString> factorCodes;
                                    for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                                    {
                                        if (var->size() > reproVar["AppearingVar"] && var->size() > reproVar["MediumName"])
                                        {
                                            if (var->at(reproVar["AppearingVar"]) == "Y")
                                            {
                                                col++;
                                                qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                tabHeader << var->at(reproVar["MediumName"]);
                                            }
                                        }
                                    }
                                    // then reproduction
                                    for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                    {
                                        if (it->size() > reproreproVar["Target"] && it->size() > reproreproVar["AppearingVar"])
                                        {
                                            if (((it->at(reproreproVar["Target"]) == "Both") || (it->at(reproreproVar["Target"]) == "Male")) && (it->at(reproreproVar["AppearingVar"]) == "Y"))
                                            {
                                                col++;
                                                qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                varName = it->at(reproreproVar["MediumName"]);
                                                varName.replace("#", "Male");
                                                tabHeader << varName;
                                            }
                                        }
                                    }
                                    for (int i(0); i < maxFemale; i++)
                                    {
                                        for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                        {
                                            if (it->size() > reproreproVar["Target"] && it->size() > reproreproVar["AppearingVar"])
                                            {
                                                if (((it->at(reproreproVar["Target"]) == "Both") || (it->at(reproreproVar["Target"]) == "Female")) && (it->at(reproreproVar["AppearingVar"]) == "Y"))
                                                {
                                                    col++;
                                                    qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                    varName = it->at(reproreproVar["MediumName"]);
                                                    varName.replace("#", "Fem%1");
                                                    tabHeader << varName.arg(QString::number(i + 1));
                                                }
                                            }
                                        }
                                    }
                                    if (maxFemale == 0)
                                    {
                                        for (QMap<int, QStringList>::iterator it(reproVarMap.begin()); it != reproVarMap.end(); it++)
                                        {
                                            if (it->size() > reproreproVar["Target"] && it->size() > reproreproVar["AppearingVar"])
                                            {
                                                if (((it->at(reproreproVar["Target"]) == "Both") || (it->at(reproreproVar["Target"]) == "Female")) && (it->at(reproreproVar["AppearingVar"]) == "Y"))
                                                {
                                                    col++;
                                                    qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                    varName = it->at(reproreproVar["MediumName"]);
                                                    varName.replace("#", "Fem1");
                                                    tabHeader << varName;
                                                }
                                            }
                                        }
                                    }
                                    qobject_cast<QTableWidget *>(dataTab)->setHorizontalHeaderLabels(QStringList(tabHeader));
                                    int row(0), index(0);
                                    QMap<QString, QVariant> datas;
                                    QList<QString> dates;
                                    for (QMap<QString, int>::iterator gu(GUNames.begin()); gu != GUNames.end(); gu++)
                                    {
                                        cell = new QTableWidgetItem;
                                        col = 0;
                                        cell->setData(Qt::UserRole, QVariant(gu.value()));
                                        cell->setText(gu.key());
                                        cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                                        if (gu.key().startsWith("----"))
                                        {
                                            row = qobject_cast<QTableWidget *>(dataTab)->rowCount();
                                        }else{
                                            row = 0;
                                        }
                                        qobject_cast<QTableWidget *>(dataTab)->insertRow(row);
                                        qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                        // first the measure
                                        for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                                        {
                                            if (var->size() > reproVar["AppearingVar"])
                                            {
                                                if (var->at(reproVar["AppearingVar"]) == "Y")
                                                {
                                                    col++;
                                                    value = "";
                                                    datas = *(new QMap<QString, QVariant>);
                                                    historic = "";
                                                    for (QMap<QString, int>::iterator measure(dataFrame[gu.value()].first.begin()); measure != dataFrame[gu.value()].first.end(); measure++)
                                                    {
                                                        if (var->size() > reproVar["Name"])
                                                        {
                                                            if (measureMap[measure.value()].size() > measurePosition[var->at(reproVar["Name"])])
                                                            {
                                                                if (measureMap[measure.value()].at(measurePosition[var->at(reproVar["Name"])]) != "")
                                                                {
                                                                    if (var->size() > reproVar["TypeVar"])
                                                                    {
                                                                        if ((var->at(reproVar["TypeVar"]).startsWith("eKEY")))// && (value != ""))
                                                                        {
                                                                            if (measureMap[measure.value()].size() > measurePosition[var->at(reproVar["Name"])])
                                                                            {
                                                                                if (factorMap[var->at(reproVar["Name"])][measureMap[measure.value()].at(measurePosition[var->at(reproVar["Name"])]).toInt()].size() > factorPosition[var->at(reproVar["Name"])]["Code"]) value = factorMap[var->at(reproVar["Name"])][measureMap[measure.value()].at(measurePosition[var->at(reproVar["Name"])]).toInt()].at(factorPosition[var->at(reproVar["Name"])]["Code"]);
                                                                            }
                                                                        }else{
                                                                            if (measureMap[measure.value()].size() > measurePosition[var->at(reproVar["Name"])]) value = measureMap[measure.value()].at(measurePosition[var->at(reproVar["Name"])]);
                                                                        }
                                                                    }
                                                                    if (measureMap[measure.value()].size() > measurePosition["DateM"])
                                                                    {
                                                                        historic = "\n" + measureMap[measure.value()].at(measurePosition["DateM"]) + "\t:\t" + value + historic;
                                                                        datas.insert(measureMap[measure.value()].at(measurePosition["DateM"]), value);
                                                                        if (!dates.contains(measureMap[measure.value()].at(measurePosition["DateM"])))
                                                                        {
                                                                            dates.append(measureMap[measure.value()].at(measurePosition["DateM"]));
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    historic = "%1" + historic;
                                                    if (var->size() > reproVar["TypeVar"] && var->size() > reproVar["UnitVar"] && var->size() > reproVar["IncreasingVar"] && var->size() > reproVar["DecreasingVar"] && var->size() > reproVar["MinValueVar"] && var->size() > reproVar["MaxValueVar"] && var->size() > reproVar["ExplainVar"])
                                                    {
                                                        if (var->at(reproVar["TypeVar"]) == "INTEGER")
                                                        {
                                                            //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                            intEdit = new QSpinBox;
                                                            intEdit->setMaximum(std::numeric_limits<int>::max());
                                                            intEdit->setMinimum(std::numeric_limits<int>::min());
                                                            intEdit->setProperty("d", QVariant(datas));
                                                            if (value != "") intEdit->setValue(value.toInt());
                                                            if (var->at(reproVar["UnitVar"]) != "")
                                                            {
                                                                intEdit->setSuffix(QString(" %1").arg(var->at(reproVar["UnitVar"])));
                                                            }
                                                            if (var->at(reproVar["IncreasingVar"]) == "Y")
                                                            {
                                                                intEdit->setProperty("i", QVariant("In"));
                                                            }else if (var->at(reproVar["DecreasingVar"]) == "Y")
                                                            {
                                                                intEdit->setProperty("i", QVariant("De"));
                                                            }else{
                                                                intEdit->setProperty("i", QVariant("No"));
                                                            }
                                                            if (var->at(reproVar["MinValueVar"]) != "")
                                                            {
                                                                intEdit->setMinimum(var->at(reproVar["MinValueVar"]).toInt());
                                                            }
                                                            if (var->at(reproVar["MaxValueVar"]) != "")
                                                            {
                                                                intEdit->setMaximum(var->at(reproVar["MaxValueVar"]).toInt());
                                                            }
                                                            intEdit->setProperty("e", QVariant(var->at(reproVar["ExplainVar"])));
                                                            intEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])));
                                                            intEdit->setFocusPolicy(Qt::StrongFocus);
                                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                            SelectFocus *test = new SelectFocus;
                                                            intEdit->installEventFilter(test);
                                                            QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                                            qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, intEdit);
                                                        }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                                        {
                                                            //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                            doubleEdit = new QDoubleSpinBox;
                                                            doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                                            doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                                            doubleEdit->setProperty("d", QVariant(datas));
                                                            if (value != "") doubleEdit->setValue(value.toDouble());
                                                            if (var->at(reproVar["UnitVar"]) != "")
                                                            {
                                                                doubleEdit->setSuffix(QString(" %1").arg(var->at(reproVar["UnitVar"])));
                                                            }
                                                            if (var->at(reproVar["IncreasingVar"]) == "Y")
                                                            {
                                                                doubleEdit->setProperty("i", QVariant("In"));
                                                            }else if (var->at(reproVar["DecreasingVar"]) == "Y")
                                                            {
                                                                doubleEdit->setProperty("i", QVariant("De"));
                                                            }else{
                                                                doubleEdit->setProperty("i", QVariant("No"));
                                                            }
                                                            if (var->at(reproVar["MinValueVar"]) != "")
                                                            {
                                                                doubleEdit->setMinimum(var->at(reproVar["MinValueVar"]).toDouble());
                                                            }
                                                            if (var->at(reproVar["MaxValueVar"]) != "")
                                                            {
                                                                doubleEdit->setMaximum(var->at(reproVar["MaxValueVar"]).toDouble());
                                                            }
                                                            doubleEdit->setProperty("e", QVariant(var->at(reproVar["ExplainVar"])));
                                                            doubleEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])));
                                                            SelectFocus *test = new SelectFocus;
                                                            doubleEdit->installEventFilter(test);
                                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                            QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                            qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, doubleEdit);
                                                        }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                                        {
                                                            //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                            textEdit = new QLineEdit;
                                                            textEdit->setProperty("d", QVariant(datas));
                                                            if (value != "") textEdit->setPlaceholderText(value);
                                                            textEdit->setProperty("e", QVariant(var->at(reproVar["ExplainVar"])));
                                                            textEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                            //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                            SelectFocus *test = new SelectFocus;
                                                            textEdit->installEventFilter(test);
                                                            qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, textEdit);
                                                        }else if (var->at(reproVar["TypeVar"]).startsWith("eKEY"))
                                                        {
                                                            //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                            factorEdit = new QComboBox;
                                                            factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                                            factorEdit->setEditable(true);
                                                            index = 0;
                                                            factorCodes.clear();
                                                            if (var->size() > reproVar["Name"])
                                                            {
                                                                for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproVar["Name"])].begin()); it != factorMap[var->at(reproVar["Name"])].end(); it++)
                                                                {
                                                                    //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproVar["Name"])]["Code"]));
                                                                    if (it->size() > factorPosition[var->at(reproVar["Name"])]["Code"] && it->size() > factorPosition[var->at(reproVar["Name"])]["Explain"])
                                                                    {
                                                                        factorEdit->addItem(it->at(factorPosition[var->at(reproVar["Name"])]["Code"]), QVariant(it.key()));
                                                                        if (value != "" && value == it->at(factorPosition[var->at(reproVar["Name"])]["Code"])) factorEdit->setCurrentIndex(index);
                                                                        index++;
                                                                        factorCodes.insert(it->at(factorPosition[var->at(reproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproVar["Name"])]["Explain"]));
                                                                    }
                                                                }
                                                            }
                                                            if (value == "") factorEdit->setCurrentIndex(-1);
                                                            factorEdit->model()->sort(0);
                                                            factorEdit->setProperty("d", QVariant(datas));
                                                            factorEdit->setProperty("e", QVariant(var->at(reproVar["ExplainVar"])));
                                                            historic = historic.arg(var->at(reproVar["ExplainVar"]) + "%1");
                                                            for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                                            {
                                                                historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                                            }
                                                            factorEdit->setToolTip(historic.arg("").replace("#", "Male"));
                                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                            //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                            QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                                            QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                                            qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, factorEdit);
                                                        }else{
                                                            cell = new QTableWidgetItem;
                                                            cell->setData(Qt::UserRole, QVariant(datas));
                                                            cell->setStatusTip(value);
                                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                            qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        // then reproduction
                                        if (maxMale != 0)
                                        {
                                            Id = dataFrame[gu.value()].second.first.first;
                                        }else{
                                            Id = -1;
                                        }
                                        if (Id == -1)
                                        {
                                            datas = *(new QMap<QString, QVariant>);
                                            value = "";
                                            for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                                            {
                                                if (var->size() > reproreproVar["Target"] && var->size() > reproreproVar["AppearingVar"])
                                                {
                                                    if (((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Male")) && (var->at(reproreproVar["AppearingVar"]) == "Y"))
                                                    {
                                                        col++;
                                                        value = "";
                                                        datas = *(new QMap<QString, QVariant>);
                                                        historic = "";
                                                        historic = "%1" + historic;
                                                        if (var->size() > reproreproVar["TypeVar"] && var->size() > reproreproVar["UnitVar"] && var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"] && var->size() > reproreproVar["MinValueVar"] && var->size() > reproreproVar["MaxValueVar"] && var->size() > reproreproVar["ExplainVar"])
                                                        {
                                                            if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                                            {
                                                                //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                                intEdit = new QSpinBox;
                                                                intEdit->setMaximum(std::numeric_limits<int>::max());
                                                                intEdit->setMinimum(std::numeric_limits<int>::min());
                                                                intEdit->setProperty("d", QVariant(datas));
                                                                if (value != "") intEdit->setValue(value.toInt());
                                                                if (var->at(reproreproVar["UnitVar"]) != "")
                                                                {
                                                                    intEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                                }
                                                                if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                                {
                                                                    intEdit->setProperty("i", QVariant("In"));
                                                                }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                                {
                                                                    intEdit->setProperty("i", QVariant("De"));
                                                                }else{
                                                                    intEdit->setProperty("i", QVariant("No"));
                                                                }
                                                                if (var->at(reproreproVar["MinValueVar"]) != "")
                                                                {
                                                                    intEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toInt());
                                                                }
                                                                if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                                {
                                                                    intEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toInt());
                                                                }
                                                                intEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                intEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                SelectFocus *test = new SelectFocus;
                                                                intEdit->installEventFilter(test);
                                                                QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                                                qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, intEdit);
                                                            }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                                            {
                                                                //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                                doubleEdit = new QDoubleSpinBox;
                                                                doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                                                doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                                                doubleEdit->setProperty("d", QVariant(datas));
                                                                //if (value != "") doubleEdit->setValue(value.toDouble());
                                                                if (var->at(reproreproVar["UnitVar"]) != "")
                                                                {
                                                                    doubleEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                                }
                                                                if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                                {
                                                                    doubleEdit->setProperty("i", QVariant("In"));
                                                                }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                                {
                                                                    doubleEdit->setProperty("i", QVariant("De"));
                                                                }else{
                                                                    doubleEdit->setProperty("i", QVariant("No"));
                                                                }
                                                                if (var->at(reproreproVar["MinValueVar"]) != "")
                                                                {
                                                                    doubleEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toDouble());
                                                                }
                                                                if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                                {
                                                                    doubleEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toDouble());
                                                                }
                                                                doubleEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                doubleEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                SelectFocus *test = new SelectFocus;
                                                                doubleEdit->installEventFilter(test);
                                                                QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, doubleEdit);
                                                            }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                                            {
                                                                //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                                                textEdit = new QLineEdit;
                                                                textEdit->setProperty("d", QVariant(datas));
                                                                //if (value != "") textEdit->setPlaceholderText(value);
                                                                textEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                textEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                SelectFocus *test = new SelectFocus;
                                                                textEdit->installEventFilter(test);
                                                                qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, textEdit);
                                                            }else if (var->at(reproVar["TypeVar"]).startsWith("eKEY"))
                                                            {
                                                                //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                                                factorEdit = new QComboBox;
                                                                factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                                                factorEdit->setEditable(true);
                                                                index = 0;
                                                                factorCodes.clear();
                                                                if (var->size() > reproreproVar["Name"])
                                                                {
                                                                    for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproreproVar["Name"])].begin()); it != factorMap[var->at(reproreproVar["Name"])].end(); it++)
                                                                    {
                                                                        if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["IdSex"])
                                                                        {
                                                                            if (it->at(factorPosition[var->at(reproreproVar["Name"])]["IdSex"]) == "1")
                                                                            {
                                                                                if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["Code"] && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Explain"])
                                                                                {
                                                                                    //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]));
                                                                                    factorEdit->addItem(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), QVariant(it.key()));
                                                                                    //if (value != "" && value == it->at(0)) factorEdit->setCurrentIndex(index);
                                                                                    index++;
                                                                                    factorCodes.insert(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproreproVar["Name"])]["Explain"]));
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                factorEdit->setCurrentIndex(-1);
                                                                factorEdit->model()->sort(0);
                                                                factorEdit->setProperty("d", QVariant(datas));
                                                                factorEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                historic = historic.arg(var->at(reproreproVar["ExplainVar"]) + "%1");
                                                                for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                                                {
                                                                    historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                                                }
                                                                factorEdit->setToolTip(historic.arg("").replace("#", "Male"));
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                                                QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                                                qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, factorEdit);
                                                            }else{
                                                                cell = new QTableWidgetItem;
                                                                cell->setData(Qt::UserRole, QVariant(datas));
                                                                cell->setStatusTip(value);
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }else{
                                            for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                                            {
                                                if (var->size() > reproreproVar["Target"] && var->size() > reproreproVar["AppearingVar"])
                                                {
                                                    if (((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Male")) && (var->at(reproreproVar["AppearingVar"]) == "Y"))
                                                    {
                                                        col++;
                                                        value = "";
                                                        datas = *(new QMap<QString, QVariant>);
                                                        historic = "";
                                                        for (QMap<QString, int>::iterator measure(dataFrame[gu.value()].second.first.second.begin()); measure != dataFrame[gu.value()].second.first.second.end(); measure++)
                                                        {
                                                            if (var->size() > reproreproVar["Name"])
                                                            {
                                                                if (reproMeasureMap[measure.value()].size() > reproMeasurePosition[var->at(reproreproVar["Name"])])
                                                                {
                                                                    if (reproMeasureMap[measure.value()].at(reproMeasurePosition[var->at(reproreproVar["Name"])]) != "")
                                                                    {
                                                                        if (var->size() > reproreproVar["TypeVar"])
                                                                        {
                                                                            if ((var->at(reproreproVar["TypeVar"]).startsWith("eKEY")))// && (value != ""))
                                                                            {
                                                                                if (reproMeasureMap[measure.value()].size() > reproMeasurePosition[var->at(reproreproVar["Name"])])
                                                                                {
                                                                                    if (factorMap[var->at(reproreproVar["Name"])][reproMeasureMap[measure.value()].at(reproMeasurePosition[var->at(reproreproVar["Name"])]).toInt()].size() > factorPosition[var->at(reproreproVar["Name"])]["Code"]) value = factorMap[var->at(reproreproVar["Name"])][reproMeasureMap[measure.value()].at(reproMeasurePosition[var->at(reproreproVar["Name"])]).toInt()].at(factorPosition[var->at(reproreproVar["Name"])]["Code"]);
                                                                                }
                                                                            }else{
                                                                                if (reproMeasureMap[measure.value()].size() > reproMeasurePosition[var->at(reproreproVar["Name"])]) value = reproMeasureMap[measure.value()].at(reproMeasurePosition[var->at(reproreproVar["Name"])]);
                                                                            }
                                                                            if (reproMeasureMap[measure.value()].size() > reproMeasurePosition["DateRM"])
                                                                            {
                                                                                historic = "\n" + reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"]) + "\t:\t" + value + historic;
                                                                                datas.insert(reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"]), value);
                                                                                if (!dates.contains(reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"])))
                                                                                {
                                                                                    dates.append(reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"]));
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        historic = "%1" + historic;
                                                        if (var->size() > reproreproVar["TypeVar"] && var->size() > reproreproVar["UnitVar"] && var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"] && var->size() > reproreproVar["MinValueVar"] && var->size() > reproreproVar["MaxValueVar"] && var->size() > reproreproVar["ExplainVar"])
                                                        {
                                                            if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                                            {
                                                                //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                                intEdit = new QSpinBox;
                                                                intEdit->setMaximum(std::numeric_limits<int>::max());
                                                                intEdit->setMinimum(std::numeric_limits<int>::min());
                                                                intEdit->setProperty("d", QVariant(datas));
                                                                if (value != "") intEdit->setValue(value.toInt());
                                                                if (var->at(reproreproVar["UnitVar"]) != "")
                                                                {
                                                                    intEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                                }
                                                                if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                                {
                                                                    intEdit->setProperty("i", QVariant("In"));
                                                                }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                                {
                                                                    intEdit->setProperty("i", QVariant("De"));
                                                                }else{
                                                                    intEdit->setProperty("i", QVariant("No"));
                                                                }
                                                                if (var->at(reproreproVar["MinValueVar"]) != "")
                                                                {
                                                                    intEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toInt());
                                                                }
                                                                if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                                {
                                                                    intEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toInt());
                                                                }
                                                                intEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                intEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                SelectFocus *test = new SelectFocus;
                                                                intEdit->installEventFilter(test);
                                                                QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                                                qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, intEdit);
                                                            }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                                            {
                                                                //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                                doubleEdit = new QDoubleSpinBox;
                                                                doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                                                doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                                                doubleEdit->setProperty("d", QVariant(datas));
                                                                if (value != "") doubleEdit->setValue(value.toDouble());
                                                                if (var->at(reproreproVar["UnitVar"]) != "")
                                                                {
                                                                    doubleEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                                }
                                                                if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                                {
                                                                    doubleEdit->setProperty("i", QVariant("In"));
                                                                }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                                {
                                                                    doubleEdit->setProperty("i", QVariant("De"));
                                                                }else{
                                                                    doubleEdit->setProperty("i", QVariant("No"));
                                                                }
                                                                if (var->at(reproreproVar["MinValueVar"]) != "")
                                                                {
                                                                    doubleEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toDouble());
                                                                }
                                                                if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                                {
                                                                    doubleEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toDouble());
                                                                }
                                                                doubleEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                doubleEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                SelectFocus *test = new SelectFocus;
                                                                doubleEdit->installEventFilter(test);
                                                                QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, doubleEdit);
                                                            }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                                            {
                                                                //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                                                textEdit = new QLineEdit;
                                                                textEdit->setProperty("d", QVariant(datas));
                                                                if (value != "") textEdit->setPlaceholderText(value);
                                                                textEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                textEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                SelectFocus *test = new SelectFocus;
                                                                textEdit->installEventFilter(test);
                                                                qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, textEdit);
                                                            }else if (var->at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                                                            {
                                                                //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                                                factorEdit = new QComboBox;
                                                                factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                                                factorEdit->setEditable(true);
                                                                index = 0;
                                                                factorCodes.clear();
                                                                if (var->size() > reproreproVar["Name"])
                                                                {
                                                                    for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproreproVar["Name"])].begin()); it != factorMap[var->at(reproreproVar["Name"])].end(); it++)
                                                                    {
                                                                        if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["IdSex"])
                                                                        {
                                                                            if (it->at(factorPosition[var->at(reproreproVar["Name"])]["IdSex"]) == "1" && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Code"] && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Explain"])
                                                                            {
                                                                                //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]));
                                                                                factorEdit->addItem(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), QVariant(it.key()));
                                                                                if (value != "" && value == it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"])) factorEdit->setCurrentIndex(index);
                                                                                index++;
                                                                                factorCodes.insert(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproreproVar["Name"])]["Explain"]));
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                if (value == "") factorEdit->setCurrentIndex(-1);
                                                                factorEdit->model()->sort(0);
                                                                factorEdit->setProperty("d", QVariant(datas));
                                                                factorEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                historic = historic.arg(var->at(reproreproVar["ExplainVar"]) + "%1");
                                                                for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                                                {
                                                                    historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                                                }
                                                                factorEdit->setToolTip(historic.arg("").replace("#", "Male"));
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                                                QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                                                qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, factorEdit);
                                                            }else{
                                                                cell = new QTableWidgetItem;
                                                                cell->setData(Qt::UserRole, QVariant(datas));
                                                                cell->setStatusTip(value);
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        for (int i(0); i < maxFemale; i++)
                                        {
                                            if (i < dataFrame[gu.value()].second.second.size())
                                            {
                                                if (dataFrame[gu.value()].second.second.keys().size() > i) tempId = dataFrame[gu.value()].second.second.keys().at(i);
                                                for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                                                {
                                                    if (var->size() > reproreproVar["Target"] && var->size() > reproreproVar["AppearingVar"])
                                                    {
                                                        if (((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Female")) && (var->at(reproreproVar["AppearingVar"]) == "Y"))
                                                        {
                                                            col++;
                                                            value = "";
                                                            datas = *(new QMap<QString, QVariant>);
                                                            historic = "";
                                                            for (QMap<QString, int>::iterator measure(dataFrame[gu.value()].second.second[tempId].begin()); measure != dataFrame[gu.value()].second.second[tempId].end(); measure++)
                                                            {
                                                                if (var->size() > reproreproVar["Name"])
                                                                {
                                                                    if (reproMeasureMap[measure.value()].size() > reproMeasurePosition[var->at(reproreproVar["Name"])])
                                                                    {
                                                                        if (reproMeasureMap[measure.value()].at(reproMeasurePosition[var->at(reproreproVar["Name"])]) != "")
                                                                        {
                                                                            if (var->size() > reproreproVar["TypeVar"] && reproMeasureMap[measure.value()].size() > reproMeasurePosition[var->at(reproreproVar["Name"])])
                                                                            {
                                                                                if ((var->at(reproreproVar["TypeVar"]).startsWith("eKEY")))// && (value != ""))
                                                                                {
                                                                                    if (reproMeasureMap[measure.value()].size() > reproMeasurePosition[var->at(reproreproVar["Name"])]) value = factorMap[var->at(reproreproVar["Name"])][reproMeasureMap[measure.value()].at(reproMeasurePosition[var->at(reproreproVar["Name"])]).toInt()].at(factorPosition[var->at(reproreproVar["Name"])]["Code"]);
                                                                                }else{
                                                                                    value = reproMeasureMap[measure.value()].at(reproMeasurePosition[var->at(reproreproVar["Name"])]);
                                                                                }
                                                                            }
                                                                            if (reproMeasureMap[measure.value()].size() > reproMeasurePosition["DateRM"])
                                                                            {
                                                                                historic = "\n" + reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"]) + "\t:\t" + value + historic;
                                                                                datas.insert(reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"]), value);
                                                                                if (!dates.contains(reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"])))
                                                                                {
                                                                                    dates.append(reproMeasureMap[measure.value()].at(reproMeasurePosition["DateRM"]));
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            historic = "%1" + historic;
                                                            if (var->size() > reproreproVar["TypeVar"] && var->size() > reproreproVar["UnitVar"] && var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"] && var->size() > reproreproVar["MinValueVar"] && var->size() > reproreproVar["MaxValueVar"] && var->size() > reproreproVar["ExplainVar"])
                                                            {
                                                                if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                                                {
                                                                    //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                                    intEdit = new QSpinBox;
                                                                    intEdit->setMaximum(std::numeric_limits<int>::max());
                                                                    intEdit->setMinimum(std::numeric_limits<int>::min());
                                                                    intEdit->setProperty("d", QVariant(datas));
                                                                    if (value != "") intEdit->setValue(value.toInt());
                                                                    if (var->at(reproreproVar["UnitVar"]) != "")
                                                                    {
                                                                        intEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                                    }
                                                                    if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                                    {
                                                                        intEdit->setProperty("i", QVariant("In"));
                                                                    }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                                    {
                                                                        intEdit->setProperty("i", QVariant("De"));
                                                                    }else{
                                                                        intEdit->setProperty("i", QVariant("No"));
                                                                    }
                                                                    if (var->at(reproreproVar["MinValueVar"]) != "")
                                                                    {
                                                                        intEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toInt());
                                                                    }
                                                                    if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                                    {
                                                                        intEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toInt());
                                                                    }
                                                                    intEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                    intEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                                                    //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                    //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                    //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                    SelectFocus *test = new SelectFocus;
                                                                    intEdit->installEventFilter(test);
                                                                    QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                                                    qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, intEdit);
                                                                }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                                                {
                                                                    //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                                    doubleEdit = new QDoubleSpinBox;
                                                                    doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                                                    doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                                                    doubleEdit->setProperty("d", QVariant(datas));
                                                                    if (value != "") doubleEdit->setValue(value.toDouble());
                                                                    if (var->at(reproreproVar["UnitVar"]) != "")
                                                                    {
                                                                        doubleEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                                    }
                                                                    if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                                    {
                                                                        doubleEdit->setProperty("i", QVariant("In"));
                                                                    }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                                    {
                                                                        doubleEdit->setProperty("i", QVariant("De"));
                                                                    }else{
                                                                        doubleEdit->setProperty("i", QVariant("No"));
                                                                    }
                                                                    if (var->at(reproreproVar["MinValueVar"]) != "")
                                                                    {
                                                                        doubleEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toDouble());
                                                                    }
                                                                    if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                                    {
                                                                        doubleEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toDouble());
                                                                    }
                                                                    doubleEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                    doubleEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                                                    //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                    //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                    //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                    SelectFocus *test = new SelectFocus;
                                                                    doubleEdit->installEventFilter(test);
                                                                    QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                    qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, doubleEdit);
                                                                }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                                                {
                                                                    //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                                                    textEdit = new QLineEdit;
                                                                    textEdit->setProperty("d", QVariant(datas));
                                                                    if (value != "") textEdit->setPlaceholderText(value);
                                                                    textEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                    textEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                                                    //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                    //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                    //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                    //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                    SelectFocus *test = new SelectFocus;
                                                                    textEdit->installEventFilter(test);
                                                                    qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, textEdit);
                                                                }else if (var->at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                                                                {
                                                                    //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                                                    factorEdit = new QComboBox;
                                                                    factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                                                    factorEdit->setEditable(true);
                                                                    index = 0;
                                                                    factorCodes.clear();
                                                                    if (var->size() > reproreproVar["Name"])
                                                                    {
                                                                        for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproreproVar["Name"])].begin()); it != factorMap[var->at(reproreproVar["Name"])].end(); it++)
                                                                        {
                                                                            if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["IdSex"] && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Code"] && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Explain"])
                                                                            {
                                                                                if (it->at(factorPosition[var->at(reproreproVar["Name"])]["IdSex"]) == "2")
                                                                                {
                                                                                    //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]));
                                                                                    factorEdit->addItem(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), QVariant(it.key()));
                                                                                    if (value != "" && value == it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"])) factorEdit->setCurrentIndex(index);
                                                                                    index++;
                                                                                    factorCodes.insert(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproreproVar["Name"])]["Explain"]));
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    if (value == "") factorEdit->setCurrentIndex(-1);
                                                                    factorEdit->model()->sort(0);
                                                                    factorEdit->setProperty("d", QVariant(datas));
                                                                    factorEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                    historic = historic.arg(var->at(reproreproVar["ExplainVar"]) + "%1");
                                                                    for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                                                    {
                                                                        historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                                                    }
                                                                    factorEdit->setToolTip(historic.arg("").replace("#", "Femelle"));
                                                                    //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                    //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                    //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                    //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                    QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                                                    QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                                                    qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, factorEdit);
                                                                }else{
                                                                    cell = new QTableWidgetItem;
                                                                    cell->setData(Qt::UserRole, QVariant(datas));
                                                                    cell->setStatusTip(value);
                                                                    //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                    //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                    //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                    qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }else{
                                                datas = *(new QMap<QString, QVariant>);
                                                value = "";
                                                for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                                                {
                                                    if (var->size() > reproreproVar["Target"] && var->size() > reproreproVar["AppearingVar"])
                                                    {
                                                        if (((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Female")) && (var->at(reproreproVar["AppearingVar"]) == "Y"))
                                                        {
                                                            col++;
                                                            value = "";
                                                            datas = *(new QMap<QString, QVariant>);
                                                            historic = "";
                                                            historic = "%1" + historic;
                                                            if (var->size() > reproreproVar["TypeVar"] && var->size() > reproreproVar["UnitVar"] && var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"] && var->size() > reproreproVar["MinValueVar"] && var->size() > reproreproVar["MaxValueVar"] && var->size() > reproreproVar["ExplainVar"])
                                                            {
                                                                if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                                                {
                                                                    //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                                    intEdit = new QSpinBox;
                                                                    intEdit->setMaximum(std::numeric_limits<int>::max());
                                                                    intEdit->setMinimum(std::numeric_limits<int>::min());
                                                                    intEdit->setProperty("d", QVariant(datas));
                                                                    if (value != "") intEdit->setValue(value.toInt());
                                                                    if (var->at(reproreproVar["UnitVar"]) != "")
                                                                    {
                                                                        intEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                                    }
                                                                    if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                                    {
                                                                        intEdit->setProperty("i", QVariant("In"));
                                                                    }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                                    {
                                                                        intEdit->setProperty("i", QVariant("De"));
                                                                    }else{
                                                                        intEdit->setProperty("i", QVariant("No"));
                                                                    }
                                                                    if (var->at(reproreproVar["MinValueVar"]) != "")
                                                                    {
                                                                        intEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toInt());
                                                                    }
                                                                    if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                                    {
                                                                        intEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toInt());
                                                                    }
                                                                    intEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                    intEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                                                    //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                    //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                    //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                    SelectFocus *test = new SelectFocus;
                                                                    intEdit->installEventFilter(test);
                                                                    QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                                                    qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, intEdit);
                                                                }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                                                {
                                                                    //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                                    doubleEdit = new QDoubleSpinBox;
                                                                    doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                                                    doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                                                    doubleEdit->setProperty("d", QVariant(datas));
                                                                    //if (value != "") doubleEdit->setValue(value.toDouble());
                                                                    if (var->at(reproreproVar["UnitVar"]) != "")
                                                                    {
                                                                        doubleEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                                    }
                                                                    if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                                    {
                                                                        doubleEdit->setProperty("i", QVariant("In"));
                                                                    }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                                    {
                                                                        doubleEdit->setProperty("i", QVariant("De"));
                                                                    }else{
                                                                        doubleEdit->setProperty("i", QVariant("No"));
                                                                    }
                                                                    if (var->at(reproreproVar["MinValueVar"]) != "")
                                                                    {
                                                                        doubleEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toDouble());
                                                                    }
                                                                    if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                                    {
                                                                        doubleEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toDouble());
                                                                    }
                                                                    doubleEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                    doubleEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                                                    //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                    //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                    //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                    SelectFocus *test = new SelectFocus;
                                                                    doubleEdit->installEventFilter(test);
                                                                    QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                    qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, doubleEdit);
                                                                }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                                                {
                                                                    //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                                                    textEdit = new QLineEdit;
                                                                    textEdit->setProperty("d", QVariant(datas));
                                                                    //if (value != "") textEdit->setPlaceholderText(value);
                                                                    textEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                    textEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                                                    //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                    //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                    //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                    //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                    SelectFocus *test = new SelectFocus;
                                                                    textEdit->installEventFilter(test);
                                                                    qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, textEdit);
                                                                }else if (var->at(reproVar["TypeVar"]).startsWith("eKEY"))
                                                                {
                                                                    //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                                                    factorEdit = new QComboBox;
                                                                    factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                                                    factorEdit->setEditable(true);
                                                                    index = 0;
                                                                    factorCodes.clear();
                                                                    if (var->size() > reproreproVar["Name"])
                                                                    {
                                                                        for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproreproVar["Name"])].begin()); it != factorMap[var->at(reproreproVar["Name"])].end(); it++)
                                                                        {
                                                                            if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["IdSex"] && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Code"] && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Explain"])
                                                                            {
                                                                                if (it->at(factorPosition[var->at(reproreproVar["Name"])]["IdSex"]) == "2")
                                                                                {
                                                                                    //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]));
                                                                                    factorEdit->addItem(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), QVariant(it.key()));
                                                                                    //if (value != "" && value == it->at(0)) factorEdit->setCurrentIndex(index);
                                                                                    index++;
                                                                                    factorCodes.insert(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproreproVar["Name"])]["Explain"]));
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                    factorEdit->setCurrentIndex(-1);
                                                                    factorEdit->model()->sort(0);
                                                                    factorEdit->setProperty("d", QVariant(datas));
                                                                    factorEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                    historic = historic.arg(var->at(reproreproVar["ExplainVar"]) + "%1");
                                                                    for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                                                    {
                                                                        historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                                                    }
                                                                    factorEdit->setToolTip(historic.arg("").replace("#", "Femelle"));
                                                                    //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                    //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                    //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                    //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                    QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                                                    QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                                                    qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, factorEdit);
                                                                }else{
                                                                    cell = new QTableWidgetItem;
                                                                    cell->setData(Qt::UserRole, QVariant(datas));
                                                                    cell->setStatusTip(value);
                                                                    //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                    //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                    //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                    qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (maxFemale == 0)
                                        {
                                            datas = *(new QMap<QString, QVariant>);
                                            value = "";
                                            for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                                            {
                                                if (var->size() > reproreproVar["Target"] && var->size() > reproreproVar["AppearingVar"])
                                                {
                                                    if (((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Female")) && (var->at(reproreproVar["AppearingVar"]) == "Y"))
                                                    {
                                                        col++;
                                                        value = "";
                                                        datas = *(new QMap<QString, QVariant>);
                                                        historic = "";
                                                        historic = "%1" + historic;
                                                        if (var->size() > reproreproVar["TypeVar"] && var->size() > reproreproVar["UnitVar"] && var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"] && var->size() > reproreproVar["MaxValueVar"] && var->size() > reproreproVar["MinValueVar"] && var->size() > reproreproVar["ExplainVar"])
                                                        {
                                                            if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                                            {
                                                                //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                                intEdit = new QSpinBox;
                                                                intEdit->setMaximum(std::numeric_limits<int>::max());
                                                                intEdit->setMinimum(std::numeric_limits<int>::min());
                                                                intEdit->setProperty("d", QVariant(datas));
                                                                if (value != "") intEdit->setValue(value.toInt());
                                                                if (var->at(reproreproVar["UnitVar"]) != "")
                                                                {
                                                                    intEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                                }
                                                                if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                                {
                                                                    intEdit->setProperty("i", QVariant("In"));
                                                                }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                                {
                                                                    intEdit->setProperty("i", QVariant("De"));
                                                                }else{
                                                                    intEdit->setProperty("i", QVariant("No"));
                                                                }
                                                                if (var->at(reproreproVar["MinValueVar"]) != "")
                                                                {
                                                                    intEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toInt());
                                                                }
                                                                if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                                {
                                                                    intEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toInt());
                                                                }
                                                                intEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                intEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                SelectFocus *test = new SelectFocus;
                                                                intEdit->installEventFilter(test);
                                                                QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                                                qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, intEdit);
                                                            }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                                            {
                                                                //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                                                doubleEdit = new QDoubleSpinBox;
                                                                doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                                                doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                                                doubleEdit->setProperty("d", QVariant(datas));
                                                                //if (value != "") doubleEdit->setValue(value.toDouble());
                                                                if (var->at(reproreproVar["UnitVar"]) != "")
                                                                {
                                                                    doubleEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                                }
                                                                if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                                {
                                                                    doubleEdit->setProperty("i", QVariant("In"));
                                                                }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                                {
                                                                    doubleEdit->setProperty("i", QVariant("De"));
                                                                }else{
                                                                    doubleEdit->setProperty("i", QVariant("No"));
                                                                }
                                                                if (var->at(reproreproVar["MinValueVar"]) != "")
                                                                {
                                                                    doubleEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toDouble());
                                                                }
                                                                if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                                {
                                                                    doubleEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toDouble());
                                                                }
                                                                doubleEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                doubleEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                SelectFocus *test = new SelectFocus;
                                                                doubleEdit->installEventFilter(test);
                                                                QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, doubleEdit);
                                                            }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                                            {
                                                                //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                                                textEdit = new QLineEdit;
                                                                textEdit->setProperty("d", QVariant(datas));
                                                                //if (value != "") textEdit->setPlaceholderText(value);
                                                                textEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                textEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                SelectFocus *test = new SelectFocus;
                                                                textEdit->installEventFilter(test);
                                                                qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, textEdit);
                                                            }else if (var->at(reproVar["TypeVar"]).startsWith("eKEY"))
                                                            {
                                                                //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                                                factorEdit = new QComboBox;
                                                                factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                                                factorEdit->setEditable(true);
                                                                index = 0;
                                                                factorCodes.clear();
                                                                if (var->size() > reproreproVar["Name"])
                                                                {
                                                                    for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproreproVar["Name"])].begin()); it != factorMap[var->at(reproreproVar["Name"])].end(); it++)
                                                                    {
                                                                        if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["IdSex"] && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Code"] && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Explain"])
                                                                        {
                                                                            if (it->at(factorPosition[var->at(reproreproVar["Name"])]["IdSex"]) == "2")
                                                                            {
                                                                                //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]));
                                                                                factorEdit->addItem(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), QVariant(it.key()));
                                                                                //if (value != "" && value == it->at(0)) factorEdit->setCurrentIndex(index);
                                                                                index++;
                                                                                factorCodes.insert(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproreproVar["Name"])]["Explain"]));
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                factorEdit->setCurrentIndex(-1);
                                                                factorEdit->model()->sort(0);
                                                                factorEdit->setProperty("d", QVariant(datas));
                                                                factorEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                                historic = historic.arg(var->at(reproreproVar["ExplainVar"]) + "%1");
                                                                for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                                                {
                                                                    historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                                                }
                                                                factorEdit->setToolTip(historic.arg("").replace("#", "Femelle"));
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                                                QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                                                QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                                                qobject_cast<QTableWidget *>(dataTab)->setCellWidget(row, col, factorEdit);
                                                            }else{
                                                                cell = new QTableWidgetItem;
                                                                cell->setData(Qt::UserRole, QVariant(datas));
                                                                cell->setStatusTip(value);
                                                                //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                                                //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                                                //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                                                qobject_cast<QTableWidget *>(dataTab)->setItem(row, col, cell);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    qSort(dates.begin(), dates.end());
                                    // Connect signals to slots
                                    QObject::connect(qobject_cast<QTableWidget *>(dataTab), SIGNAL(cellDoubleClicked(int, int)), this, SLOT(modifiesInputTable(int, int)));
                                    QObject::connect(qobject_cast<QTableWidget *>(dataTab)->horizontalHeader(), SIGNAL(sectionDoubleClicked(int)), this, SLOT(modifiesInputTable(int)));
                                    QObject::connect(saveButton, SIGNAL(clicked()), this, SLOT(saveRecord()));
                                    QObject::connect(qobject_cast<QTableWidget *>(dataTab), SIGNAL(cellClicked(int, int)), this, SLOT(updateCommentAndModifyMeasure(int, int)));
                                }
                                reproMeasureFile.close();
                            }
                            measureFile.close();
                        }
                        reproVarFile.close();
                    }
                    varFile.close();
                }
                sexFile.close();
            }
            reproFile.close();
        }
        GUFile.close();
    }else{
        QMessageBox::information(NULL, "Déjà enregistré", "Il y a un enregistrement pour cet axe!");
    }
}
