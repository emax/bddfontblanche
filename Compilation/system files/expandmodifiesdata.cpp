#include <MainWindow.h>

void MainWindow::expandModifiesData(int a, int b)
{
    // we put the table with the datas
    QTableWidget *GUTab, *reproTab;
    QLabel *GUTitle, *reproTitle, *title;
    if (mainLayout->rowCount() == 5)
    {
        QWidget * buttonBackToMain = mainLayout->itemAtPosition(4, 0)->widget();
        mainLayout->removeWidget(buttonBackToMain);
        mainLayout->addWidget(buttonBackToMain, 7, 0, 1, 2);
        QWidget * buttonBackToBack = mainLayout->itemAtPosition(3, 0)->widget();
        mainLayout->removeWidget(buttonBackToBack);
        mainLayout->addWidget(buttonBackToBack, 6, 0, 1, 2);
        GUTab = new QTableWidget(0, 0);
        GUTab->setObjectName("GUTable");
        mainLayout->addWidget(GUTab, 5, 0);
        reproTab = new QTableWidget(0, 0);
        reproTab->setObjectName("reproTable");
        mainLayout->addWidget(reproTab, 5, 1);
        GUTab->verticalHeader()->hide();
        reproTab->verticalHeader()->hide();
        GUTitle = new QLabel("Mesures d'UC");
        reproTitle = new QLabel("Mesures de reproduction");
        mainLayout->addWidget(GUTitle, 4, 0);
        mainLayout->addWidget(reproTitle, 4, 1);
        title = new QLabel;
        mainLayout->addWidget(title, 3, 0, 1, 2);
    }else{
        GUTab = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(5, 0)->widget());
        for (int i = GUTab->rowCount(); i>0; i--)
        {
            GUTab->removeRow(i - 1);
        }
        for (int j = GUTab->columnCount(); j>0; j--)
        {
            GUTab->removeColumn(j - 1);
        }
        reproTab = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(5, 1)->widget());
        for (int i = reproTab->rowCount(); i>0; i--)
        {
            reproTab->removeRow(i - 1);
        }
        for (int j = reproTab->columnCount(); j>0; j--)
        {
            reproTab->removeColumn(j - 1);
        }
        QObject::disconnect(GUTab, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(modifiesValueData(int, int)));
        QObject::disconnect(reproTab, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(modifiesValueData(int, int)));
        GUTitle = qobject_cast<QLabel *>(mainLayout->itemAtPosition(4, 0)->widget());
        GUTitle->setText("Mesures d'UC");
        reproTitle = qobject_cast<QLabel *>(mainLayout->itemAtPosition(4, 1)->widget());
        reproTitle->setText("Mesures de reproduction");
        title = qobject_cast<QLabel *>(mainLayout->itemAtPosition(3, 0)->widget());
    }

    // we need now to know which was the selected Axis
    int axisSelected(-1);
    axisSelected = qobject_cast<QTableWidget *>(QObject::sender())->item(a, b)->data(Qt::UserRole).toList().at(0).toInt();
    title->setText(qobject_cast<QTableWidget *>(QObject::sender())->item(a, 0)->text());

    // Now we browse list3 to have each GU concerned with this axis
    QFile GUFile("bin/list3");
    QMap<QString, int> GUs; //GU name and Ids
    QMap<QString, int> GUPosition;
    QString line;
    int countLine(-1), Id;
    QStringList dataRow;
    QMap<int, QPair<QList<QStringList>, QPair<QPair<int, QList<QStringList> >, QList<QPair<int, QList<QStringList> > > > > > fullDatas; // data contener
    if (!GUFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list3.");
        urgentStop();
        return;
    }else{
        QTextStream GUStream(&GUFile);
        line = GUStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list3\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!GUStream.atEnd()  && line != "###")
            {
                line = GUStream.readLine();
                GUPosition.insert(line.split("\t").at(0), countLine);
                countLine++;
            }
            while(!GUStream.atEnd())
            {
                dataRow = GUStream.readLine().split("\t");
                Id = dataRow.at(0).toInt();
                dataRow.removeFirst();
                if (dataRow.size() > GUPosition["Id2"])
                {
                    if (dataRow.at(GUPosition["Id2"]).toInt() == axisSelected)
                    {
                        fullDatas.insert(Id, *(new QPair<QList<QStringList>, QPair<QPair<int, QList<QStringList> >, QList<QPair<int, QList<QStringList> > > > >));
                        if (dataRow.size() > GUPosition["Year"] && dataRow.size() > GUPosition["NGU"])
                        {
                            if ((dataRow.at(GUPosition["Year"]) == "0") || (dataRow.at(GUPosition["Year"]) == ""))
                            {
                                GUs.insert(QString("---- . %1").arg(dataRow.at(GUPosition["NGU"])), Id);
                            }else{
                                GUs.insert(QString("%1 . %2").arg(dataRow.at(GUPosition["Year"])).arg(dataRow.at(GUPosition["NGU"])), Id);
                            }
                        }
                    }
                }
            }
        }
        GUFile.close();
    }

    // Now we browse list4 to have each reproduction concerned with this axis
    QFile reproFile("bin/list4");
    QMap<QString, int> repros; //GU name and Ids
    QMap<QString, int> reproPosition;
    QMap<int, QList<int> > sexRepro; // IdRepro, Bearer, Sex, Number
    QString reproName;
    countLine = -1;
    int count;
    if (!reproFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list4.");
        urgentStop();
        return;
    }else{
        QTextStream reproStream(&reproFile);
        line = reproStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list4\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!reproStream.atEnd()  && line != "###")
            {
                line = reproStream.readLine();
                reproPosition.insert(line.split("\t").at(0), countLine);
                countLine++;
            }
            while(!reproStream.atEnd())
            {
                dataRow = reproStream.readLine().split("\t");
                Id = dataRow.at(0).toInt();
                dataRow.removeFirst();
                if (GUs.values().contains(dataRow.at(reproPosition["Id3"]).toInt()))
                {
                    // this reproduction is beared by a GU from the selected axis
                    if (dataRow.size() > reproPosition["IdSex"] && dataRow.size() > reproPosition["Id3"])
                    {
                        if (dataRow.at(reproPosition["IdSex"]) == "1")
                        {
                            sexRepro.insert(Id, *(new QList<int>));
                            sexRepro[Id].append(dataRow.at(reproPosition["Id3"]).toInt());
                            sexRepro[Id].append(1);
                            sexRepro[Id].append(0);
                            fullDatas[dataRow.at(reproPosition["Id3"]).toInt()].second.first.first = Id;
                            reproName = GUs.key(dataRow.at(reproPosition["Id3"]).toInt()) + " Male";
                            repros.insert(reproName, Id);
                        }else{
                            fullDatas[dataRow.at(reproPosition["Id3"]).toInt()].second.second.append(*(new QPair<int, QList<QStringList> >));
                            count = fullDatas[dataRow.at(reproPosition["Id3"]).toInt()].second.second.size();
                            fullDatas[dataRow.at(reproPosition["Id3"]).toInt()].second.second[count - 1].first = Id;
                            reproName = GUs.key(dataRow.at(reproPosition["Id3"]).toInt()) + " Femelle " + QString::number(count);
                            repros.insert(reproName, Id);
                            sexRepro.insert(Id, *(new QList<int>));
                            sexRepro[Id].append(dataRow.at(reproPosition["Id3"]).toInt());
                            sexRepro[Id].append(2);
                            sexRepro[Id].append(count - 1);
                        }
                    }
                }
            }
        }
        reproFile.close();
    }

    // Now we browse measure to have each GU measures concerned with this axis
    QFile measureFile("bin/measure");
    QMap<QString, int> varPosition;
    QMap<QString, bool> varFactor;
    QMap<int, QString> varNames;
    QMap<QString, QString> varType;
    QMap<QString, QMap<QString, QVariant> > factorCodes;
    countLine = -1;
    QString fileName, varName;
    if (!measureFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/measure.");
        urgentStop();
        return;
    }else{
        QTextStream measureStream(&measureFile);
        line = measureStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/measure\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!measureStream.atEnd()  && line != "###")
            {
                countLine++;
                line = measureStream.readLine();
                if (line != "###")
                {
                    varName = line.split("\t").at(0);
                    varPosition.insert(varName, countLine);
                    varNames.insert(countLine, varName);
                    if (line.split("\t").size() > 1)
                    {
                        if ((line.split("\t").at(1).startsWith("eKEY:")) && (varName != "Id3"))
                        {
                            varFactor.insert(varName, true);
                            varType.insert(varName, "FACTOR");
                            // we have to load each codes then
                            factorCodes.insert(varName, *(new QMap<QString, QVariant>));
                            fileName = "bin/" + line.split("\t").at(1);
                            fileName.replace("eKEY:", "");
                            QFile tempFile(fileName);
                            QMap<QString, int> tempPosition;
                            int tempLine (-1);
                            if (!tempFile.open(QIODevice::ReadOnly | QIODevice::Text))
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : " + fileName);
                                urgentStop();
                                return;
                            }else{
                                QTextStream tempStream(&tempFile);
                                dataRow = tempStream.readLine().split("\t");
                                if (dataRow.at(0) != "###")
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : " + fileName + "\nMauvaise entête (\"###\" attendu)");
                                    urgentStop();
                                    return;
                                }else{
                                    dataRow.clear();
                                    dataRow.append("");
                                    while (!tempStream.atEnd()  && dataRow.at(0) != "###")
                                    {
                                        dataRow = tempStream.readLine().split("\t");
                                        tempPosition.insert(dataRow.at(0), tempLine);
                                        tempLine++;
                                    }
                                    while(!tempStream.atEnd())
                                    {
                                        //QMessageBox::information(NULL, varName + QString::number(Id), dataRow.at(tempPosition["Code"]));
                                        dataRow = tempStream.readLine().split("\t");
                                        Id = dataRow.at(0).toInt();
                                        dataRow.removeFirst();
                                        if (dataRow.size() > tempPosition["Code"]) factorCodes[varName].insert(QString::number(Id), QVariant(dataRow.at(tempPosition["Code"])));
                                    }
                                }
                                tempFile.close();
                            }
                        }else
                        {
                            varFactor.insert(varName, false);
                            if (line.split("\t").at(1) == "DATE")
                            {
                                varType.insert(varName, "DATE");
                            }else if (line.split("\t").at(1) == "DOUBLE")
                            {
                                varType.insert(varName, "DOUBLE");
                            }else if (line.split("\t").at(1) == "INTEGER")
                            {
                                varType.insert(varName, "INTEGER");
                            }else if (line.split("\t").at(1) == "STRING")
                            {
                                varType.insert(varName, "STRING");
                            }else
                            {
                                varType.insert(varName, "CONST");
                            }
                        }
                    }
                }
            }
            while(!measureStream.atEnd())
            {
                dataRow = measureStream.readLine().split("\t");
                Id = dataRow.at(0).toInt();
                if (dataRow.size() > varPosition["Id3"])
                {
                    if (GUs.values().contains(dataRow.at(varPosition["Id3"]).toInt()))
                    {
                        // this measure concernes the selected axis
                        fullDatas[dataRow.at(varPosition["Id3"]).toInt()].first.append(dataRow);
                    }
                }
            }
        }
        measureFile.close();
    }

    // Now we browse reproductionMeasure to have each repro measures concerned with this axis
    QFile reproMeasureFile("bin/reproductionMeasure");
    QMap<QString, int> reproVarPosition;
    QMap<QString, bool> reproVarFactor;
    QMap<int, QString> reproVarNames;
    QMap<QString, QString> reproVarType;
    QMap<QString, QMap<QString, QVariant> > femaleCodes, maleCodes;
    countLine = -1;
    if (!reproMeasureFile.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        // Error
        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/reproductionMeasure.");
        urgentStop();
        return;
    }else{
        QTextStream reproMeasureStream(&reproMeasureFile);
        line = reproMeasureStream.readLine();
        if (line != "###")
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/reproductionMeasure\nMauvaise entête (\"###\" attendu)");
            urgentStop();
            return;
        }else{
            line = "";
            while (!reproMeasureStream.atEnd()  && line != "###")
            {
                countLine++;
                line = reproMeasureStream.readLine();
                if (line != "###")
                {
                    varName = line.split("\t").at(0);
                    reproVarPosition.insert(varName, countLine);
                    reproVarNames.insert(countLine, varName);
                    if (line.split("\t").size() > 1)
                    {
                        if ((line.split("\t").at(1).startsWith("eKEY:")) && (varName != "Id4"))
                        {
                            reproVarFactor.insert(varName, true);
                            reproVarType.insert(varName, "FACTOR");
                            // we have to load each codes then
                            maleCodes.insert(varName, *(new QMap<QString, QVariant>));
                            femaleCodes.insert(varName, *(new QMap<QString, QVariant>));
                            fileName = "bin/" + line.split("\t").at(1);
                            fileName.replace("eKEY:", "");
                            QFile tempFile(fileName);
                            QMap<QString, int> tempPosition;
                            int tempLine (-1);
                            if (!tempFile.open(QIODevice::ReadOnly | QIODevice::Text))
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : " + fileName);
                                urgentStop();
                                return;
                            }else{
                                QTextStream tempStream(&tempFile);
                                dataRow = tempStream.readLine().split("\t");
                                if (dataRow.at(0) != "###")
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : " + fileName + "\nMauvaise entête (\"###\" attendu)");
                                    urgentStop();
                                    return;
                                }else{
                                    dataRow.clear();
                                    dataRow.append("");
                                    while (!tempStream.atEnd()  && dataRow.at(0) != "###")
                                    {
                                        dataRow = tempStream.readLine().split("\t");
                                        tempPosition.insert(dataRow.at(0), tempLine);
                                        tempLine++;
                                    }
                                    while(!tempStream.atEnd())
                                    {
                                        //QMessageBox::information(NULL, varName + QString::number(Id), dataRow.at(tempPosition["Code"]));
                                        dataRow = tempStream.readLine().split("\t");
                                        Id = dataRow.at(0).toInt();
                                        dataRow.removeFirst();
                                        if (dataRow.size() > tempPosition["IdSex"] && dataRow.size() > tempPosition["Code"])
                                        {
                                            if (dataRow.at(tempPosition["IdSex"]) == "1")
                                            {
                                                maleCodes[varName].insert(QString::number(Id), QVariant(dataRow.at(tempPosition["Code"])));
                                            }else{
                                                femaleCodes[varName].insert(QString::number(Id), QVariant(dataRow.at(tempPosition["Code"])));
                                            }
                                        }
                                    }
                                }
                                tempFile.close();
                            }
                        }else
                        {
                            reproVarFactor.insert(varName, false);
                            if (line.split("\t").at(1) == "DATE")
                            {
                                reproVarType.insert(varName, "DATE");
                            }else if (line.split("\t").at(1) == "DOUBLE")
                            {
                                reproVarType.insert(varName, "DOUBLE");
                            }else if (line.split("\t").at(1) == "INTEGER")
                            {
                                reproVarType.insert(varName, "INTEGER");
                            }else if (line.split("\t").at(1) == "STRING")
                            {
                                reproVarType.insert(varName, "STRING");
                            }else
                            {
                                reproVarType.insert(varName, "CONST");
                            }
                        }
                    }
                }
            }
            while(!reproMeasureStream.atEnd())
            {
                dataRow = reproMeasureStream.readLine().split("\t");
                Id = dataRow.at(0).toInt();
                if (dataRow.size() > reproVarPosition["Id4"])
                {
                    if (repros.values().contains(dataRow.at(reproVarPosition["Id4"]).toInt()))
                    {
                        // this measure concernes the selected axis
                        if (sexRepro[dataRow.at(reproVarPosition["Id4"]).toInt()].at(1) == 1)
                        {
                            fullDatas[sexRepro[dataRow.at(reproVarPosition["Id4"]).toInt()].at(0)].second.first.second.append(dataRow);
                        }else{
                            fullDatas[sexRepro[dataRow.at(reproVarPosition["Id4"]).toInt()].at(0)].second.second[sexRepro[dataRow.at(reproVarPosition["Id4"]).toInt()].at(2)].second.append(dataRow);
                        }
                    }
                }
            }
        }
        reproMeasureFile.close();
    }

    // we fill the tables
    QTableWidgetItem *cell;
    int GURow(-1), GUCol;
    GUTab->setColumnCount(varNames.size() + 2);
    int reproRow(-1), reproCol;
    reproTab->setColumnCount(reproVarNames.size() + 2);
    QList<QVariant> datas;
    QPushButton *deleteRowButton;
    for (QMap<int, QPair<QList<QStringList>, QPair<QPair<int, QList<QStringList> >, QList<QPair<int, QList<QStringList> > > > > >::iterator GU(fullDatas.begin()); GU != fullDatas.end(); GU++)
    {
        for (QList<QStringList>::iterator measure(GU.value().first.begin()); measure != GU.value().first.end(); measure++)
        {
            GURow++;
            GUTab->insertRow(GURow);
            GUCol = 0;
            cell = new QTableWidgetItem;
            cell->setText(GUs.key(GU.key()));
            cell->setData(Qt::UserRole, QVariant((*measure).at(0).toInt()));
            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
            GUTab->setItem(GURow, 0, cell);
            for (QMap<int, QString>::iterator var(varNames.begin()); var != varNames.end(); var++)
            {
                datas.clear();
                GUCol++;
                cell = new QTableWidgetItem;
                if (varFactor[var.value()])
                {
                    datas.append(QVariant((*measure).at(0)));
                    datas.append(QVariant(var.value()));
                    datas.append(QVariant(var.key()));
                    datas.append(QVariant("FACTOR"));
                    datas.append(QVariant(factorCodes[var.value()]));
                    if ((*measure).size() > var.key())
                    {
                        if ((*measure).at(var.key()) != "")
                        {
                            //QMessageBox::information(NULL, var.value(), (*measure).at(var.key()));
                            cell->setText(factorCodes[var.value()][(*measure).at(var.key())].toString());
                        } else cell->setText("");
                    }
                }else
                {
                    if ((*measure).size() > var.key()) cell->setText((*measure).at(var.key()));
                    datas.append(QVariant((*measure).at(0)));
                    datas.append(QVariant(var.value()));
                    datas.append(QVariant(var.key()));
                    datas.append(QVariant(varType[var.value()]));
                }
                cell->setData(Qt::UserRole, QVariant(datas));
                cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                GUTab->setItem(GURow, GUCol, cell);
            }
            deleteRowButton = new QPushButton("Supprimer");
            QObject::connect(deleteRowButton, SIGNAL(clicked()), this, SLOT(deleteMeasure()));
            deleteRowButton->setObjectName(QString("Row") + QString::number(GURow));
            GUTab->setCellWidget(GURow, GUCol + 1, deleteRowButton);
        }
        // Male reproduction
        for (QList<QStringList>::iterator measure(GU.value().second.first.second.begin()); measure != GU.value().second.first.second.end(); measure++)
        {
            reproRow++;
            reproTab->insertRow(reproRow);
            reproCol = 0;
            cell = new QTableWidgetItem;
            cell->setText(repros.key(GU.value().second.first.first));
            cell->setData(Qt::UserRole, QVariant((*measure).at(0).toInt()));
            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
            reproTab->setItem(reproRow, 0, cell);
            for (QMap<int, QString>::iterator var(reproVarNames.begin()); var != reproVarNames.end(); var++)
            {
                datas.clear();
                reproCol++;
                cell = new QTableWidgetItem;
                if (reproVarFactor[var.value()])
                {
                    datas.append(QVariant((*measure).at(0)));
                    datas.append(QVariant(var.value()));
                    datas.append(QVariant(var.key()));
                    datas.append(QVariant("FACTOR"));
                    datas.append(QVariant(maleCodes[var.value()]));
                    if ((*measure).size() > var.key())
                    {
                        if ((*measure).at(var.key()) != "")
                        {
                            //QMessageBox::information(NULL, var.value(), (*measure).at(var.key()));
                            cell->setText(maleCodes[var.value()][(*measure).at(var.key())].toString());
                        } else cell->setText("");
                    }
                }else
                {
                    if ((*measure).size() > var.key()) cell->setText((*measure).at(var.key()));
                    datas.append(QVariant((*measure).at(0)));
                    datas.append(QVariant(var.value()));
                    datas.append(QVariant(var.key()));
                    datas.append(QVariant(reproVarType[var.value()]));
                }
                cell->setData(Qt::UserRole, QVariant(datas));
                cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                reproTab->setItem(reproRow, reproCol, cell);
            }
            deleteRowButton = new QPushButton("Supprimer");
            QObject::connect(deleteRowButton, SIGNAL(clicked()), this, SLOT(deleteMeasure()));
            deleteRowButton->setObjectName(QString("Row") + QString::number(reproRow));
            reproTab->setCellWidget(reproRow, reproCol + 1, deleteRowButton);
        }
        // Female reproduction
        for (QList<QPair<int, QList<QStringList> > >::iterator repro(GU.value().second.second.begin()); repro != GU.value().second.second.end(); repro++)
        {
            for (QList<QStringList>::iterator measure(repro->second.begin()); measure != repro->second.end(); measure++)
            {
                reproRow++;
                reproTab->insertRow(reproRow);
                reproCol = 0;
                cell = new QTableWidgetItem;
                cell->setText(repros.key(repro->first));
                cell->setData(Qt::UserRole, QVariant((*measure).at(0).toInt()));
                cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                reproTab->setItem(reproRow, 0, cell);
                for (QMap<int, QString>::iterator var(reproVarNames.begin()); var != reproVarNames.end(); var++)
                {
                    datas.clear();
                    reproCol++;
                    cell = new QTableWidgetItem;
                    if (reproVarFactor[var.value()])
                    {
                        datas.append(QVariant((*measure).at(0)));
                        datas.append(QVariant(var.value()));
                        datas.append(QVariant(var.key()));
                        datas.append(QVariant("FACTOR"));
                        datas.append(QVariant(femaleCodes[var.value()]));
                        if ((*measure).size() > var.key())
                        {
                            if ((*measure).at(var.key()) != "")
                            {
                                //QMessageBox::information(NULL, var.value(), (*measure).at(var.key()));
                                cell->setText(femaleCodes[var.value()][(*measure).at(var.key())].toString());
                            } else
                            {
                                cell->setText("");
                            }
                        }
                    }else
                    {
                        if ((*measure).size() > var.key()) cell->setText((*measure).at(var.key()));
                        datas.append(QVariant((*measure).at(0)));
                        datas.append(QVariant(var.value()));
                        datas.append(QVariant(var.key()));
                        datas.append(QVariant(reproVarType[var.value()]));
                    }
                    cell->setData(Qt::UserRole, QVariant(datas));
                    cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                    reproTab->setItem(reproRow, reproCol, cell);
                }
                deleteRowButton = new QPushButton("Supprimer");
                QObject::connect(deleteRowButton, SIGNAL(clicked()), this, SLOT(deleteMeasure()));
                deleteRowButton->setObjectName(QString("Row") + QString::number(reproRow));
                reproTab->setCellWidget(reproRow, reproCol + 1, deleteRowButton);
            }
        }
    }
    QList<QString> tabHeader;
    tabHeader.clear();
    tabHeader << "UC" << "L" << "IdUC";
    for (QMap<int, QString>::iterator var(varNames.begin()); var != varNames.end(); var++)
    {
        if ((var.value() != "IdM") && (var.value() != "Id3"))
        {
            tabHeader << var.value();
        }
    }
    tabHeader << "Supprimer";
    GUTab->setHorizontalHeaderLabels(QStringList(tabHeader));
    tabHeader.clear();
    tabHeader << "Repro" << "L" << "IdRepro";
    for (QMap<int, QString>::iterator var(reproVarNames.begin()); var != reproVarNames.end(); var++)
    {
        if ((var.value() != "IdRM") && (var.value() != "Id4"))
        {
            tabHeader << var.value();
        }
    }
    tabHeader << "Supprimer";
    reproTab->setHorizontalHeaderLabels(QStringList(tabHeader));

    QObject::connect(GUTab, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(modifiesValueData(int, int)));
    QObject::connect(reproTab, SIGNAL(cellDoubleClicked(int,int)), this, SLOT(modifiesValueData(int, int)));
}
