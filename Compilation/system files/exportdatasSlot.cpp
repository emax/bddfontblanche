#include <MainWindow.h>

void MainWindow::exportDatasSlot()
{
    // Empty mainLayout
    while(mainLayout->count() > 0){
        QLayoutItem *item = mainLayout->takeAt(0);
        delete item->widget();
        delete item;
    }
    delete mainLayout;
    mainLayout = new QGridLayout;
    setLayout(mainLayout);

    // 5 buttons to create
    QPushButton *exportAll = new QPushButton;
    QPushButton *exportFinal = new QPushButton;
    QPushButton *prepareCampaign = new QPushButton;
    QPushButton *backToPrevious = new QPushButton;
    QPushButton *backToMain = new QPushButton;

    // Labels for the buttons
    exportAll->setText("Exporter toutes les données");
    exportFinal->setText("Exporter les états finaux");
    prepareCampaign->setText("Préparer une campagne de terrain");
    backToPrevious->setText("Retour à la fenêtre précédante");
    backToMain->setText("Retour à la fenêtre principale");

    // Connect slots to signals clicked
    QObject::connect(exportAll, SIGNAL(clicked()), this, SLOT(exportAllSlot()));
    QObject::connect(exportFinal, SIGNAL(clicked()), this, SLOT(exportFinalSlot()));
    QObject::connect(prepareCampaign, SIGNAL(clicked()), this, SLOT(prepareCampaignSlot()));
    QObject::connect(backToPrevious, SIGNAL(clicked()), this, SLOT(backToConsultOrExport()));
    QObject::connect(backToMain, SIGNAL(clicked()), this, SLOT(backTo0Slot()));

    //Display buttons into the mainLayout
    mainLayout->addWidget(exportAll, 0, 0);
    mainLayout->addWidget(exportFinal, 1, 0);
    mainLayout->addWidget(prepareCampaign, 2, 0);
    mainLayout->addWidget(backToPrevious, 3, 0);
    mainLayout->addWidget(backToMain, 4, 0);

    setObjectName("exportData");
}
