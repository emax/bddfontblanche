#include <focuschangepolicy.h>

bool SelectFocus::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::FocusIn) {
        QString className(obj->metaObject()->className());
        if (className == "QSpinBox")
        {
            qobject_cast<QSpinBox*>(obj)->selectAll();
        }else if (className == "QDoubleSpinBox")
        {
            qobject_cast<QDoubleSpinBox*>(obj)->selectAll();
        }else if (className == "QLineEdit")
        {
            qobject_cast<QLineEdit*>(obj)->selectAll();
        }
        return true;
    } else {
        // standard event processing
        return QObject::eventFilter(obj, event);
    }
}
