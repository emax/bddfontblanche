#include <MainWindow.h>

void MainWindow::inputDataSet()
{
    if (objectName() != "inputData")
    {
        // We ask the user to give us an user name
        QString userName("");
        userName = QInputDialog::getText(NULL, "Nom d'utilisateur", "Dis moi quel est ton nom");
        if (userName != "")
        {
            // we remove forbidden characters from userName
            userName.remove("/");
            userName.remove("\\");
            userName.remove(":");
            userName.remove("*");
            userName.remove("?");
            userName.remove("\"");
            userName.remove("<");
            userName.remove(">");
            userName.remove("|");

            // then the name of the authors of the datas
            QString dataAuthor("");
            dataAuthor = QInputDialog::getText(NULL, "Nom des auteurs", "Maintenant il me faut le nom des auteurs des mesures");
            dataAuthor.remove("\n");
            dataAuthor.remove("\t");

            // then we need to have a date for the campaign
            bool *dateSet;
            dateSet = new bool(false);
            QDate measureDate(DialogueDate::getDate(NULL, QString("Date de mesure"),QString("Il me faut la date de la campagne de mesure"), QDate::currentDate(), dateSet));
            if (*dateSet)
            {
                QList<QVariant> datas;
                datas.append(QVariant(userName));
                datas.append(QVariant(dataAuthor));
                datas.append(QVariant(measureDate));
                userData = QVariant(datas);

                // We save the database
                QString folderName;
                folderName = QString("%1-%2.BM").arg(QDateTime::currentDateTime().toString("dd.MM.yyyy.hh.mm.ss.zzz")).arg(userName);
                QDir originDir("bin"), targetDir("historic");
                targetDir.mkdir(folderName);
                targetDir.cd(folderName);
                QStringList fileList;
                fileList = originDir.entryList(QStringList(), QDir::Files);
                for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
                {
                    QFile::copy(QString("bin/%1").arg(*it), QString("%1/%2").arg(targetDir.path()).arg(*it));
                }
                originDir.cd("dictionary");
                targetDir.mkdir("dictionary");
                targetDir.cd("dictionary");
                fileList = originDir.entryList(QStringList(), QDir::Files);
                for (QStringList::iterator it(fileList.begin()); it != fileList.end(); it++)
                {
                    QFile::copy(QString("bin/dictionary/%1").arg(*it), QString("%1/%2").arg(targetDir.path()).arg(*it));
                }

                // Empty mainLayout
                while(mainLayout->count() > 0){
                    QLayoutItem *item = mainLayout->takeAt(0);
                    delete item->widget();
                    delete item;
                }
                delete mainLayout;
                mainLayout = new QGridLayout;
                setLayout(mainLayout);

                // 5 buttons to create
                QPushButton *controlLabel = new QPushButton;
                QPushButton *rainExclusionLabel = new QPushButton;
                QPushButton *lightExclusionLabel = new QPushButton;
                QPushButton *irrigationLabel = new QPushButton;
                QPushButton *backButton = new QPushButton;
                QPushButton *backTo0 = new QPushButton;

                // Labels for the buttons
                controlLabel->setText("Témoin");
                rainExclusionLabel->setText("Exclusion");
                lightExclusionLabel->setText("Renversé");
                irrigationLabel->setText("Irrigation");
                backButton->setText("Retour à la fenêtre précédente");
                backTo0->setText("Retour à la fenêtre principale");
                controlLabel->setFlat(true);
                rainExclusionLabel->setFlat(true);
                lightExclusionLabel->setFlat(true);
                irrigationLabel->setFlat(true);

                // 4 plots button need to have a name
                controlLabel->setObjectName("Témoin");
                rainExclusionLabel->setObjectName("Exclusion");
                lightExclusionLabel->setObjectName("Renversé");
                irrigationLabel->setObjectName("Irrigation");

                // Connect slots to signals clicked
                QObject::connect(controlLabel, SIGNAL(clicked()), this, SLOT(inputData()));
                QObject::connect(rainExclusionLabel, SIGNAL(clicked()), this, SLOT(inputData()));
                QObject::connect(lightExclusionLabel, SIGNAL(clicked()), this, SLOT(inputData()));
                QObject::connect(irrigationLabel, SIGNAL(clicked()), this, SLOT(inputData()));
                QObject::connect(backButton, SIGNAL(clicked()), this, SLOT(saveDataContext()));
                QObject::connect(backTo0, SIGNAL(clicked()), this, SLOT(backTo0Slot()));

                //Display buttons into the mainLayout
                mainLayout->addWidget(controlLabel, 0, 0);
                mainLayout->addWidget(rainExclusionLabel, 1, 0);
                mainLayout->addWidget(lightExclusionLabel, 0, 1);
                mainLayout->addWidget(irrigationLabel, 1, 1);
                mainLayout->addWidget(backButton, 2, 0, 1, 2);
                mainLayout->addWidget(backTo0, 3, 0, 1, 2);
                setObjectName("inputDataSet");
            }
        }
    }else{
        // Empty mainLayout
        while(mainLayout->count() > 0){
            QLayoutItem *item = mainLayout->takeAt(0);
            delete item->widget();
            delete item;
        }
        delete mainLayout;
        mainLayout = new QGridLayout;
        setLayout(mainLayout);

        // 5 buttons to create
        QPushButton *controlLabel = new QPushButton;
        QPushButton *rainExclusionLabel = new QPushButton;
        QPushButton *lightExclusionLabel = new QPushButton;
        QPushButton *irrigationLabel = new QPushButton;
        QPushButton *backButton = new QPushButton;
        QPushButton *backTo0 = new QPushButton;

        // Labels for the buttons
        controlLabel->setText("Témoin");
        rainExclusionLabel->setText("Exclusion");
        lightExclusionLabel->setText("Renversé");
        irrigationLabel->setText("Irrigation");
        backButton->setText("Retour à la fenêtre précédente");
        backTo0->setText("Retour à la fenêtre principale");
        controlLabel->setFlat(true);
        rainExclusionLabel->setFlat(true);
        lightExclusionLabel->setFlat(true);
        irrigationLabel->setFlat(true);

        // 4 plots button need to have a name
        controlLabel->setObjectName("Témoin");
        rainExclusionLabel->setObjectName("Exclusion");
        lightExclusionLabel->setObjectName("Renversé");
        irrigationLabel->setObjectName("Irrigation");

        // Connect slots to signals clicked
        QObject::connect(controlLabel, SIGNAL(clicked()), this, SLOT(inputData()));
        QObject::connect(rainExclusionLabel, SIGNAL(clicked()), this, SLOT(inputData()));
        QObject::connect(lightExclusionLabel, SIGNAL(clicked()), this, SLOT(inputData()));
        QObject::connect(irrigationLabel, SIGNAL(clicked()), this, SLOT(inputData()));
        QObject::connect(backButton, SIGNAL(clicked()), this, SLOT(saveDataContext()));
        QObject::connect(backTo0, SIGNAL(clicked()), this, SLOT(backTo0Slot()));

        //Display buttons into the mainLayout
        mainLayout->addWidget(controlLabel, 0, 0);
        mainLayout->addWidget(rainExclusionLabel, 1, 0);
        mainLayout->addWidget(lightExclusionLabel, 0, 1);
        mainLayout->addWidget(irrigationLabel, 1, 1);
        mainLayout->addWidget(backButton, 2, 0, 1, 2);
        mainLayout->addWidget(backTo0, 3, 0, 1, 2);
        setObjectName("inputDataSet");
    }
}
