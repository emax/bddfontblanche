#include <MainWindow.h>

void MainWindow::loadPreviousDatas()
{
    // Empty mainLayout
    while(mainLayout->count() > 0){
        QLayoutItem *item = mainLayout->takeAt(0);
        delete item->widget();
        delete item;
    }
    delete mainLayout;
    mainLayout = new QGridLayout;
    setLayout(mainLayout);

    // 3 buttons to create
    QPushButton *loadDatasButton = new QPushButton;
    QPushButton *backButton = new QPushButton;
    QPushButton *backToMain = new QPushButton;

    // Labels for the buttons
    loadDatasButton->setText("Charger la sauvegarde sélectionnée");
    backButton->setText("Retour à la page précédente");
    backToMain->setText("Retour à la fenêtre principale");

    // a QLabel and a table
    QLabel *title = new QLabel;
    QTableWidget *saveTable = new QTableWidget(0, 3);
    title->setText("Charger une précédente sauvegarde");

    // We have to fill the table
    saveTable->verticalHeader()->hide();
    QList<QString> tabHeader;
    tabHeader << "Date" << "Opérateur" << "Contexte";
    saveTable->setHorizontalHeaderLabels(QStringList(tabHeader));
    tabHeader.clear();
    saveTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    saveTable->setSelectionMode(QAbstractItemView::SingleSelection);
    QStringList dirList, tempList;
    QString dateString, userString, contextString, tempString;
    dirList = QDir("historic").entryList(QStringList(), QDir::Dirs);
    QTableWidgetItem *cell;
    int countLine(0);
    for (QStringList::iterator it = dirList.begin(); it != dirList.end(); it++)
    {
        if (((*it) != ".") && ((*it) != ".."))
        {
            saveTable->insertRow(countLine);
            tempList = (*it).split("-").at(0).split(".");
            if (tempList.size() > 6) dateString = tempList.at(0) + "/" + tempList.at(1) + "/" + tempList.at(2) + " (" + tempList.at(3) + ":" + tempList.at(4) + ":" + tempList.at(5) + "." + tempList.at(6) + ")";
            tempList = (*it).split("-");
            tempList.removeFirst();
            tempString = tempList.join("-");
            tempList = tempString.split(".");
            contextString = tempList.last();
            tempList.removeLast();
            userString = tempList.join(".");
            if (contextString == "BF")
            {
                contextString = "Avant export de terrain";
            }else if (contextString == "BM")
            {
                contextString = "Avant saisie de données";
            }else if (contextString == "BNV")
            {
                contextString = "Avant ajout de nouvelle variable";
            }else if (contextString == "BCV")
            {
                contextString = "Avant chargement d'une sauvegarde précédente";
            }else if (contextString == "BCC")
            {
                contextString = "Avant changement de caractéristiques d'axe";
            }else if (contextString == "BDM")
            {
                contextString = "Avant modification de données enregistrées";
            }
            cell = new QTableWidgetItem;
            cell->setData(Qt::UserRole, QVariant(*it));
            cell->setText(dateString);
            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
            saveTable->setItem(countLine, 0, cell);
            cell = new QTableWidgetItem;
            cell->setData(Qt::UserRole, QVariant(*it));
            cell->setText(userString);
            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
            saveTable->setItem(countLine, 1, cell);
            cell = new QTableWidgetItem;
            cell->setData(Qt::UserRole, QVariant(*it));
            cell->setText(contextString);
            cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
            saveTable->setItem(countLine, 2, cell);
            countLine++;
        }
    }

    // Connect slots to signals clicked
    QObject::connect(loadDatasButton, SIGNAL(clicked()), this, SLOT(loadDatas()));
    //QObject::connect(saveTable, SIGNAL(doubleClicked()), this, SLOT(loadDatas()));
    QObject::connect(backButton, SIGNAL(clicked()), this, SLOT(modifiesDataBase()));
    QObject::connect(backToMain, SIGNAL(clicked()), this, SLOT(backTo0Slot()));

    //Display buttons into the mainLayout
    mainLayout->addWidget(title, 0, 0);
    mainLayout->addWidget(saveTable, 1, 0);
    mainLayout->addWidget(loadDatasButton, 2, 0);
    mainLayout->addWidget(backButton, 3, 0);
    mainLayout->addWidget(backToMain, 4, 0);

    setObjectName("loadDatas");
}
