#include <QApplication>
#include <QTextCodec>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <QString>

#include "MainWindow.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < 0x050000
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
#endif
    QApplication app(argc, argv);

    // Reading file state
    QFile state("state");
    if (!state.open(QIODevice::ReadWrite | QIODevice::Text)) return app.exec();

    QTextStream in(&state);
    QString line = in.readLine();
    state.close();
    MainWindow window(&line);

    if (line == "2")
    {
        // State 2 : Database being modified by another user
        QMessageBox::critical(NULL, "Impossible de lire la base de données", "Des modifications de la base de données sont en cours, vous ne pouvez pas la consulter!");
    }else{
        // We can create the main window
        window.show();
    }

    return app.exec();
}
