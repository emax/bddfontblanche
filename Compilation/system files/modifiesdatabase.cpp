#include <MainWindow.h>

void MainWindow::modifiesDataBase()
{
    // Empty mainLayout
    while(mainLayout->count() > 0){
        QLayoutItem *item = mainLayout->takeAt(0);
        delete item->widget();
        delete item;
    }
    delete mainLayout;
    mainLayout = new QGridLayout;
    setLayout(mainLayout);

    // Set state file to 2
    QFile stateFile("state");
    stateFile.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream in(&stateFile);
    in << 2;
    stateFile.close();

    // 5 buttons to create
    QPushButton *modifiesDatasButton = new QPushButton;
    QPushButton *changeAxisCharacteristicsButton = new QPushButton;
    QPushButton *addParameterButton = new QPushButton;
    QPushButton *loadPreviousDatasButton = new QPushButton;
    QPushButton *backToMain = new QPushButton;

    // Labels for the buttons
    modifiesDatasButton->setText("Modifier/Supprimer des données");
    changeAxisCharacteristicsButton->setText("Changer les caractéristiques/noms des axes");
    addParameterButton->setText("Ajouter des variables à suivre");
    loadPreviousDatasButton->setText("Charger une version précédente des données");
    backToMain->setText("Retour à la fenêtre principale");

    // Connect slots to signals clicked
    QObject::connect(modifiesDatasButton, SIGNAL(clicked()), this, SLOT(modifiesDatas()));
    QObject::connect(changeAxisCharacteristicsButton, SIGNAL(clicked()), this, SLOT(changeAxisCharacteristics()));
    QObject::connect(addParameterButton, SIGNAL(clicked()), this, SLOT(addParameter()));
    QObject::connect(loadPreviousDatasButton, SIGNAL(clicked()), this, SLOT(loadPreviousDatas()));
    QObject::connect(backToMain, SIGNAL(clicked()), this, SLOT(backTo0Slot()));

    //Display buttons into the mainLayout
    mainLayout->addWidget(modifiesDatasButton, 0, 0);
    mainLayout->addWidget(changeAxisCharacteristicsButton, 1, 0);
    mainLayout->addWidget(addParameterButton, 2, 0);
    mainLayout->addWidget(loadPreviousDatasButton, 3, 0);
    mainLayout->addWidget(backToMain, 4, 0);

    setObjectName("modifiesDataBase");
}
