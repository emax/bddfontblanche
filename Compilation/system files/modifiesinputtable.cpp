#include <MainWindow.h>

void MainWindow::modifiesInputTable(int a, int b)
{
    if (b == 0)
    {
        QTableWidget *dataTable = qobject_cast<QTableWidget *>(QObject::sender());
        dataTable->insertRow(a + 1);

        // we first read the variables
        QFile varFile("bin/dictionary/variables");
        int Id, countLine(-1);
        QMap<QString, int> reproVar;
        QMap<int, QStringList> varMap;
        QFile * tempFile;
        QMap<QString, QMap<QString, int> > factorPosition;
        QMap<QString, QMap<int, QStringList> > factorMap;
        QMap<QString, int> tempPosition;
        QMap<int, QStringList> tempMap;
        QString line;
        QStringList dataRow;
        int tempId;
        if (!varFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/variables.");
            urgentStop();
            return;
        }else{
            QTextStream varStream(&varFile);
            line = varStream.readLine();
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/variables\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!varStream.atEnd()  && line != "###")
                {
                    line = varStream.readLine();
                    reproVar.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
                while(!varStream.atEnd())
                {
                    dataRow = varStream.readLine().split("\t");
                    Id = dataRow.at(0).toInt();
                    dataRow.removeFirst();
                    varMap.insert(Id, dataRow);
                    if (varMap[Id].size() > reproVar["TypeVar"])
                    {
                        if (varMap[Id].at(reproVar["TypeVar"]).startsWith("eKEY"))
                        {
                            // factor variable
                            if (dataRow.at(reproVar["TypeVar"]).split(":").size() > 1) tempFile = new QFile(QString("bin/%1").arg(dataRow.at(reproVar["TypeVar"]).split(":").at(1)));
                            countLine = -1;
                            tempPosition.clear();
                            tempMap.clear();
                            if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                urgentStop();
                                return;
                            }else{
                                QTextStream tempStream(tempFile);
                                line = tempStream.readLine();
                                if (line != "###")
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                    urgentStop();
                                    return;
                                }else{
                                    line = "";
                                    while (!tempStream.atEnd()  && line != "###")
                                    {
                                        line = tempStream.readLine();
                                        tempPosition.insert(line.split("\t").at(0), countLine);
                                        countLine++;
                                    }
                                    while(!tempStream.atEnd())
                                    {
                                        dataRow = tempStream.readLine().split("\t");
                                        tempId = dataRow.at(0).toInt();
                                        dataRow.removeFirst();
                                        tempMap.insert(tempId, dataRow);
                                    }
                                }
                            }
                            tempFile->close();
                            if (varMap[Id].size() > reproVar["Name"])
                            {
                                factorPosition.insert(varMap[Id].at(reproVar["Name"]), tempPosition);
                                factorMap.insert(varMap[Id].at(reproVar["Name"]), tempMap);
                            }
                        }
                    }
                }
            }
            // then reproduction variables
            QFile reproVarFile("bin/dictionary/reproductionVariables");
            countLine = -1;
            QMap<QString, int> reproreproVar;
            QMap<int, QStringList> reproVarMap;
            if (!reproVarFile.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/reproductionVariables.");
                urgentStop();
                return;
            }else{
                QTextStream reproVarStream(&reproVarFile);
                line = reproVarStream.readLine();
                if (line != "###")
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/reproductionVariables\nMauvaise entête (\"###\" attendu)");
                    urgentStop();
                    return;
                }else{
                    line = "";
                    while (!reproVarStream.atEnd()  && line != "###")
                    {
                        line = reproVarStream.readLine();
                        reproreproVar.insert(line.split("\t").at(0), countLine);
                        countLine++;
                    }
                    while(!reproVarStream.atEnd())
                    {
                        dataRow = reproVarStream.readLine().split("\t");
                        Id = dataRow.at(0).toInt();
                        dataRow.removeFirst();
                        reproVarMap.insert(Id, dataRow);
                        if (reproVarMap[Id].size() > reproreproVar["TypeVar"])
                        {
                            if (reproVarMap[Id].at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                            {
                                // factor variable
                                if (dataRow.at(reproreproVar["TypeVar"]).split(":").size() > 1) tempFile = new QFile(QString("bin/%1").arg(dataRow.at(reproreproVar["TypeVar"]).split(":").at(1)));
                                countLine = -1;
                                tempPosition.clear();
                                tempMap.clear();
                                if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                    urgentStop();
                                    return;
                                }else{
                                    QTextStream tempStream(tempFile);
                                    line = tempStream.readLine();
                                    if (line != "###")
                                    {
                                        // Error
                                        QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                        urgentStop();
                                        return;
                                    }else{
                                        line = "";
                                        while (!tempStream.atEnd()  && line != "###")
                                        {
                                            line = tempStream.readLine();
                                            tempPosition.insert(line.split("\t").at(0), countLine);
                                            countLine++;
                                        }
                                        while(!tempStream.atEnd())
                                        {
                                            dataRow = tempStream.readLine().split("\t");
                                            tempId = dataRow.at(0).toInt();
                                            dataRow.removeFirst();
                                            tempMap.insert(tempId, dataRow);
                                        }
                                    }
                                }
                                tempFile->close();
                                if (reproVarMap[Id].size() > reproreproVar["Name"])
                                {
                                    factorPosition.insert(reproVarMap[Id].at(reproreproVar["Name"]), tempPosition);
                                    factorMap.insert(reproVarMap[Id].at(reproreproVar["Name"]), tempMap);
                                }
                            }
                        }
                    }
                    // we can now fill the new line
                    QTableWidgetItem *cell;
                    cell = new QTableWidgetItem;
                    int col(0);
                    cell->setData(Qt::UserRole, QVariant("New GU"));
                    // GU Name
                    QString Year;
                    QFont cellFont;
                    int NGU(0);
                    if (dataTable->rowCount() == (a + 2))
                    {
                        Year = "----";
                        if (dataTable->item(a, 0)->text().startsWith("----"))
                        {
                            if (dataTable->item(a, 0)->text().split(" . ").size() > 1) NGU = dataTable->item(a, 0)->text().split(" . ").at(1).toInt();
                        }
                        NGU--;
                    }else{
                        Year = dataTable->item(a + 2, 0)->text().split(" . ").at(0);
                        if (dataTable->item(a + 2, 0)->text().split(" . ").size() > 1) NGU = dataTable->item(a + 2, 0)->text().split(" . ").at(1).toInt() + 1;
                        if (dataTable->item(a, 0)->text().split(" . ").at(0) == Year)
                        {
                            int r(a);
                            bool test(true);
                            while (test)
                            {
                                if (r >= 0)
                                {
                                    if (dataTable->item(r, 0)->text().split(" . ").at(0) == Year)
                                    {
                                        if (dataTable->item(r, 0)->text().split(" . ").size() > 1) dataTable->item(r, 0)->setText(QString("%1 . %2").arg(Year).arg(dataTable->item(r, 0)->text().split(" . ").at(1).toInt() + 1));
                                        cellFont = dataTable->item(r, 0)->font();
                                        cellFont.setBold(true);
                                        dataTable->item(r, 0)->setFont(cellFont);
                                        r--;
                                    }else{
                                        test = false;
                                    }
                                }else{
                                    test = false;
                                }
                            }
                        }
                    }
                    cell->setText(QString("%1 . %2").arg(Year).arg(NGU));
                    cellFont = cell->font();
                    cellFont.setItalic(true);
                    cell->setFont(cellFont);
                    cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                    qobject_cast<QTableWidget *>(dataTable)->setItem(a + 1, col, cell);
                    // first the measure
                    QString value, historic;
                    QMap<QString, QVariant> datas;
                    QSpinBox *intEdit;
                    QDoubleSpinBox *doubleEdit;
                    QLineEdit *textEdit;
                    QComboBox *factorEdit;
                    QMap<QString, QString> factorCodes;
                    int index;
                    for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                    {
                        if (var->size() > reproVar["AppearingVar"])
                        {
                            if (var->at(reproVar["AppearingVar"]) == "Y")
                            {
                                col++;
                                value = "";
                                datas = *(new QMap<QString, QVariant>);
                                historic = "%1";
                                if (var->size() > reproVar["TypeVar"])
                                {
                                    if (var->at(reproVar["TypeVar"]) == "INTEGER")
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        intEdit = new QSpinBox;
                                        intEdit->setMaximum(std::numeric_limits<int>::max());
                                        intEdit->setMinimum(std::numeric_limits<int>::min());
                                        intEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproVar["UnitVar"])
                                        {
                                            if (var->at(reproVar["UnitVar"]) != "")
                                            {
                                                intEdit->setSuffix(QString(" %1").arg(var->at(reproVar["UnitVar"])));
                                            }
                                        }
                                        if (var->size() > reproVar["InvreasingVar"] && var->size() > reproVar["DecreasingVar"])
                                        {
                                            if (var->at(reproVar["IncreasingVar"]) == "Y")
                                            {
                                                intEdit->setProperty("i", QVariant("In"));
                                            }else if (var->at(reproVar["DecreasingVar"]) == "Y")
                                            {
                                                intEdit->setProperty("i", QVariant("De"));
                                            }else{
                                                intEdit->setProperty("i", QVariant("No"));
                                            }
                                        }
                                        if (var->size() > reproVar["MinVarlueVar"])
                                        {
                                            if (var->at(reproVar["MinValueVar"]) != "")
                                            {
                                                intEdit->setMinimum(var->at(reproVar["MinValueVar"]).toInt());
                                            }
                                        }
                                        if (var->size() > reproVar["MaxValueVar"])
                                        {
                                            if (var->at(reproVar["MaxValueVar"]) != "")
                                            {
                                                intEdit->setMaximum(var->at(reproVar["MaxValueVar"]).toInt());
                                            }
                                        }
                                        if (var->size() > reproVar["ExplainVar"])
                                        {
                                            intEdit->setProperty("e", QVariant(var->at(reproVar["ExplainVar"])));
                                            intEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        SelectFocus *test = new SelectFocus;
                                        intEdit->installEventFilter(test);
                                        QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                        intEdit->setFont(cellFont);
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(a + 1, col, intEdit);
                                    }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        doubleEdit = new QDoubleSpinBox;
                                        doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                        doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                        doubleEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproVar["UnitVar"])
                                        {
                                            if (var->at(reproVar["UnitVar"]) != "")
                                            {
                                                doubleEdit->setSuffix(QString(" %1").arg(var->at(reproVar["UnitVar"])));
                                            }
                                        }
                                        if (var->size() > reproVar["IncreasingVar"] && var->size() > reproVar["DecreasingVar"])
                                        {
                                            if (var->at(reproVar["IncreasingVar"]) == "Y")
                                            {
                                                doubleEdit->setProperty("i", QVariant("In"));
                                            }else if (var->at(reproVar["DecreasingVar"]) == "Y")
                                            {
                                                doubleEdit->setProperty("i", QVariant("De"));
                                            }else{
                                                doubleEdit->setProperty("i", QVariant("No"));
                                            }
                                        }
                                        if (var->size() > reproVar["MinValueVar"])
                                        {
                                            if (var->at(reproVar["MinValueVar"]) != "")
                                            {
                                                doubleEdit->setMinimum(var->at(reproVar["MinValueVar"]).toDouble());
                                            }
                                        }
                                        if (var->size() > reproVar["MaxValueVar"])
                                        {
                                            if (var->at(reproVar["MaxValueVar"]) != "")
                                            {
                                                doubleEdit->setMaximum(var->at(reproVar["MaxValueVar"]).toDouble());
                                            }
                                        }
                                        if (var->size() > reproVar["ExplainVar"])
                                        {
                                            doubleEdit->setProperty("e", QVariant(var->at(reproVar["ExplainVar"])));
                                            doubleEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        SelectFocus *test = new SelectFocus;
                                        doubleEdit->installEventFilter(test);
                                        doubleEdit->setFont(cellFont);
                                        QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(a + 1, col, doubleEdit);
                                    }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        textEdit = new QLineEdit;
                                        textEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproVar["ExplainVar"])
                                        {
                                            textEdit->setProperty("e", QVariant(var->at(reproVar["ExplainVar"])));
                                            textEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        SelectFocus *test = new SelectFocus;
                                        textEdit->installEventFilter(test);
                                        textEdit->setFont(cellFont);
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(a + 1, col, textEdit);
                                    }else if (var->at(reproVar["TypeVar"]).startsWith("eKEY"))
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        factorEdit = new QComboBox;
                                        factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                        factorEdit->setEditable(true);
                                        index = 0;
                                        factorCodes.clear();
                                        if (var->size() > reproVar["Name"])
                                        {
                                            for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproVar["Name"])].begin()); it != factorMap[var->at(reproVar["Name"])].end(); it++)
                                            {
                                                //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproVar["Name"])]["Code"]));
                                                if (it->size() > factorPosition[var->at(reproVar["Name"])]["Code"])
                                                {
                                                    factorEdit->addItem(it->at(factorPosition[var->at(reproVar["Name"])]["Code"]), QVariant(it.key()));
                                                    index++;
                                                    if (it->size() > factorPosition[var->at(reproVar["Name"])]["Explain"]) factorCodes.insert(it->at(factorPosition[var->at(reproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproVar["Name"])]["Explain"]));
                                                }
                                            }
                                        }
                                        factorEdit->setCurrentIndex(-1);
                                        factorEdit->model()->sort(0);
                                        factorEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproVar["ExplainVar"])
                                        {
                                            factorEdit->setProperty("e", QVariant(var->at(reproVar["ExplainVar"])));
                                            historic = historic.arg(var->at(reproVar["ExplainVar"]) + "%1");
                                        }
                                        for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                        {
                                            historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                        }
                                        factorEdit->setToolTip(historic.arg("").replace("#", "Male"));
                                        factorEdit->setFont(cellFont);
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                        QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(a + 1, col, factorEdit);
                                    }else{
                                        cell = new QTableWidgetItem;
                                        cell->setData(Qt::UserRole, QVariant(datas));
                                        cell->setStatusTip(value);
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        cell->setFont(cellFont);
                                        qobject_cast<QTableWidget *>(dataTable)->setItem(a + 1, col, cell);
                                    }
                                }
                            }
                        }
                    }
                    // then reproduction
                    datas = *(new QMap<QString, QVariant>);
                    for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                    {
                        if (var->size() > reproreproVar["Target"] && var->size() > reproreproVar["AppearingVar"])
                        {
                            if (((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Male")) && (var->at(reproreproVar["AppearingVar"]) == "Y"))
                            {
                                col++;
                                value = "";
                                datas = *(new QMap<QString, QVariant>);
                                historic = "";
                                historic = "%1" + historic;
                                if (var->size() > reproreproVar["TypeVar"])
                                {
                                    if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        intEdit = new QSpinBox;
                                        intEdit->setMaximum(std::numeric_limits<int>::max());
                                        intEdit->setMinimum(std::numeric_limits<int>::min());
                                        intEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproreproVar["UnitVar"])
                                        {
                                            if (var->at(reproreproVar["UnitVar"]) != "")
                                            {
                                                intEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                            }
                                        }
                                        if (var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"])
                                        {
                                            if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                            {
                                                intEdit->setProperty("i", QVariant("In"));
                                            }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                            {
                                                intEdit->setProperty("i", QVariant("De"));
                                            }else{
                                                intEdit->setProperty("i", QVariant("No"));
                                            }
                                        }
                                        if (var->size() > reproreproVar["MinValueVar"])
                                        {
                                            if (var->at(reproreproVar["MinValueVar"]) != "")
                                            {
                                                intEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toInt());
                                            }
                                        }
                                        if (var->size() > reproreproVar["MaxValueVar"])
                                        {
                                            if (var->at(reproreproVar["MaxValueVar"]) != "")
                                            {
                                                intEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toInt());
                                            }
                                        }
                                        if (var->size() > reproreproVar["ExplainVar"])
                                        {
                                            intEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                            intEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        SelectFocus *test = new SelectFocus;
                                        intEdit->installEventFilter(test);
                                        intEdit->setFont(cellFont);
                                        QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(a + 1, col, intEdit);
                                    }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        doubleEdit = new QDoubleSpinBox;
                                        doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                        doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                        doubleEdit->setProperty("d", QVariant(datas));
                                        //if (value != "") doubleEdit->setValue(value.toDouble());
                                        if (var->size() > reproreproVar["UnitVar"])
                                        {
                                            if (var->at(reproreproVar["UnitVar"]) != "")
                                            {
                                                doubleEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                            }
                                        }
                                        if (var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"])
                                        {
                                            if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                            {
                                                doubleEdit->setProperty("i", QVariant("In"));
                                            }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                            {
                                                doubleEdit->setProperty("i", QVariant("De"));
                                            }else{
                                                doubleEdit->setProperty("i", QVariant("No"));
                                            }
                                        }
                                        if (var->size() > reproreproVar["MinValueVar"])
                                        {
                                            if (var->at(reproreproVar["MinValueVar"]) != "")
                                            {
                                                doubleEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toDouble());
                                            }
                                        }
                                        if (var->size() > reproreproVar["MaxValueVar"])
                                        {
                                            if (var->at(reproreproVar["MaxValueVar"]) != "")
                                            {
                                                doubleEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toDouble());
                                            }
                                        }
                                        if (var->size() > reproreproVar["ExplainVar"])
                                        {
                                            doubleEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                            doubleEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        SelectFocus *test = new SelectFocus;
                                        doubleEdit->installEventFilter(test);
                                        doubleEdit->setFont(cellFont);
                                        QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(a + 1, col, doubleEdit);
                                    }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                    {
                                        //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                        textEdit = new QLineEdit;
                                        textEdit->setProperty("d", QVariant(datas));
                                        //if (value != "") textEdit->setPlaceholderText(value);
                                        if (var->size() > reproreproVar["ExplainVar"])
                                        {
                                            textEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                            textEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        SelectFocus *test = new SelectFocus;
                                        textEdit->installEventFilter(test);
                                        textEdit->setFont(cellFont);
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(a + 1, col, textEdit);
                                    }else if (var->at(reproVar["TypeVar"]).startsWith("eKEY"))
                                    {
                                        //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                        factorEdit = new QComboBox;
                                        factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                        factorEdit->setEditable(true);
                                        index = 0;
                                        factorCodes.clear();
                                        if (var->size() > reproreproVar["Name"])
                                        {
                                            for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproreproVar["Name"])].begin()); it != factorMap[var->at(reproreproVar["Name"])].end(); it++)
                                            {
                                                //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]));
                                                if (it->at(factorPosition[var->at(reproreproVar["Name"])]["IdSex"]) == "1")
                                                {
                                                    if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["Code"])
                                                    {
                                                        factorEdit->addItem(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), QVariant(it.key()));
                                                        //if (value != "" && value == it->at(0)) factorEdit->setCurrentIndex(index);
                                                        index++;
                                                        if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["Explain"]) factorCodes.insert(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproreproVar["Name"])]["Explain"]));
                                                    }
                                                }
                                            }
                                        }
                                        factorEdit->setCurrentIndex(-1);
                                        factorEdit->model()->sort(0);
                                        factorEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproreproVar["ExplainVar"])
                                        {
                                            factorEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                            historic = historic.arg(var->at(reproreproVar["ExplainVar"]) + "%1");
                                        }
                                        for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                        {
                                            historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                        }
                                        factorEdit->setToolTip(historic.arg("").replace("#", "Male"));
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        factorEdit->setFont(cellFont);
                                        QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                        QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(a + 1, col, factorEdit);
                                    }else{
                                        cell = new QTableWidgetItem;
                                        cell->setData(Qt::UserRole, QVariant(datas));
                                        cell->setStatusTip(value);
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        cell->setFont(cellFont);
                                        qobject_cast<QTableWidget *>(dataTable)->setItem(a + 1, col, cell);
                                    }
                                }
                            }
                        }
                    }
                    while (col <= (dataTable->columnCount() - 1))
                    {
                        for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                        {
                            if (var->size() > reproreproVar["Target"] && var->size() > reproreproVar["AppearingVar"])
                            {
                                if (((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Female")) && (var->at(reproreproVar["AppearingVar"]) == "Y"))
                                {
                                    col++;
                                    value = "";
                                    datas = *(new QMap<QString, QVariant>);
                                    historic = "%1";
                                    if (var->size() > reproreproVar["TypeVar"])
                                    {
                                        if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                        {
                                            //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                            intEdit = new QSpinBox;
                                            intEdit->setMaximum(std::numeric_limits<int>::max());
                                            intEdit->setMinimum(std::numeric_limits<int>::min());
                                            intEdit->setProperty("d", QVariant(datas));
                                            if (var->size() > reproreproVar["UnitVar"])
                                            {
                                                if (var->at(reproreproVar["UnitVar"]) != "")
                                                {
                                                    intEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                }
                                            }
                                            if (var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"])
                                            {
                                                if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                {
                                                    intEdit->setProperty("i", QVariant("In"));
                                                }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                {
                                                    intEdit->setProperty("i", QVariant("De"));
                                                }else{
                                                    intEdit->setProperty("i", QVariant("No"));
                                                }
                                            }
                                            if (var->size() > reproreproVar["MinValueVar"])
                                            {
                                                if (var->at(reproreproVar["MinValueVar"]) != "")
                                                {
                                                    intEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toInt());
                                                }
                                            }
                                            if (var->size() > reproreproVar["MaxValueVar"])
                                            {
                                                if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                {
                                                    intEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toInt());
                                                }
                                            }
                                            if (var->size() > reproreproVar["ExplainVar"])
                                            {
                                                intEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                intEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                            }
                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                            SelectFocus *test = new SelectFocus;
                                            intEdit->installEventFilter(test);
                                            intEdit->setFont(cellFont);
                                            QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                            qobject_cast<QTableWidget *>(dataTable)->setCellWidget(a + 1, col, intEdit);
                                        }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                        {
                                            //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                            doubleEdit = new QDoubleSpinBox;
                                            doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                            doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                            doubleEdit->setProperty("d", QVariant(datas));
                                            if (var->size() > reproreproVar["UnitVar"])
                                            {
                                                if (var->at(reproreproVar["UnitVar"]) != "")
                                                {
                                                    doubleEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                }
                                            }
                                            if (var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"])
                                            {
                                                if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                {
                                                    doubleEdit->setProperty("i", QVariant("In"));
                                                }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                {
                                                    doubleEdit->setProperty("i", QVariant("De"));
                                                }else{
                                                    doubleEdit->setProperty("i", QVariant("No"));
                                                }
                                            }
                                            if (var->size() > reproreproVar["MinValueVar"])
                                            {
                                                if (var->at(reproreproVar["MinValueVar"]) != "")
                                                {
                                                    doubleEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toDouble());
                                                }
                                            }
                                            if (var->size() > reproreproVar["MaxValueVar"])
                                            {
                                                if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                {
                                                    doubleEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toDouble());
                                                }
                                            }
                                            if (var->size() > reproreproVar["ExplainVar"])
                                            {
                                                doubleEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                doubleEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                            }
                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                            SelectFocus *test = new SelectFocus;
                                            doubleEdit->installEventFilter(test);
                                            doubleEdit->setFont(cellFont);
                                            QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                            qobject_cast<QTableWidget *>(dataTable)->setCellWidget(a + 1, col, doubleEdit);
                                        }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                        {
                                            //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                            textEdit = new QLineEdit;
                                            textEdit->setProperty("d", QVariant(datas));
                                            if (var->size() > reproreproVar["ExplainVar"])
                                            {
                                                textEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                textEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                            }
                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                            //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                            SelectFocus *test = new SelectFocus;
                                            textEdit->installEventFilter(test);
                                            textEdit->setFont(cellFont);
                                            qobject_cast<QTableWidget *>(dataTable)->setCellWidget(a + 1, col, textEdit);
                                        }else if (var->at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                                        {
                                            //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                            factorEdit = new QComboBox;
                                            factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                            factorEdit->setEditable(true);
                                            index = 0;
                                            factorCodes.clear();
                                            if (var->size() > reproreproVar["Name"])
                                            {
                                                for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproreproVar["Name"])].begin()); it != factorMap[var->at(reproreproVar["Name"])].end(); it++)
                                                {
                                                    //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]));
                                                    if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["IdSex"])
                                                    {
                                                        if (it->at(factorPosition[var->at(reproreproVar["Name"])]["IdSex"]) == "2" && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Code"])
                                                        {
                                                            factorEdit->addItem(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), QVariant(it.key()));
                                                            index++;
                                                            if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["Explain"]) factorCodes.insert(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproreproVar["Name"])]["Explain"]));
                                                        }
                                                    }
                                                }
                                            }
                                            factorEdit->setCurrentIndex(-1);
                                            factorEdit->model()->sort(0);
                                            factorEdit->setProperty("d", QVariant(datas));
                                            if (var->size() > reproreproVar["ExplainVar"])
                                            {
                                                factorEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                historic = historic.arg(var->at(reproreproVar["ExplainVar"]) + "%1");
                                            }
                                            for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                            {
                                                historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                            }
                                            factorEdit->setToolTip(historic.arg("").replace("#", "Femelle"));
                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                            //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                            factorEdit->setFont(cellFont);
                                            QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                            QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                            qobject_cast<QTableWidget *>(dataTable)->setCellWidget(a + 1, col, factorEdit);
                                        }else{
                                            cell = new QTableWidgetItem;
                                            cell->setData(Qt::UserRole, QVariant(datas));
                                            cell->setStatusTip(value);
                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                            cell->setFont(cellFont);
                                            qobject_cast<QTableWidget *>(dataTable)->setItem(a + 1, col, cell);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    dataTable->setCurrentCell(a + 1, 0);
                }
            }
        }
    }
}

void MainWindow::modifiesInputTable(int b)
{
    QHeaderView *header = qobject_cast<QHeaderView *>(QObject::sender());
    QTableWidget *dataTable = qobject_cast<QTableWidget *>(header->parentWidget());
    if (b == 0)
    {
        dataTable->insertRow(0);

        // we first read the variables
        QFile varFile("bin/dictionary/variables");
        int Id, countLine(-1);
        QMap<QString, int> reproVar;
        QMap<int, QStringList> varMap;
        QFile * tempFile;
        QMap<QString, QMap<QString, int> > factorPosition;
        QMap<QString, QMap<int, QStringList> > factorMap;
        QMap<QString, int> tempPosition;
        QMap<int, QStringList> tempMap;
        QString line;
        QStringList dataRow;
        int tempId;
        if (!varFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/variables.");
            urgentStop();
            return;
        }else{
            QTextStream varStream(&varFile);
            line = varStream.readLine();
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/variables\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!varStream.atEnd()  && line != "###")
                {
                    line = varStream.readLine();
                    reproVar.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
                while(!varStream.atEnd())
                {
                    dataRow = varStream.readLine().split("\t");
                    Id = dataRow.at(0).toInt();
                    dataRow.removeFirst();
                    varMap.insert(Id, dataRow);
                    if (varMap[Id].size() > reproVar["TypeVar"])
                    {
                        if (varMap[Id].at(reproVar["TypeVar"]).startsWith("eKEY"))
                        {
                            // factor variable
                            if (dataRow.at(reproVar["TypeVar"]).split(":").size() > 1)
                            {
                                tempFile = new QFile(QString("bin/%1").arg(dataRow.at(reproVar["TypeVar"]).split(":").at(1)));
                                countLine = -1;
                                tempPosition.clear();
                                tempMap.clear();
                                if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                    urgentStop();
                                    return;
                                }else{
                                    QTextStream tempStream(tempFile);
                                    line = tempStream.readLine();
                                    if (line != "###")
                                    {
                                        // Error
                                        QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                        urgentStop();
                                        return;
                                    }else{
                                        line = "";
                                        while (!tempStream.atEnd()  && line != "###")
                                        {
                                            line = tempStream.readLine();
                                            tempPosition.insert(line.split("\t").at(0), countLine);
                                            countLine++;
                                        }
                                        while(!tempStream.atEnd())
                                        {
                                            dataRow = tempStream.readLine().split("\t");
                                            tempId = dataRow.at(0).toInt();
                                            dataRow.removeFirst();
                                            tempMap.insert(tempId, dataRow);
                                        }
                                    }
                                }
                                tempFile->close();
                                if (varMap[Id].size() > reproVar["Name"])
                                {
                                    factorPosition.insert(varMap[Id].at(reproVar["Name"]), tempPosition);
                                    factorMap.insert(varMap[Id].at(reproVar["Name"]), tempMap);
                                }
                            }
                        }
                    }
                }
            }
            // then reproduction variables
            QFile reproVarFile("bin/dictionary/reproductionVariables");
            countLine = -1;
            QMap<QString, int> reproreproVar;
            QMap<int, QStringList> reproVarMap;
            if (!reproVarFile.open(QIODevice::ReadOnly | QIODevice::Text))
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/reproductionVariables.");
                urgentStop();
                return;
            }else{
                QTextStream reproVarStream(&reproVarFile);
                line = reproVarStream.readLine();
                if (line != "###")
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/reproductionVariables\nMauvaise entête (\"###\" attendu)");
                    urgentStop();
                    return;
                }else{
                    line = "";
                    while (!reproVarStream.atEnd()  && line != "###")
                    {
                        line = reproVarStream.readLine();
                        reproreproVar.insert(line.split("\t").at(0), countLine);
                        countLine++;
                    }
                    while(!reproVarStream.atEnd())
                    {
                        dataRow = reproVarStream.readLine().split("\t");
                        Id = dataRow.at(0).toInt();
                        dataRow.removeFirst();
                        reproVarMap.insert(Id, dataRow);
                        if (reproVarMap[Id].size() > reproreproVar["TypeVar"])
                        {
                            if (reproVarMap[Id].at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                            {
                                // factor variable
                                if (dataRow.at(reproreproVar["TypeVar"]).split(":").size() > 1)
                                {
                                    tempFile = new QFile(QString("bin/%1").arg(dataRow.at(reproreproVar["TypeVar"]).split(":").at(1)));
                                    countLine = -1;
                                    tempPosition.clear();
                                    tempMap.clear();
                                    if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                                    {
                                        // Error
                                        QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                        urgentStop();
                                        return;
                                    }else{
                                        QTextStream tempStream(tempFile);
                                        line = tempStream.readLine();
                                        if (line != "###")
                                        {
                                            // Error
                                            QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                            urgentStop();
                                            return;
                                        }else{
                                            line = "";
                                            while (!tempStream.atEnd()  && line != "###")
                                            {
                                                line = tempStream.readLine();
                                                tempPosition.insert(line.split("\t").at(0), countLine);
                                                countLine++;
                                            }
                                            while(!tempStream.atEnd())
                                            {
                                                dataRow = tempStream.readLine().split("\t");
                                                tempId = dataRow.at(0).toInt();
                                                dataRow.removeFirst();
                                                tempMap.insert(tempId, dataRow);
                                            }
                                        }
                                    }
                                    tempFile->close();
                                    if (reproVarMap[Id].size() > reproreproVar["Name"])
                                    {
                                        factorPosition.insert(reproVarMap[Id].at(reproreproVar["Name"]), tempPosition);
                                        factorMap.insert(reproVarMap[Id].at(reproreproVar["Name"]), tempMap);
                                    }
                                }
                            }
                        }
                    }
                    // we can now fill the new line
                    QTableWidgetItem *cell;
                    cell = new QTableWidgetItem;
                    int col(0);
                    cell->setData(Qt::UserRole, QVariant("New GU"));
                    // GU Name
                    QString Year;
                    QFont cellFont;
                    int NGU(0);
                    if (dataTable->rowCount() == 1)
                    {
                        Year = QString::number(QInputDialog::getInt(NULL, "Année de l'UC", "Merci de m'indiquer l'année de l'UC", QDate::currentDate().year()));
                        if (Year == "0") Year = "----";
                        NGU = 1;
                    }else{
                        Year = dataTable->item(1, 0)->text().split(" . ").at(0);
                        if (dataTable->item(1, 0)->text().split(" . ").size() > 1) NGU = dataTable->item(1, 0)->text().split(" . ").at(1).toInt() + 1;
                        if (Year == "----")
                        {
                            for (int i(1); i < dataTable->rowCount(); i++)
                            {
                                dataTable->item(i, 0)->setText(QString("---- . -%1").arg(i + 1));
                                cellFont = dataTable->item(i, 0)->font();
                                cellFont.setBold(true);
                                dataTable->item(i, 0)->setFont(cellFont);
                            }
                            NGU = -1;
                        }
                    }
                    cell->setText(QString("%1 . %2").arg(Year).arg(NGU));
                    cellFont = cell->font();
                    cellFont.setItalic(true);
                    cell->setFont(cellFont);
                    cell->setFlags(cell->flags() & ~ Qt::ItemIsEditable);
                    qobject_cast<QTableWidget *>(dataTable)->setItem(0, col, cell);
                    // first the measure
                    QString value, historic;
                    QMap<QString, QVariant> datas;
                    QSpinBox *intEdit;
                    QDoubleSpinBox *doubleEdit;
                    QLineEdit *textEdit;
                    QComboBox *factorEdit;
                    QMap<QString, QString> factorCodes;
                    int index;
                    for (QMap<int, QStringList>::iterator var(varMap.begin()); var != varMap.end(); var++)
                    {
                        if (var->size() > reproVar["AppearingVar"])
                        {
                            if (var->at(reproVar["AppearingVar"]) == "Y")
                            {
                                col++;
                                value = "";
                                datas = *(new QMap<QString, QVariant>);
                                historic = "%1";
                                if (var->size() > reproVar["TypeVar"])
                                {
                                    if (var->at(reproVar["TypeVar"]) == "INTEGER")
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        intEdit = new QSpinBox;
                                        intEdit->setMaximum(std::numeric_limits<int>::max());
                                        intEdit->setMinimum(std::numeric_limits<int>::min());
                                        intEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproVar["UnitVar"])
                                        {
                                            if (var->at(reproVar["UnitVar"]) != "")
                                            {
                                                intEdit->setSuffix(QString(" %1").arg(var->at(reproVar["UnitVar"])));
                                            }
                                        }
                                        if (var->size() > reproVar["IncreasingVar"] && var->size() > reproVar["DecreasingVar"])
                                        {
                                            if (var->at(reproVar["IncreasingVar"]) == "Y")
                                            {
                                                intEdit->setProperty("i", QVariant("In"));
                                            }else if (var->at(reproVar["DecreasingVar"]) == "Y")
                                            {
                                                intEdit->setProperty("i", QVariant("De"));
                                            }else{
                                                intEdit->setProperty("i", QVariant("No"));
                                            }
                                        }
                                        if (var->size() > reproVar["MinValueVar"])
                                        {
                                            if (var->at(reproVar["MinValueVar"]) != "")
                                            {
                                                intEdit->setMinimum(var->at(reproVar["MinValueVar"]).toInt());
                                            }
                                        }
                                        if (var->size() > reproVar["MaxValueVar"])
                                        {
                                            if (var->at(reproVar["MaxValueVar"]) != "")
                                            {
                                                intEdit->setMaximum(var->at(reproVar["MaxValueVar"]).toInt());
                                            }
                                        }
                                        if (var->size() > reproVar["ExplainVar"])
                                        {
                                            intEdit->setProperty("e", QVariant(var->at(reproVar["ExplainVar"])));
                                            intEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        SelectFocus *test = new SelectFocus;
                                        intEdit->installEventFilter(test);
                                        QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                        intEdit->setFont(cellFont);
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(0, col, intEdit);
                                    }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        doubleEdit = new QDoubleSpinBox;
                                        doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                        doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                        doubleEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproVar["UnitVar"])
                                        {
                                            if (var->at(reproVar["UnitVar"]) != "")
                                            {
                                                doubleEdit->setSuffix(QString(" %1").arg(var->at(reproVar["UnitVar"])));
                                            }
                                        }
                                        if (var->size() > reproVar["IncreasingVar"] && var->size() > reproVar["DecreasingVar"])
                                        {
                                            if (var->at(reproVar["IncreasingVar"]) == "Y")
                                            {
                                                doubleEdit->setProperty("i", QVariant("In"));
                                            }else if (var->at(reproVar["DecreasingVar"]) == "Y")
                                            {
                                                doubleEdit->setProperty("i", QVariant("De"));
                                            }else{
                                                doubleEdit->setProperty("i", QVariant("No"));
                                            }
                                        }
                                        if (var->size() > reproVar["MinValueVar"])
                                        {
                                            if (var->at(reproVar["MinValueVar"]) != "")
                                            {
                                                doubleEdit->setMinimum(var->at(reproVar["MinValueVar"]).toDouble());
                                            }
                                        }
                                        if (var->size() > reproVar["MaxValueVar"])
                                        {
                                            if (var->at(reproVar["MaxValueVar"]) != "")
                                            {
                                                doubleEdit->setMaximum(var->at(reproVar["MaxValueVar"]).toDouble());
                                            }
                                        }
                                        if (var->size() > reproVar["ExplainVar"])
                                        {
                                            doubleEdit->setProperty("e", QVariant(var->at(reproVar["ExplainVar"])));
                                            doubleEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        SelectFocus *test = new SelectFocus;
                                        doubleEdit->installEventFilter(test);
                                        doubleEdit->setFont(cellFont);
                                        QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(0, col, doubleEdit);
                                    }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        textEdit = new QLineEdit;
                                        textEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproVar["ExplainVar"])
                                        {
                                            textEdit->setProperty("e", QVariant(var->at(reproVar["ExplainVar"])));
                                            textEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        SelectFocus *test = new SelectFocus;
                                        textEdit->installEventFilter(test);
                                        textEdit->setFont(cellFont);
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(0, col, textEdit);
                                    }else if (var->at(reproVar["TypeVar"]).startsWith("eKEY"))
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        factorEdit = new QComboBox;
                                        factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                        factorEdit->setEditable(true);
                                        index = 0;
                                        factorCodes.clear();
                                        if (var->size() > reproVar["Name"])
                                        {
                                            for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproVar["Name"])].begin()); it != factorMap[var->at(reproVar["Name"])].end(); it++)
                                            {
                                                //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproVar["Name"])]["Code"]));
                                                if (it->size() > factorPosition[var->at(reproVar["Name"])]["Code"])
                                                {
                                                    factorEdit->addItem(it->at(factorPosition[var->at(reproVar["Name"])]["Code"]), QVariant(it.key()));
                                                    index++;
                                                    if (it->size() > factorPosition[var->at(reproVar["Name"])]["Explain"]) factorCodes.insert(it->at(factorPosition[var->at(reproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproVar["Name"])]["Explain"]));
                                                }
                                            }
                                        }
                                        factorEdit->setCurrentIndex(-1);
                                        factorEdit->model()->sort(0);
                                        factorEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproVar["ExplainVar"])
                                        {
                                            factorEdit->setProperty("e", QVariant(var->at(reproVar["ExplainVar"])));
                                            historic = historic.arg(var->at(reproVar["ExplainVar"]) + "%1");
                                        }
                                        for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                        {
                                            historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                        }
                                        factorEdit->setToolTip(historic.arg("").replace("#", "Male"));
                                        factorEdit->setFont(cellFont);
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                        QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(0, col, factorEdit);
                                    }else{
                                        cell = new QTableWidgetItem;
                                        cell->setData(Qt::UserRole, QVariant(datas));
                                        cell->setStatusTip(value);
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        cell->setFont(cellFont);
                                        qobject_cast<QTableWidget *>(dataTable)->setItem(0, col, cell);
                                    }
                                }
                            }
                        }
                    }
                    // then reproduction
                    datas = *(new QMap<QString, QVariant>);
                    for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                    {
                        if (var->size() > reproreproVar["Target"] && var->size() > reproreproVar["AppearingVar"])
                        {
                            if (((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Male")) && (var->at(reproreproVar["AppearingVar"]) == "Y"))
                            {
                                col++;
                                value = "";
                                datas = *(new QMap<QString, QVariant>);
                                historic = "";
                                historic = "%1" + historic;
                                if (var->size() > reproreproVar["TypeVar"])
                                {
                                    if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        intEdit = new QSpinBox;
                                        intEdit->setMaximum(std::numeric_limits<int>::max());
                                        intEdit->setMinimum(std::numeric_limits<int>::min());
                                        intEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproreproVar["UnitVar"])
                                        {
                                            if (var->at(reproreproVar["UnitVar"]) != "")
                                            {
                                                intEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                            }
                                        }
                                        if (var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"])
                                        {
                                            if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                            {
                                                intEdit->setProperty("i", QVariant("In"));
                                            }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                            {
                                                intEdit->setProperty("i", QVariant("De"));
                                            }else{
                                                intEdit->setProperty("i", QVariant("No"));
                                            }
                                        }
                                        if (var->size() > reproreproVar["MinValueVar"])
                                        {
                                            if (var->at(reproreproVar["MinValueVar"]) != "")
                                            {
                                                intEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toInt());
                                            }
                                        }
                                        if (var->size() > reproreproVar["MaxValueVar"])
                                        {
                                            if (var->at(reproreproVar["MaxValueVar"]) != "")
                                            {
                                                intEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toInt());
                                            }
                                        }
                                        if (var->size() > reproreproVar["ExplainVar"])
                                        {
                                            intEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                            intEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        SelectFocus *test = new SelectFocus;
                                        intEdit->installEventFilter(test);
                                        intEdit->setFont(cellFont);
                                        QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(0, col, intEdit);
                                    }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        doubleEdit = new QDoubleSpinBox;
                                        doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                        doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                        doubleEdit->setProperty("d", QVariant(datas));
                                        //if (value != "") doubleEdit->setValue(value.toDouble());
                                        if (var->size() > reproreproVar["UnitVar"])
                                        {
                                            if (var->at(reproreproVar["UnitVar"]) != "")
                                            {
                                                doubleEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                            }
                                        }
                                        if (var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"])
                                        {
                                            if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                            {
                                                doubleEdit->setProperty("i", QVariant("In"));
                                            }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                            {
                                                doubleEdit->setProperty("i", QVariant("De"));
                                            }else{
                                                doubleEdit->setProperty("i", QVariant("No"));
                                            }
                                        }
                                        if (var->size() > reproreproVar["MinValueVar"])
                                        {
                                            if (var->at(reproreproVar["MinValueVar"]) != "")
                                            {
                                                doubleEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toDouble());
                                            }
                                        }
                                        if (var->size() > reproreproVar["MaxValueVar"])
                                        {
                                            if (var->at(reproreproVar["MaxValueVar"]) != "")
                                            {
                                                doubleEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toDouble());
                                            }
                                        }
                                        if (var->size() > reproreproVar["ExplainVar"])
                                        {
                                            doubleEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                            doubleEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        SelectFocus *test = new SelectFocus;
                                        doubleEdit->installEventFilter(test);
                                        doubleEdit->setFont(cellFont);
                                        QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(0, col, doubleEdit);
                                    }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                    {
                                        //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                        textEdit = new QLineEdit;
                                        textEdit->setProperty("d", QVariant(datas));
                                        //if (value != "") textEdit->setPlaceholderText(value);
                                        if (var->size() > reproreproVar["ExplainVar"])
                                        {
                                            textEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                            textEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Male"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        SelectFocus *test = new SelectFocus;
                                        textEdit->installEventFilter(test);
                                        textEdit->setFont(cellFont);
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(0, col, textEdit);
                                    }else if (var->at(reproVar["TypeVar"]).startsWith("eKEY"))
                                    {
                                        //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                        factorEdit = new QComboBox;
                                        factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                        factorEdit->setEditable(true);
                                        index = 0;
                                        factorCodes.clear();
                                        if (var->size() > reproreproVar["Name"])
                                        {
                                            for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproreproVar["Name"])].begin()); it != factorMap[var->at(reproreproVar["Name"])].end(); it++)
                                            {
                                                //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]));
                                                if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["IdSex"])
                                                {
                                                    if (it->at(factorPosition[var->at(reproreproVar["Name"])]["IdSex"]) == "1" && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Code"])
                                                    {
                                                        factorEdit->addItem(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), QVariant(it.key()));
                                                        //if (value != "" && value == it->at(0)) factorEdit->setCurrentIndex(index);
                                                        index++;
                                                        if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["Explain"]) factorCodes.insert(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproreproVar["Name"])]["Explain"]));
                                                    }
                                                }
                                            }
                                        }
                                        factorEdit->setCurrentIndex(-1);
                                        factorEdit->model()->sort(0);
                                        factorEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproreproVar["ExplainVar"])
                                        {
                                            factorEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                            historic = historic.arg(var->at(reproreproVar["ExplainVar"]) + "%1");
                                        }
                                        for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                        {
                                            historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                        }
                                        factorEdit->setToolTip(historic.arg("").replace("#", "Male"));
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        factorEdit->setFont(cellFont);
                                        QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                        QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                        qobject_cast<QTableWidget *>(dataTable)->setCellWidget(0, col, factorEdit);
                                    }else{
                                        cell = new QTableWidgetItem;
                                        cell->setData(Qt::UserRole, QVariant(datas));
                                        cell->setStatusTip(value);
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        cell->setFont(cellFont);
                                        qobject_cast<QTableWidget *>(dataTable)->setItem(0, col, cell);
                                    }
                                }
                            }
                        }
                    }
                    while (col <= (dataTable->columnCount() - 1))
                    {
                        for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                        {
                            if (var->size() > reproreproVar["Target"] && var->size() > reproreproVar["AppearingVar"])
                            {
                                if (((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Female")) && (var->at(reproreproVar["AppearingVar"]) == "Y"))
                                {
                                    col++;
                                    value = "";
                                    datas = *(new QMap<QString, QVariant>);
                                    historic = "%1";
                                    if (var->size() > reproreproVar["TypeVar"])
                                    {
                                        if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                        {
                                            //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                            intEdit = new QSpinBox;
                                            intEdit->setMaximum(std::numeric_limits<int>::max());
                                            intEdit->setMinimum(std::numeric_limits<int>::min());
                                            intEdit->setProperty("d", QVariant(datas));
                                            if (var->size() > reproreproVar["UnitVar"])
                                            {
                                                if (var->at(reproreproVar["UnitVar"]) != "")
                                                {
                                                    intEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                }
                                            }
                                            if (var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"])
                                            {
                                                if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                {
                                                    intEdit->setProperty("i", QVariant("In"));
                                                }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                {
                                                    intEdit->setProperty("i", QVariant("De"));
                                                }else{
                                                    intEdit->setProperty("i", QVariant("No"));
                                                }
                                            }
                                            if (var->size() > reproreproVar["MinValueVar"])
                                            {
                                                if (var->at(reproreproVar["MinValueVar"]) != "")
                                                {
                                                    intEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toInt());
                                                }
                                            }
                                            if (var->size() > reproreproVar["MaxValueVar"])
                                            {
                                                if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                {
                                                    intEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toInt());
                                                }
                                            }
                                            if (var->size() > reproreproVar["ExplainVar"])
                                            {
                                                intEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                intEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                            }
                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                            SelectFocus *test = new SelectFocus;
                                            intEdit->installEventFilter(test);
                                            intEdit->setFont(cellFont);
                                            QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                            qobject_cast<QTableWidget *>(dataTable)->setCellWidget(0, col, intEdit);
                                        }else if (var->at(reproVar["TypeVar"]) == "DOUBLE")
                                        {
                                            //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                            doubleEdit = new QDoubleSpinBox;
                                            doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                            doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                            doubleEdit->setProperty("d", QVariant(datas));
                                            if (var->size() > reproreproVar["UnitVar"])
                                            {
                                                if (var->at(reproreproVar["UnitVar"]) != "")
                                                {
                                                    doubleEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                                }
                                            }
                                            if (var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"])
                                            {
                                                if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                                {
                                                    doubleEdit->setProperty("i", QVariant("In"));
                                                }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                                {
                                                    doubleEdit->setProperty("i", QVariant("De"));
                                                }else{
                                                    doubleEdit->setProperty("i", QVariant("No"));
                                                }
                                            }
                                            if (var->size() > reproreproVar["MinValueVar"])
                                            {
                                                if (var->at(reproreproVar["MinValueVar"]) != "")
                                                {
                                                    doubleEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toDouble());
                                                }
                                            }
                                            if (var->size() > reproreproVar["MaxValueVar"])
                                            {
                                                if (var->at(reproreproVar["MaxValueVar"]) != "")
                                                {
                                                    doubleEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toDouble());
                                                }
                                            }
                                            if (var->size() > reproreproVar["ExplainVar"])
                                            {
                                                doubleEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                doubleEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                            }
                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                            SelectFocus *test = new SelectFocus;
                                            doubleEdit->installEventFilter(test);
                                            doubleEdit->setFont(cellFont);
                                            QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                            qobject_cast<QTableWidget *>(dataTable)->setCellWidget(0, col, doubleEdit);
                                        }else if (var->at(reproVar["TypeVar"]) == "STRING")
                                        {
                                            //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                            textEdit = new QLineEdit;
                                            textEdit->setProperty("d", QVariant(datas));
                                            if (var->size() > reproreproVar["ExplainVar"])
                                            {
                                                textEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                textEdit->setToolTip(historic.arg(var->at(reproVar["ExplainVar"])).replace("#", "Femelle"));
                                            }
                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                            //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                            SelectFocus *test = new SelectFocus;
                                            textEdit->installEventFilter(test);
                                            textEdit->setFont(cellFont);
                                            qobject_cast<QTableWidget *>(dataTable)->setCellWidget(0, col, textEdit);
                                        }else if (var->at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                                        {
                                            //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                            factorEdit = new QComboBox;
                                            factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                            factorEdit->setEditable(true);
                                            index = 0;
                                            factorCodes.clear();
                                            if (var->size() > reproreproVar["Name"])
                                            {
                                                for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproreproVar["Name"])].begin()); it != factorMap[var->at(reproreproVar["Name"])].end(); it++)
                                                {
                                                    //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]));
                                                    if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["IdSex"])
                                                    {
                                                        if (it->at(factorPosition[var->at(reproreproVar["Name"])]["IdSex"]) == "2" && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Code"])
                                                        {
                                                            factorEdit->addItem(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), QVariant(it.key()));
                                                            index++;
                                                            if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["Explain"]) factorCodes.insert(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproreproVar["Name"])]["Explain"]));
                                                        }
                                                    }
                                                }
                                            }
                                            factorEdit->setCurrentIndex(-1);
                                            factorEdit->model()->sort(0);
                                            factorEdit->setProperty("d", QVariant(datas));
                                            if (var->size() > reproreproVar["ExplainVar"])
                                            {
                                                factorEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                                historic = historic.arg(var->at(reproreproVar["ExplainVar"]) + "%1");
                                            }
                                            for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                            {
                                                historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                            }
                                            factorEdit->setToolTip(historic.arg("").replace("#", "Femelle"));
                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                            //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                            factorEdit->setFont(cellFont);
                                            QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                            QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                            qobject_cast<QTableWidget *>(dataTable)->setCellWidget(0, col, factorEdit);
                                        }else{
                                            cell = new QTableWidgetItem;
                                            cell->setData(Qt::UserRole, QVariant(datas));
                                            cell->setStatusTip(value);
                                            //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                            //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                            //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                            cell->setFont(cellFont);
                                            qobject_cast<QTableWidget *>(dataTable)->setItem(0, col, cell);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    dataTable->setCurrentCell(0, 0);
                }
            }
        }
        //QMessageBox::information(NULL, "", "UC ajoutée");
    }else if (b == (dataTable->columnCount() - 1))
    {
        // we first read the variables
        QFile varFile("bin/dictionary/variables");
        int Id, countLine(-1);
        QMap<QString, int> reproVar;
        QMap<int, QStringList> varMap;
        QFile * tempFile;
        QMap<QString, QMap<QString, int> > factorPosition;
        QMap<QString, QMap<int, QStringList> > factorMap;
        QMap<QString, int> tempPosition;
        QMap<int, QStringList> tempMap;
        QString line;
        QStringList dataRow;
        int tempId;
        // then reproduction variables
        QFile reproVarFile("bin/dictionary/reproductionVariables");
        countLine = -1;
        QMap<QString, int> reproreproVar;
        QMap<int, QStringList> reproVarMap;
        if (!reproVarFile.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            // Error
            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/dictionary/reproductionVariables.");
            urgentStop();
            return;
        }else{
            QTextStream reproVarStream(&reproVarFile);
            line = reproVarStream.readLine();
            if (line != "###")
            {
                // Error
                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/dictionary/reproductionVariables\nMauvaise entête (\"###\" attendu)");
                urgentStop();
                return;
            }else{
                line = "";
                while (!reproVarStream.atEnd()  && line != "###")
                {
                    line = reproVarStream.readLine();
                    reproreproVar.insert(line.split("\t").at(0), countLine);
                    countLine++;
                }
                while(!reproVarStream.atEnd())
                {
                    dataRow = reproVarStream.readLine().split("\t");
                    Id = dataRow.at(0).toInt();
                    dataRow.removeFirst();
                    reproVarMap.insert(Id, dataRow);
                    if (reproVarMap[Id].size() > reproreproVar["TypeVar"])
                    {
                        if (reproVarMap[Id].at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                        {
                            // factor variable
                            if (dataRow.at(reproreproVar["TypeVar"]).split(":").size() > 1)
                            {
                                tempFile = new QFile(QString("bin/%1").arg(dataRow.at(reproreproVar["TypeVar"]).split(":").at(1)));
                                countLine = -1;
                                tempPosition.clear();
                                tempMap.clear();
                                if (!tempFile->open(QIODevice::ReadOnly | QIODevice::Text))
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", QString("Le programme n'a pas réussi à lire le fichier de données : %1.").arg(tempFile->fileName()));
                                    urgentStop();
                                    return;
                                }else{
                                    QTextStream tempStream(tempFile);
                                    line = tempStream.readLine();
                                    if (line != "###")
                                    {
                                        // Error
                                        QMessageBox::critical(NULL, "Erreur", QString("Une erreur est survenue à la lecture de la première ligne du fichier : %1\nMauvaise entête (\"###\" attendu)").arg(tempFile->fileName()));
                                        urgentStop();
                                        return;
                                    }else{
                                        line = "";
                                        while (!tempStream.atEnd()  && line != "###")
                                        {
                                            line = tempStream.readLine();
                                            tempPosition.insert(line.split("\t").at(0), countLine);
                                            countLine++;
                                        }
                                        while(!tempStream.atEnd())
                                        {
                                            dataRow = tempStream.readLine().split("\t");
                                            tempId = dataRow.at(0).toInt();
                                            dataRow.removeFirst();
                                            tempMap.insert(tempId, dataRow);
                                        }
                                    }
                                }
                                tempFile->close();
                                if (reproVarMap[Id].size() > reproreproVar["Name"])
                                {
                                    factorPosition.insert(reproVarMap[Id].at(reproreproVar["Name"]), tempPosition);
                                    factorMap.insert(reproVarMap[Id].at(reproreproVar["Name"]), tempMap);
                                }
                            }
                        }
                    }
                }
                // we can now fill the new lines
                QTableWidgetItem *cell;
                int col(b);
                QFont cellFont;
                QString value, historic;
                QMap<QString, QVariant> datas;
                QSpinBox *intEdit;
                QDoubleSpinBox *doubleEdit;
                QLineEdit *textEdit;
                QComboBox *factorEdit;
                QMap<QString, QString> factorCodes;
                int index;
                int nFem;
                nFem = dataTable->horizontalHeaderItem(b)->text().lastIndexOf(QRegExp("[0-9]+$"));
                nFem = dataTable->horizontalHeaderItem(b)->text().size() - nFem;
                nFem = dataTable->horizontalHeaderItem(b)->text().right(nFem).toInt() + 1;
                for (QMap<int, QStringList>::iterator var(reproVarMap.begin()); var != reproVarMap.end(); var++)
                {
                    if (var->size() > reproreproVar["Target"] && var->size() > reproreproVar["appearingVar"])
                    {
                        if (((var->at(reproreproVar["Target"]) == "Both") || (var->at(reproreproVar["Target"]) == "Female")) && (var->at(reproreproVar["AppearingVar"]) == "Y"))
                        {
                            cell = new QTableWidgetItem;
                            value = var->at(reproreproVar["MediumName"]);
                            value.replace("#", "Fem");
                            value = QString("%1%2").arg(value).arg(nFem);
                            cell->setText(value);
                            cellFont = cell->font();
                            cellFont.setItalic(true);
                            cell->setFont(cellFont);
                            col++;
                            dataTable->insertColumn(col);
                            dataTable->setHorizontalHeaderItem(col, cell);
                            value = "";
                            datas = *(new QMap<QString, QVariant>);
                            historic = "%1";
                            if (var->size() > reproreproVar["TypeVar"])
                            {
                                if (var->at(reproreproVar["TypeVar"]) == "INTEGER")
                                {
                                    for (int r(0); r < dataTable->rowCount(); r ++)
                                    {
                                        intEdit = new QSpinBox;
                                        intEdit->setMaximum(std::numeric_limits<int>::max());
                                        intEdit->setMinimum(std::numeric_limits<int>::min());
                                        intEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproreproVar["UnitVar"])
                                        {
                                            if (var->at(reproreproVar["UnitVar"]) != "")
                                            {
                                                intEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                            }
                                        }
                                        if (var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"])
                                        {
                                            if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                            {
                                                intEdit->setProperty("i", QVariant("In"));
                                            }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                            {
                                                intEdit->setProperty("i", QVariant("De"));
                                            }else{
                                                intEdit->setProperty("i", QVariant("No"));
                                            }
                                        }
                                        if (var->size() > reproreproVar["MinValueVar"])
                                        {
                                            if (var->at(reproreproVar["MinValueVar"]) != "")
                                            {
                                                intEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toInt());
                                            }
                                        }
                                        if (var->size() > reproreproVar["MaxValueVar"])
                                        {
                                            if (var->at(reproreproVar["MaxValueVar"]) != "")
                                            {
                                                intEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toInt());
                                            }
                                        }
                                        if (var->size() > reproreproVar["ExplainVar"])
                                        {
                                            intEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                            intEdit->setToolTip(historic.arg(var->at(reproreproVar["ExplainVar"])).replace("#", "Femelle"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        SelectFocus *test = new SelectFocus;
                                        intEdit->installEventFilter(test);
                                        intEdit->setFont(cellFont);
                                        QObject::connect(intEdit, SIGNAL(valueChanged(int)), this, SLOT(checkValue(int)));
                                        dataTable->setCellWidget(r, col, intEdit);
                                    }
                                }else if (var->at(reproreproVar["TypeVar"]) == "DOUBLE")
                                {
                                    for (int r(0); r < dataTable->rowCount(); r ++)
                                    {
                                        //if (var->at(reproVar["RangeableVar"]) == "Y") cas à traiter
                                        doubleEdit = new QDoubleSpinBox;
                                        doubleEdit->setMinimum(std::numeric_limits<double>::min());
                                        doubleEdit->setMaximum(std::numeric_limits<double>::max());
                                        doubleEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproreproVar["UnitVar"])
                                        {
                                            if (var->at(reproreproVar["UnitVar"]) != "")
                                            {
                                                doubleEdit->setSuffix(QString(" %1").arg(var->at(reproreproVar["UnitVar"])));
                                            }
                                        }
                                        if (var->size() > reproreproVar["IncreasingVar"] && var->size() > reproreproVar["DecreasingVar"])
                                        {
                                            if (var->at(reproreproVar["IncreasingVar"]) == "Y")
                                            {
                                                doubleEdit->setProperty("i", QVariant("In"));
                                            }else if (var->at(reproreproVar["DecreasingVar"]) == "Y")
                                            {
                                                doubleEdit->setProperty("i", QVariant("De"));
                                            }else{
                                                doubleEdit->setProperty("i", QVariant("No"));
                                            }
                                        }
                                        if (var->size() > reproreproVar["MinValueVar"])
                                        {
                                            if (var->at(reproreproVar["MinValueVar"]) != "")
                                            {
                                                doubleEdit->setMinimum(var->at(reproreproVar["MinValueVar"]).toDouble());
                                            }
                                        }
                                        if (var->size() > reproreproVar["MaxValueVar"])
                                        {
                                            if (var->at(reproreproVar["MaxValueVar"]) != "")
                                            {
                                                doubleEdit->setMaximum(var->at(reproreproVar["MaxValueVar"]).toDouble());
                                            }
                                        }
                                        if (var->size() > reproreproVar["ExplainVar"])
                                        {
                                            doubleEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                            doubleEdit->setToolTip(historic.arg(var->at(reproreproVar["ExplainVar"])).replace("#", "Femelle"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        SelectFocus *test = new SelectFocus;
                                        doubleEdit->installEventFilter(test);
                                        doubleEdit->setFont(cellFont);
                                        QObject::connect(doubleEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        dataTable->setCellWidget(r, col, doubleEdit);
                                    }
                                }else if (var->at(reproreproVar["TypeVar"]) == "STRING")
                                {
                                    for (int r(0); r < dataTable->rowCount(); r ++)
                                    {
                                        //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                        textEdit = new QLineEdit;
                                        textEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproreproVar["ExplainVar"])
                                        {
                                            textEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                            textEdit->setToolTip(historic.arg(var->at(reproreproVar["ExplainVar"])).replace("#", "Femelle"));
                                        }
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        //QObject::connect(textEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        SelectFocus *test = new SelectFocus;
                                        textEdit->installEventFilter(test);
                                        textEdit->setFont(cellFont);
                                        dataTable->setCellWidget(r, col, textEdit);
                                    }
                                }else if (var->at(reproreproVar["TypeVar"]).startsWith("eKEY"))
                                {
                                    for (int r(0); r < dataTable->rowCount(); r ++)
                                    {
                                        //if (var->at(reproreproVar["RangeableVar"]) == "Y") cas à traiter
                                        factorEdit = new QComboBox;
                                        factorEdit->setInsertPolicy(QComboBox::InsertAtTop);
                                        factorEdit->setEditable(true);
                                        index = 0;
                                        factorCodes.clear();
                                        if (var->size() > reproreproVar["Name"])
                                        {
                                            for (QMap<int, QStringList>::iterator it(factorMap[var->at(reproreproVar["Name"])].begin()); it != factorMap[var->at(reproreproVar["Name"])].end(); it++)
                                            {
                                                //QMessageBox::information(NULL, it->at(0), it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]));
                                                if (it->size() > factorPosition[var->at(reproreproVar["Name"])]["IdSex"])
                                                {
                                                    if (it->at(factorPosition[var->at(reproreproVar["Name"])]["IdSex"]) == "2" && it->size() > factorPosition[var->at(reproreproVar["Name"])]["Code"])
                                                    {
                                                        factorEdit->addItem(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), QVariant(it.key()));
                                                        index++;
                                                        if (factorPosition[var->at(reproreproVar["Name"])]["Explain"]) factorCodes.insert(it->at(factorPosition[var->at(reproreproVar["Name"])]["Code"]), it->at(factorPosition[var->at(reproreproVar["Name"])]["Explain"]));
                                                    }
                                                }
                                            }
                                        }
                                        factorEdit->setCurrentIndex(-1);
                                        factorEdit->model()->sort(0);
                                        factorEdit->setProperty("d", QVariant(datas));
                                        if (var->size() > reproreproVar["ExplainVar"])
                                        {
                                            factorEdit->setProperty("e", QVariant(var->at(reproreproVar["ExplainVar"])));
                                            historic = historic.arg(var->at(reproreproVar["ExplainVar"]) + "%1");
                                        }
                                        for (QMap<QString, QString>::iterator it(factorCodes.begin()); it != factorCodes.end(); it++)
                                        {
                                            historic = historic.arg("\n" + it.key() + "\t" + it.value() + "%1");
                                        }
                                        factorEdit->setToolTip(historic.arg("").replace("#", "Femelle"));
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        //QObject::connect(factorEdit, SIGNAL(valueChanged(double)), this, SLOT(checkValue(double)));
                                        factorEdit->setFont(cellFont);
                                        QObject::connect(factorEdit, SIGNAL(activated(QString)), this, SLOT(checkValue(QString)));
                                        QObject::connect(factorEdit, SIGNAL(editTextChanged(QString)), this, SLOT(checkValue(QString)));
                                        dataTable->setCellWidget(r, col, factorEdit);
                                    }
                                }else{
                                    for (int r(0); r < dataTable->rowCount(); r ++)
                                    {
                                        cell = new QTableWidgetItem;
                                        cell->setData(Qt::UserRole, QVariant(datas));
                                        cell->setStatusTip(value);
                                        //cell->setFlags(Qt::ItemIsSelectable & Qt::ItemIsEditable);
                                        //cell->setFlags(cell->flags() & Qt::ItemIsEditable);
                                        //qobject_cast<QTableWidget *>(dataTab)->insertColumn(col);
                                        cell->setFont(cellFont);
                                        dataTable->setItem(r, col, cell);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }else{
        /*int nFem;
        nFem = dataTable->horizontalHeaderItem(b)->text().lastIndexOf(QRegExp("[0-9]+$"));
        QMessageBox::information(NULL, "", QString::number(nFem));
        nFem = dataTable->horizontalHeaderItem(b)->text().size() - nFem;
        QMessageBox::information(NULL, "", dataTable->horizontalHeaderItem(b)->text().right(nFem));
        nFem = dataTable->horizontalHeaderItem(b)->text().right(nFem).toInt() + 1;*/
    }
}

void MainWindow::modifiesInputTable()
{
    if (mainLayout->rowCount() == 15)
    {
        QTableWidget *dataTable = qobject_cast<QTableWidget*>(mainLayout->itemAtPosition(11, 0)->widget());
        int row(dataTable->currentRow()), rowStart;
        if ((dataTable->hasFocus()) && (row != -1))
        {
            QString newYear, currYear;
            currYear = dataTable->item(row, 0)->text().split(" . ").at(0);
            newYear = QString::number(QInputDialog::getInt(NULL, "Année de l'UC", "Donne moi l'année à affecter à l'UC", QDate::currentDate().year()));
            int NGU(0);
            QFont cellFont;
            bool test(true);
            if (newYear != currYear)
            {
                if (currYear == "----")
                {
                    rowStart = row - 1;
                    while (test)
                    {
                        if (rowStart < 0)
                        {
                            test = false;
                        }else{
                            if (dataTable->item(rowStart, 0)->text().split(" . ").at(0) != "----")
                            {
                                if (dataTable->item(rowStart, 0)->text().split(" . ").at(0).toInt() <= newYear.toInt())
                                {
                                    rowStart--;
                                }else{
                                    test = false;
                                }
                            }else{
                                rowStart--;
                            }
                        }
                    }
                    for (int r(row); r > rowStart; r--)
                    {
                        NGU++;
                        dataTable->item(r, 0)->setText(QString("%1 . %2").arg(newYear).arg(NGU));
                        cellFont = dataTable->item(r, 0)->font();
                        cellFont.setBold(true);
                        dataTable->item(r, 0)->setFont(cellFont);
                    }
                }else
                {
                    if (newYear.toInt() < currYear.toInt())
                    {
                        rowStart = row + 1;
                        while (test)
                        {
                            if (rowStart == dataTable->rowCount())
                            {
                                test = false;
                            }else{
                                if (dataTable->item(rowStart, 0)->text().split(" . ").at(0) == "----")
                                {
                                    test = false;
                                }else{
                                    if (dataTable->item(rowStart, 0)->text().split(" . ").at(0).toInt() <= newYear.toInt())
                                    {
                                        test = false;
                                    }else{
                                        rowStart++;
                                    }
                                }
                            }
                        }
                        if (rowStart != dataTable->rowCount())
                        {
                            if (dataTable->item(rowStart, 0)->text().split(" . ").at(0) == newYear)
                            {
                                if (dataTable->item(rowStart, 0)->text().split(" . ").size() > 1) NGU = dataTable->item(rowStart, 0)->text().split(" . ").at(1).toInt();
                            }
                        }
                        for (int r(rowStart - 1); r >= row; r--)
                        {
                            NGU++;
                            dataTable->item(r, 0)->setText(QString("%1 . %2").arg(newYear).arg(NGU));
                            cellFont = dataTable->item(r, 0)->font();
                            cellFont.setBold(true);
                            dataTable->item(r, 0)->setFont(cellFont);
                        }
                        test = true;
                    }else{
                        if (row < (dataTable->rowCount() - 1))
                        {
                            if (dataTable->item(row + 1, 0)->text().split(" . ").at(0) == newYear)
                            {
                                if (dataTable->item(row + 1, 0)->text().split(" . ").size() > 1) NGU = dataTable->item(row + 1, 0)->text().split(" . ").at(1).toInt();
                            }
                        }
                        NGU++;
                        dataTable->item(row, 0)->setText(QString("%1 . %2").arg(newYear).arg(NGU));
                        cellFont = dataTable->item(row, 0)->font();
                        cellFont.setBold(true);
                        dataTable->item(row, 0)->setFont(cellFont);
                    }
                    test = true;
                    rowStart = row - 1;
                    while (test)
                    {
                        if (rowStart < 0)
                        {
                            test = false;
                        }else{
                            if (dataTable->item(rowStart, 0)->text().split(" . ").at(0).toInt() > newYear.toInt())
                            {
                                test = false;
                            }else{
                                rowStart--;
                            }
                        }
                    }
                    for (int r(row - 1); r > rowStart; r--)
                    {
                        NGU++;
                        dataTable->item(r, 0)->setText(QString("%1 . %2").arg(newYear).arg(NGU));
                        cellFont = dataTable->item(r, 0)->font();
                        cellFont.setBold(true);
                        dataTable->item(r, 0)->setFont(cellFont);
                    }
                    test = true;
                    row = rowStart + 1;
                    rowStart = row - 1;
                    if (rowStart >= 0)
                    {
                        currYear = dataTable->item(rowStart, 0)->text().split(" . ").at(0);
                        NGU = 0;
                    }
                    while (test)
                    {
                        if (rowStart < 0)
                        {
                            test = false;
                        }else{
                            if (dataTable->item(rowStart, 0)->text().split(" . ").at(0) != currYear)
                            {
                                test = false;
                            }else{
                                rowStart--;
                            }
                        }
                    }
                    for (int r(row - 1); r > rowStart; r--)
                    {
                        NGU++;
                        dataTable->item(r, 0)->setText(QString("%1 . %2").arg(currYear).arg(NGU));
                        cellFont = dataTable->item(r, 0)->font();
                        cellFont.setBold(true);
                        dataTable->item(r, 0)->setFont(cellFont);
                    }
                }
            }
        }
    }
}
