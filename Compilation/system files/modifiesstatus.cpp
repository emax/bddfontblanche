#include <MainWindow.h>

void MainWindow::modifiesStatus()
{
    if (objectName() == "inputData")
    {
        QLabel *label0, *label1;
        label0 = qobject_cast<QLabel *>(mainLayout->itemAtPosition(1, 0)->widget());
        label1 = qobject_cast<QLabel *>(mainLayout->itemAtPosition(1, 3)->widget());
        if ((label0->text() == "Axes suivis") && (label1->text() == "Axes abandonnés"))
        {
            QTableWidget *tab0, *tab1;
            tab0 = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2,0)->widget());
            tab1 = qobject_cast<QTableWidget *>(mainLayout->itemAtPosition(2,3)->widget());
            int row(tab0->currentRow());
            if ((tab0->hasFocus()) && (row >= 0))
            {
                if (QMessageBox::information(NULL, "changer le status", "Tu vas changer le status de l'axe " + tab0->item(row, 0)->text() + ", cette action va passer cet axe dans les axes abandonnés.\nConfirme", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
                {
                    QString statusName;
                    bool followed;
                    int status(newStatus::getNewStatus(NULL, &statusName, &followed));
                    // we change the status of the axis
                    QFile AxisFile("bin/list2");
                    QMap<QString, int> AxisPosition;
                    QString line, cat, Phrase;
                    QStringList rawLine;
                    int count(0);
                    Phrase = "";
                    QList<QVariant> axisProperties(tab0->item(row, 0)->data(Qt::UserRole).toList());
                    if (!AxisFile.open(QIODevice::ReadWrite | QIODevice::Text))
                    {
                        // Error
                        QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list2.");
                        urgentStop();
                        return;
                    }else{
                        QTextStream AxisStream(&AxisFile);
                        line = AxisStream.readLine();
                        Phrase += line;
                        if (line != "###")
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list2\nMauvaise entête (\"###\" attendu)");
                            urgentStop();
                            return;
                        }else{
                            line = AxisStream.readLine();
                            Phrase += "\n" + line;
                            while (!AxisStream.atEnd()  && line != "###")
                            {
                                cat = line.split("\t").at(0);
                                AxisPosition.insert(cat, count);
                                line = AxisStream.readLine();
                                Phrase += "\n" + line;
                                count++;
                            }
                            line = AxisStream.readLine();
                            while(line.split("\t").at(0).toInt() != axisProperties.at(0).toInt())
                            {
                                Phrase += "\n" + line;
                                line = AxisStream.readLine();
                            }
                            rawLine = line.split("\t");
                            rawLine[AxisPosition["IdStat"]] = QString::number(status);
                            line = rawLine.join("\t");
                            Phrase += "\n" + line;
                            while (!AxisStream.atEnd())
                            {
                                line = AxisStream.readLine();
                                Phrase += "\n" + line;
                            }
                            AxisFile.resize(0);
                            AxisStream << Phrase;
                        }
                    }
                    AxisFile.close();
                    Phrase = "";
                    axisProperties[6] = QVariant(status);
                    if (statusName.toLower() == "suivi")
                    {
                        tab0->item(row, 1)->setText(statusName);
                        tab0->item(row, 0)->setData(Qt::UserRole, QVariant(axisProperties));
                        tab0->item(row, 1)->setData(Qt::UserRole, QVariant(axisProperties));
                        tab0->item(row, 2)->setData(Qt::UserRole, QVariant(axisProperties));
                        tab0->item(row, 3)->setData(Qt::UserRole, QVariant(axisProperties));
                        tab0->item(row, 4)->setData(Qt::UserRole, QVariant(axisProperties));
                        tab0->item(row, 5)->setData(Qt::UserRole, QVariant(axisProperties));
                        tab0->setFocus();
                    }else{
                        // change axis name (old ...)
                        if (tab0->item(row, 0)->text().contains("old"))
                        {
                            QTableWidgetItem *item;
                            tab1->insertRow(0);
                            item = new QTableWidgetItem;
                            item->setText(tab0->item(row, 0)->text());
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab1->setItem(0, 0, item);
                            tab0->removeRow(row);
                            row = 0;
                            for (int r(0); r < tab1->rowCount(); r++)
                            {
                                if (tab1->item(r, 0)->data(Qt::UserRole).toList().at(0).toInt() == axisProperties.at(0).toInt())
                                {
                                    row = r;
                                }
                            }
                            item = new QTableWidgetItem;
                            item->setText(statusName);
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab1->setItem(row, 1, item);
                            item = new QTableWidgetItem;
                            if (axisProperties.size() > 7) item->setText(QString::number(axisProperties.at(7).toInt()));
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab1->setItem(row, 2, item);
                            item = new QTableWidgetItem;
                            if (axisProperties.size() > 8) item->setText(QString::number(axisProperties.at(8).toInt()));
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab1->setItem(row, 3, item);
                            item = new QTableWidgetItem;
                            if (axisProperties.size() > 9) item->setText(axisProperties.at(9).toDate().toString("dd/MM/yyyy"));
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab1->setItem(row, 4, item);
                            item = new QTableWidgetItem;
                            if (axisProperties.size() > 10) item->setText(axisProperties.at(10).toDate().toString("dd/MM/yyyy"));
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab1->setItem(row, 5, item);
                            tab1->setCurrentCell(row, 0);
                            tab1->setFocus();
                        }else{
                            QString begin;
                            int count, maxCount(0);
                            if (axisProperties.size() > 5) begin = tab0->item(row, 0)->text().replace(axisProperties.at(5).toString(), "") + " old" + axisProperties.at(5).toString() + "-";
                            for (int r(0); r < tab1->rowCount(); r++)
                            {
                                if (tab1->item(r, 0)->text().startsWith(begin))
                                {
                                    count = tab1->item(r, 0)->text().replace(begin, "").toInt();
                                    if (count > maxCount) maxCount = count;
                                }
                            }
                            maxCount++;
                            if (axisProperties.size() > 5)
                            {
                                axisProperties[5] = "old" + axisProperties.at(5).toString() + "-";
                                if (maxCount < 10)
                                {
                                    axisProperties[5] = axisProperties.at(5).toString() + "0";
                                }
                                axisProperties[5]  = axisProperties.at(5).toString() +  QString::number(maxCount);
                            }
                            count = 0;
                            if (!AxisFile.open(QIODevice::ReadWrite | QIODevice::Text))
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list2.");
                                urgentStop();
                                return;
                            }else{
                                QTextStream AxisStream(&AxisFile);
                                line = AxisStream.readLine();
                                Phrase += line;
                                if (line != "###")
                                {
                                    // Error
                                    QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list2\nMauvaise entête (\"###\" attendu)");
                                    urgentStop();
                                    return;
                                }else{
                                    line = AxisStream.readLine();
                                    Phrase += "\n" + line;
                                    while (!AxisStream.atEnd()  && line != "###")
                                    {
                                        cat = line.split("\t").at(0);
                                        AxisPosition.insert(cat, count);
                                        line = AxisStream.readLine();
                                        Phrase += "\n" + line;
                                        count++;
                                    }
                                    line = AxisStream.readLine();
                                    while(line.split("\t").at(0).toInt() != axisProperties.at(0).toInt())
                                    {
                                        Phrase += "\n" + line;
                                        line = AxisStream.readLine();
                                    }
                                    rawLine = line.split("\t");
                                    if (axisProperties.size() > 5) rawLine[AxisPosition["NRam"]] = axisProperties.at(5).toString();
                                    line = rawLine.join("\t");
                                    Phrase += "\n" + line;
                                    while (!AxisStream.atEnd())
                                    {
                                        line = AxisStream.readLine();
                                        Phrase += "\n" + line;
                                    }
                                    AxisFile.resize(0);
                                    AxisStream << Phrase;
                                }
                            }
                            AxisFile.close();
                            Phrase = "";
                            QTableWidgetItem *item;
                            tab1->insertRow(0);
                            item = new QTableWidgetItem;
                            if (maxCount < 10)
                            {
                                item->setText(begin + "0" + QString::number(maxCount));
                            }else{
                                item->setText(begin + QString::number(maxCount));
                            }
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab1->setItem(0, 0, item);
                            tab0->removeRow(row);
                            row = 0;
                            for (int r(0); r < tab1->rowCount(); r++)
                            {
                                if (tab1->item(r, 0)->data(Qt::UserRole).toList().at(0).toInt() == axisProperties.at(0).toInt())
                                {
                                    row = r;
                                }
                            }
                            item = new QTableWidgetItem;
                            item->setText(statusName);
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab1->setItem(row, 1, item);
                            item = new QTableWidgetItem;
                            if (axisProperties.size() > 7) item->setText(QString::number(axisProperties.at(7).toInt()));
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab1->setItem(row, 2, item);
                            item = new QTableWidgetItem;
                            if (axisProperties.size() > 8) item->setText(QString::number(axisProperties.at(8).toInt()));
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab1->setItem(row, 3, item);
                            item = new QTableWidgetItem;
                            if (axisProperties.size() > 9) item->setText(axisProperties.at(9).toDate().toString("dd/MM/yyyy"));
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab1->setItem(row, 4, item);
                            item = new QTableWidgetItem;
                            if (axisProperties.size() > 10) item->setText(axisProperties.at(10).toDate().toString("dd/MM/yyyy"));
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab1->setItem(row, 5, item);
                            tab1->setCurrentCell(row, 0);
                            tab1->setFocus();
                        }
                    }
                }
            }else{
                int row(tab1->currentRow());
                if ((tab1->hasFocus()) && (row != -1))
                {
                    if (QMessageBox::information(NULL, "changer le status", "Tu vas changer le status de l'axe " + tab1->item(row, 0)->text() + ".\nConfirme", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
                    {
                        QString statusName;
                        bool followed;
                        int status(newStatus::getNewStatus(NULL, &statusName, &followed));
                        // we change the status of the axis
                        QFile AxisFile("bin/list2");
                        QMap<QString, int> AxisPosition;
                        QString line, cat, Phrase;
                        QStringList rawLine;
                        int count(0);
                        Phrase = "";
                        QList<QVariant> axisProperties(tab1->item(row, 0)->data(Qt::UserRole).toList());
                        if (!AxisFile.open(QIODevice::ReadWrite | QIODevice::Text))
                        {
                            // Error
                            QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : bin/list2.");
                            urgentStop();
                            return;
                        }else{
                            QTextStream AxisStream(&AxisFile);
                            line = AxisStream.readLine();
                            Phrase += line;
                            if (line != "###")
                            {
                                // Error
                                QMessageBox::critical(NULL, "Erreur", "Une erreur est survenue à la lecture de la première ligne du fichier : bin/list2\nMauvaise entête (\"###\" attendu)");
                                urgentStop();
                                return;
                            }else{
                                line = AxisStream.readLine();
                                Phrase += "\n" + line;
                                while (!AxisStream.atEnd()  && line != "###")
                                {
                                    cat = line.split("\t").at(0);
                                    AxisPosition.insert(cat, count);
                                    line = AxisStream.readLine();
                                    Phrase += "\n" + line;
                                    count++;
                                }
                                line = AxisStream.readLine();
                                while(line.split("\t").at(0).toInt() != axisProperties.at(0).toInt())
                                {
                                    Phrase += "\n" + line;
                                    line = AxisStream.readLine();
                                }
                                rawLine = line.split("\t");
                                rawLine[AxisPosition["IdStat"]] = QString::number(status);
                                line = rawLine.join("\t");
                                Phrase += "\n" + line;
                                while (!AxisStream.atEnd())
                                {
                                    line = AxisStream.readLine();
                                    Phrase += "\n" + line;
                                }
                                AxisFile.resize(0);
                                AxisStream << Phrase;
                            }
                        }
                        AxisFile.close();
                        Phrase = "";
                        axisProperties[6] = QVariant(status);
                        if (statusName.toLower() == "suivi")
                        {
                            QTableWidgetItem *item;
                            tab0->insertRow(0);
                            item = new QTableWidgetItem;
                            item->setText(tab1->item(row, 0)->text());
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab0->setItem(0, 0, item);
                            tab1->removeRow(row);
                            row = 0;
                            for (int r(0); r < tab0->rowCount(); r++)
                            {
                                if (tab0->item(r, 0)->data(Qt::UserRole).toList().at(0).toInt() == axisProperties.at(0).toInt())
                                {
                                    row = r;
                                }
                            }
                            item = new QTableWidgetItem;
                            item->setText(statusName);
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab0->setItem(row, 1, item);
                            item = new QTableWidgetItem;
                            if (axisProperties.size() > 7) item->setText(QString::number(axisProperties.at(7).toInt()));
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab0->setItem(row, 2, item);
                            item = new QTableWidgetItem;
                            if (axisProperties.size() > 8) item->setText(QString::number(axisProperties.at(8).toInt()));
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab0->setItem(row, 3, item);
                            item = new QTableWidgetItem;
                            if (axisProperties.size() > 9) item->setText(axisProperties.at(9).toDate().toString("dd/MM/yyyy"));
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab0->setItem(row, 4, item);
                            item = new QTableWidgetItem;
                            if (axisProperties.size() > 10) item->setText(axisProperties.at(10).toDate().toString("dd/MM/yyyy"));
                            item->setData(Qt::UserRole, QVariant(axisProperties));
                            item->setFlags(item->flags() & ~ Qt::ItemIsEditable);
                            tab0->setItem(row, 5, item);
                            tab0->setCurrentCell(row, 0);
                            tab0->setFocus();
                        }else{
                            tab1->item(row, 1)->setText(statusName);
                            tab1->item(row, 0)->setData(Qt::UserRole, QVariant(axisProperties));
                            tab1->item(row, 1)->setData(Qt::UserRole, QVariant(axisProperties));
                            tab1->item(row, 2)->setData(Qt::UserRole, QVariant(axisProperties));
                            tab1->item(row, 3)->setData(Qt::UserRole, QVariant(axisProperties));
                            tab1->item(row, 4)->setData(Qt::UserRole, QVariant(axisProperties));
                            tab1->item(row, 5)->setData(Qt::UserRole, QVariant(axisProperties));
                            tab1->setFocus();
                        }
                    }
                }
            }
        }
    }
}

