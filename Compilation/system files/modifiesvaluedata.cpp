#include <MainWindow.h>

void MainWindow::modifiesValueData(int a, int b)
{
    QString fileName, factorValue("");
    QTableWidget *tab;
    tab = qobject_cast<QTableWidget *>(QObject::sender());
    if (tab->objectName() == "GUTable")
    {
        fileName = "bin/measure";
    }else{
        fileName = "bin/reproductionMeasure";
    }
    if ((b > 0) && (b < (tab->columnCount() - 1)))
    {
        QList<QVariant> datas;
        datas = tab->item(a, b)->data(Qt::UserRole).toList();
        int Id, pos;
        QString name, type, value;
        if (datas.size() > 3)
        {
            Id = datas.at(0).toInt();
            name = datas.at(1).toString();
            pos = datas.at(2).toInt();
            type = datas.at(3).toString();
            bool ok(false);
            if (type == "DOUBLE")
            {
                double temp;
                temp = QInputDialog::getDouble(NULL, "Nouvelle valeur", "Quelle nouvelle valeur pour la variable '" + name + "'?", tab->item(a, b)->text().toDouble(), -2147483641, 2147483647, 2, &ok);
                if (ok)
                {
                    value = QString::number(temp);
                }else{
                    if (QMessageBox::information(NULL, "Valeur nulle?", "Faut-il mettre une valeur nulle?", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
                    {
                        value = "";
                        ok = true;
                    }
                }
            }else if (type == "INTEGER")
            {
                int temp;
                temp = QInputDialog::getInt(NULL, "Nouvelle valeur", "Quelle nouvelle valeur pour la variable '" + name + "'?", tab->item(a, b)->text().toInt(), -2147483647, 214783647, 1, &ok);
                if (ok)
                {
                    value = QString::number(temp);
                }else{
                    if (QMessageBox::information(NULL, "Valeur nulle?", "Faut-il mettre une valeur nulle?", QMessageBox::Yes, QMessageBox::No) == QMessageBox::Yes)
                    {
                        value = "";
                        ok = true;
                    }
                }
            }else if (type == "STRING")
            {
                value = QInputDialog::getText(NULL, "Nouvelle valeur", "Quelle nouvelle valeur pour la variable '" + name + "'?", QLineEdit::Normal, tab->item(a, b)->text(), &ok);
            }else if (type == "DATE")
            {
                QDate temp;
                temp = DialogueDate::getDate(NULL, "Nouvele valeur", "Quelle nouvelle valeur pour la variable '" + name + "'?", QDate::currentDate(), &ok);
                if (ok)
                {
                    value = temp.toString("yyyy.MM.dd");
                }
            }else if (type == "FACTOR")
            {
                QMap<QString, QVariant> factorMap;
                if (datas.size() > 4)
                {
                    factorMap = datas.at(4).toMap();
                    bool temp(false);
                    value = QInputDialog::getText(NULL, "Nouvelle valeur", "Quelle nouvelle valeur pour la variable '" + name + "'?", QLineEdit::Normal, tab->item(a, b)->text(), &ok);
                    if (ok == false)
                    {
                        temp = true;
                    }else{
                        if (value == "")
                        {
                            temp = true;
                        }else{
                            if (factorMap.values().contains(QVariant(value)))
                            {
                                temp = true;
                            }
                        }
                    }
                    while (!temp)
                    {
                        value = QInputDialog::getText(NULL, "Nouvelle valeur", "Merci de me donner une nouvelle valeur valide pour la variable '" + name + "'?", QLineEdit::Normal, tab->item(a, b)->text(), &ok);
                        if (ok == false)
                        {
                            temp = true;
                        }else{
                            if (value == "")
                            {
                                temp = true;
                            }else{
                                if (factorMap.values().contains(QVariant(value)))
                                {
                                    temp = true;
                                }
                            }
                        }
                    }
                    if ((ok) && (value != ""))
                    {
                        factorValue = factorMap.key(QVariant(value));
                    }
                }
            }
            if (ok)
            {
                // Now we can modifie the line
                QFile measureFile(fileName);
                QString s;
                QStringList dataRow;
                s = "";
                if (!measureFile.open(QIODevice::ReadWrite | QIODevice::Text))
                {
                    // Error
                    QMessageBox::critical(NULL, "Erreur", "Le programme n'a pas réussi à lire le fichier de données : " + fileName + ".");
                    urgentStop();
                    return;
                }else{
                    QTextStream measureStream(&measureFile);
                    while (!measureStream.atEnd())
                    {
                        dataRow = measureStream.readLine().split("\t");
                        if (dataRow.at(0).toInt() == Id)
                        {
                            if (type == "FACTOR")
                            {
                                dataRow[pos] = factorValue;
                            }else dataRow[pos] = value;
                        }
                        s += dataRow.join("\t") + "\n";
                    }
                    measureFile.resize(0);
                    measureStream << s;
                    measureFile.close();
                }
                tab->item(a, b)->setText(value);
            }
        }
    }
}
