#include "MainWindow.h"

void MainWindow::saveDataContext()
{
    // Empty mainLayout
    while(mainLayout->count() > 0){
        QLayoutItem *item = mainLayout->takeAt(0);
        delete item->widget();
        delete item;
    }
    delete mainLayout;
    mainLayout = new QGridLayout;
    setLayout(mainLayout);

    // Set state file to 2
    QFile stateFile("state");
    stateFile.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream in(&stateFile);
    in << 2;
    stateFile.close();

    // 3 buttons to create
    QPushButton *inputDataButton = new QPushButton;
    QPushButton *boardInputButton = new QPushButton;
    QPushButton *backTo0Button = new QPushButton;

    // Labels for the buttons
    inputDataButton->setText("Saisir des données");
    boardInputButton->setText("Préparer une saisie embarquée");
    backTo0Button->setText("Retour");

    // Connect slots to signals clicked
    QObject::connect(inputDataButton, SIGNAL(clicked()), this, SLOT(inputDataSet()));
    QObject::connect(boardInputButton, SIGNAL(clicked()), this, SLOT(boardInputSet()));
    QObject::connect(backTo0Button, SIGNAL(clicked()), this, SLOT(backTo0Slot()));

    //Display buttons into the mainLayout
    mainLayout->addWidget(inputDataButton, 0, 0);
    mainLayout->addWidget(boardInputButton, 1, 0);
    mainLayout->addWidget(backTo0Button, 2, 0);

    if (*state == "4")
    {
        boardInputButton->hide();
    }
}
