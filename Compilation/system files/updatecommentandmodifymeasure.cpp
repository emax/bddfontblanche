#include <MainWindow.h>

void MainWindow::updateCommentAndModifyMeasure(int a, int b)
{
    // We first need to know which tree is concerned
    QTableWidget *sourceTab;
    sourceTab = qobject_cast<QTableWidget *>(QObject::sender());
    if (sourceTab->columnCount() == 6)
    {
        QTableWidgetItem *sourceCell = sourceTab->item(a, b);
        // Now we update the comment combobox
        QComboBox *combo(qobject_cast<QComboBox *>(mainLayout->itemAtPosition(2, 2)->widget()));
        combo->setCurrentIndex(-1);
        while (combo->count() > 1)
        {
            combo->removeItem(1);
        }
        // We then update the commment table
        QList<QVariant> axisProperties(sourceCell->data(Qt::UserRole).toList());
        combo->addItem(sourceTab->item(a, 0)->text(), axisProperties.at(0));
        combo->setCurrentIndex(1);
        // And now if there is a measure we load it
    }else{
        QTableWidgetItem *sourceCell = sourceTab->item(a, 0);
        // Now we update the comment combobox
        QComboBox *combo(qobject_cast<QComboBox *>(mainLayout->itemAtPosition(2, 2)->widget()));
        combo->setCurrentIndex(-1);
        while (combo->count() > 1)
        {
            combo->removeItem(1);
        }
        // We then update the commment table
        QVariant GUProperties(sourceCell->data(Qt::UserRole));
        if (userData.toList().size() > 3) combo->addItem(qobject_cast<QLabel *>(mainLayout->itemAtPosition(10, 0)->widget())->text(), userData.toList().at(3));
        combo->addItem(combo->itemText(1) + " - " + sourceTab->item(a, 0)->text(), GUProperties);
        combo->setCurrentIndex(2);
        // And now if there is a measure we load it
    }
}
