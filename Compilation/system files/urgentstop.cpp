#include <MainWindow.h>
#include <addaxisclass.h>
#include <changeparameterclass.h>

void MainWindow::urgentStop()
{
    // Set the state file to the previous state
    QFile stateFile("state");
    stateFile.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream in(&stateFile);
    in << state->toInt();
    stateFile.close();
    close();
}

void addAxisClass::urgentStop()
{
    // Set the state file to the previous state
    close();
}

void newStatus::urgentStop()
{
    // Set the state file to the previous state
    close();
}

void changeParameterClass::urgentStop()
{
    // Set the state file to the previous state
    close();
}
